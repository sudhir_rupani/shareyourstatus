package com.prayosha.shareyourstatus.Item;

import java.io.Serializable;

public class CategoryList implements Serializable {

    private String cid, category_name, category_image, category_image_thumb,start_color,end_color;

    public CategoryList(String cid, String category_name, String category_image, String category_image_thumb, String start_color, String end_color) {
        this.cid = cid;
        this.category_name = category_name;
        this.category_image = category_image;
        this.category_image_thumb = category_image_thumb;
        this.start_color = start_color;
        this.end_color = end_color;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    public String getCategory_image_thumb() {
        return category_image_thumb;
    }

    public void setCategory_image_thumb(String category_image_thumb) {
        this.category_image_thumb = category_image_thumb;
    }

    public String getStart_color() {
        return start_color;
    }

    public void setStart_color(String start_color) {
        this.start_color = start_color;
    }

    public String getEnd_color() {
        return end_color;
    }

    public void setEnd_color(String end_color) {
        this.end_color = end_color;
    }
}
