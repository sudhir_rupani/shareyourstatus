package com.prayosha.shareyourstatus.Item;

import java.io.Serializable;

public class AboutUsList implements Serializable {

    private String app_name, publisher_id, banner_ad_type, banner_ad_id, interstitial_ad_type, interstital_ad_id, interstital_ad_click, rewarded_video_ads_id, rewarded_video_click, spinner_opt;
    private boolean banner_ad, interstital_ad, rewarded_video_ads;

    public AboutUsList(String app_name, String publisher_id, String banner_ad_type, String banner_ad_id, String interstitial_ad_type, String interstital_ad_id, String interstital_ad_click, String rewarded_video_ads_id, String rewarded_video_click, String spinner_opt, boolean banner_ad, boolean interstital_ad, boolean rewarded_video_ads) {
        this.app_name = app_name;
        this.publisher_id = publisher_id;
        this.banner_ad_type = banner_ad_type;
        this.banner_ad_id = banner_ad_id;
        this.interstitial_ad_type = interstitial_ad_type;
        this.interstital_ad_id = interstital_ad_id;
        this.interstital_ad_click = interstital_ad_click;
        this.rewarded_video_ads_id = rewarded_video_ads_id;
        this.rewarded_video_click = rewarded_video_click;
        this.spinner_opt = spinner_opt;
        this.banner_ad = banner_ad;
        this.interstital_ad = interstital_ad;
        this.rewarded_video_ads = rewarded_video_ads;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(String publisher_id) {
        this.publisher_id = publisher_id;
    }

    public String getBanner_ad_type() {
        return banner_ad_type;
    }

    public void setBanner_ad_type(String banner_ad_type) {
        this.banner_ad_type = banner_ad_type;
    }

    public String getBanner_ad_id() {
        return banner_ad_id;
    }

    public void setBanner_ad_id(String banner_ad_id) {
        this.banner_ad_id = banner_ad_id;
    }

    public String getInterstitial_ad_type() {
        return interstitial_ad_type;
    }

    public void setInterstitial_ad_type(String interstitial_ad_type) {
        this.interstitial_ad_type = interstitial_ad_type;
    }

    public String getInterstital_ad_id() {
        return interstital_ad_id;
    }

    public void setInterstital_ad_id(String interstital_ad_id) {
        this.interstital_ad_id = interstital_ad_id;
    }

    public String getInterstital_ad_click() {
        return interstital_ad_click;
    }

    public void setInterstital_ad_click(String interstital_ad_click) {
        this.interstital_ad_click = interstital_ad_click;
    }

    public String getRewarded_video_ads_id() {
        return rewarded_video_ads_id;
    }

    public void setRewarded_video_ads_id(String rewarded_video_ads_id) {
        this.rewarded_video_ads_id = rewarded_video_ads_id;
    }

    public String getRewarded_video_click() {
        return rewarded_video_click;
    }

    public void setRewarded_video_click(String rewarded_video_click) {
        this.rewarded_video_click = rewarded_video_click;
    }

    public String getSpinner_opt() {
        return spinner_opt;
    }

    public void setSpinner_opt(String spinner_opt) {
        this.spinner_opt = spinner_opt;
    }

    public boolean isBanner_ad() {
        return banner_ad;
    }

    public void setBanner_ad(boolean banner_ad) {
        this.banner_ad = banner_ad;
    }

    public boolean isInterstital_ad() {
        return interstital_ad;
    }

    public void setInterstital_ad(boolean interstital_ad) {
        this.interstital_ad = interstital_ad;
    }

    public boolean isRewarded_video_ads() {
        return rewarded_video_ads;
    }

    public void setRewarded_video_ads(boolean rewarded_video_ads) {
        this.rewarded_video_ads = rewarded_video_ads;
    }
}
