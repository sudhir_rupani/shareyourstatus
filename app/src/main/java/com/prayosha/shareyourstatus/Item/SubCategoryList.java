package com.prayosha.shareyourstatus.Item;

import java.io.Serializable;
import java.util.List;

public class SubCategoryList implements Serializable {

    private String adView, id, cid, status_type, status_title, video_url,gif_url, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, total_download,already_like, category_name, quote_bg, quote_font,is_favourite,is_reviewed,external_link;
    private String user_id, user_name, user_image, already_follow, is_verified, total_comment, watermark_image, watermark_on_off;
    private List<CommentList> commentLists;

    public SubCategoryList() {
    }

    //download
    public SubCategoryList(String id, String video_title, String video_url,String gif_url, String video_thumbnail_b, String video_thumbnail_s, String category_name, String video_layout,String status_type) {
        this.id = id;
        this.status_title = video_title;
        this.video_url = video_url;
        this.gif_url = gif_url;
        this.status_thumbnail_b = video_thumbnail_b;
        this.status_thumbnail_s = video_thumbnail_s;
        this.category_name = category_name;
        this.status_layout = video_layout;
        this.status_type = status_type;
    }

    //All other uses
    public SubCategoryList(String adView, String id, String status_type, String status_title, String status_layout, String status_thumbnail_b, String status_thumbnail_s, String total_viewer, String total_likes, String already_like, String category_name, String quote_bg, String quote_font,String is_favourite,String is_reviewed,String external_link) {
        this.adView = adView;
        this.id = id;
        this.status_type = status_type;
        this.status_title = status_title;
        this.status_layout = status_layout;
        this.status_thumbnail_b = status_thumbnail_b;
        this.status_thumbnail_s = status_thumbnail_s;
        this.total_viewer = total_viewer;
        this.total_likes = total_likes;
        this.already_like = already_like;
        this.category_name = category_name;
        this.quote_bg = quote_bg;
        this.quote_font = quote_font;
        this.is_favourite = is_favourite;
        this.is_reviewed = is_reviewed;
        this.external_link = external_link;
    }

    //Sub UserActivity Detail
    public SubCategoryList(String adView, String id, String cid, String status_type, String status_title, String video_url, String status_layout, String status_thumbnail_b, String status_thumbnail_s, String total_viewer, String total_likes,String total_download, String already_like, String category_name, String quote_bg, String quote_font, String user_id, String user_name, String user_image, String already_follow, String is_verified, String total_comment, String watermark_image, String watermark_on_off,String is_favourite,String is_reviewed,String external_link, List<CommentList> commentLists) {
        this.adView = adView;
        this.id = id;
        this.cid = cid;
        this.status_type = status_type;
        this.status_title = status_title;
        this.video_url = video_url;
        this.status_layout = status_layout;
        this.status_thumbnail_b = status_thumbnail_b;
        this.status_thumbnail_s = status_thumbnail_s;
        this.total_viewer = total_viewer;
        this.total_likes = total_likes;
        this.total_download = total_download;
        this.already_like = already_like;
        this.category_name = category_name;
        this.quote_bg = quote_bg;
        this.quote_font = quote_font;
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_image = user_image;
        this.already_follow = already_follow;
        this.is_verified = is_verified;
        this.total_comment = total_comment;
        this.watermark_image = watermark_image;
        this.watermark_on_off = watermark_on_off;
        this.is_favourite = is_favourite;
        this.is_reviewed = is_reviewed;
        this.external_link = external_link;
        this.commentLists = commentLists;
    }

    public String getAdView() {
        return adView;
    }

    public void setAdView(String adView) {
        this.adView = adView;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getStatus_type() {
        return status_type;
    }

    public void setStatus_type(String status_type) {
        this.status_type = status_type;
    }

    public String getStatus_title() {
        return status_title;
    }

    public void setStatus_title(String status_title) {
        this.status_title = status_title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getGif_url() {
        return gif_url;
    }

    public void setGif_url(String gif_url) {
        this.gif_url = gif_url;
    }

    public String getStatus_layout() {
        return status_layout;
    }

    public void setStatus_layout(String status_layout) {
        this.status_layout = status_layout;
    }

    public String getStatus_thumbnail_b() {
        return status_thumbnail_b;
    }

    public void setStatus_thumbnail_b(String status_thumbnail_b) {
        this.status_thumbnail_b = status_thumbnail_b;
    }

    public String getStatus_thumbnail_s() {
        return status_thumbnail_s;
    }

    public void setStatus_thumbnail_s(String status_thumbnail_s) {
        this.status_thumbnail_s = status_thumbnail_s;
    }

    public String getTotal_viewer() {
        return total_viewer;
    }

    public void setTotal_viewer(String total_viewer) {
        this.total_viewer = total_viewer;
    }

    public String getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(String total_likes) {
        this.total_likes = total_likes;
    }

    public String getTotal_download() {
        return total_download;
    }

    public void setTotal_download(String total_download) {
        this.total_download = total_download;
    }

    public String getAlready_like() {
        return already_like;
    }

    public void setAlready_like(String already_like) {
        this.already_like = already_like;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getQuote_bg() {
        return quote_bg;
    }

    public void setQuote_bg(String quote_bg) {
        this.quote_bg = quote_bg;
    }

    public String getQuote_font() {
        return quote_font;
    }

    public void setQuote_font(String quote_font) {
        this.quote_font = quote_font;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getAlready_follow() {
        return already_follow;
    }

    public void setAlready_follow(String already_follow) {
        this.already_follow = already_follow;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getTotal_comment() {
        return total_comment;
    }

    public void setTotal_comment(String total_comment) {
        this.total_comment = total_comment;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getIs_reviewed() {
        return is_reviewed;
    }

    public void setIs_reviewed(String is_reviewed) {
        this.is_reviewed = is_reviewed;
    }

    public String getExternal_link() {
        return external_link;
    }

    public void setExternal_link(String external_link) {
        this.external_link = external_link;
    }

    public String getWatermark_image() {
        return watermark_image;
    }

    public void setWatermark_image(String watermark_image) {
        this.watermark_image = watermark_image;
    }

    public String getWatermark_on_off() {
        return watermark_on_off;
    }

    public void setWatermark_on_off(String watermark_on_off) {
        this.watermark_on_off = watermark_on_off;
    }

    public List<CommentList> getCommentLists() {
        return commentLists;
    }

    public void setCommentLists(List<CommentList> commentLists) {
        this.commentLists = commentLists;
    }

}
