package com.prayosha.shareyourstatus.Util;

import android.os.Environment;

import com.prayosha.shareyourstatus.BuildConfig;
import com.prayosha.shareyourstatus.Item.AboutUsList;

import java.io.File;
import java.util.List;

public class Constant_Api {

    //search_menu server api url
    public static String url = BuildConfig.My_api + "api.php";

    //tag
    public static String STATUS = "status";

    //search_menu server api url
    public static String video_upload_url = BuildConfig.My_api + "api_video_upload.php";

    //search_menu server api tag
    public static String tag = "data";

    //Status path
    public static String status_path = "WhatsApp/Media/.Statuses";

    //video storage folder path
    public static String video_path = Environment.getExternalStorageDirectory() + "/Video_Status/" + "Video/";

    //image and gif status storage path
    public static String image_path = Environment.getExternalStorageDirectory() + "/Video_Status/" + "Status_Image/";

    //Status Download path
    public static String download_status_path = Environment.getExternalStorageDirectory() + "/Video_Status/" + "/status_saver/";

    public static int AD_COUNT = 0;
    public static int AD_COUNT_SHOW = 0;

    public static int REWARD_VIDEO_AD_COUNT = 0;
    public static int REWARD_VIDEO_AD_COUNT_SHOW = 0;

    public static AboutUsList aboutUsList;

    public static List<File> imageFilesList;
    public static List<File> videoFilesList;

    public static List<File> downloadImageFilesList;
    public static List<File> downloadVideoFilesList;

}
