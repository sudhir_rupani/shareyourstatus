package com.prayosha.shareyourstatus.Util;


public class EnchantedPagerConstants {
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.90f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
}
