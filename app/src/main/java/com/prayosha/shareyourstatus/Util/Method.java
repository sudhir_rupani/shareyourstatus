package com.prayosha.shareyourstatus.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.ViewPager;

import com.prayosha.shareyourstatus.DataBase.DatabaseHandler;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.FullScreen;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.InterFace.VideoAd;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Service.DownloadIGService;
import com.prayosha.shareyourstatus.Service.DownloadVideoService;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.login.LoginManager;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

import cz.msebera.android.httpclient.Header;

public class Method {

    private Activity activity;
    public static boolean loginBack = false;
    public static boolean isUpload = true, isDownload = true;
    public boolean personalization_ad = false;
    private OnClick onClick;
    private VideoAd videoAd;
    private FullScreen fullScreen;

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    private final String myPreference = "login";
    public String pref_login = "pref_login";
    private String firstTime = "firstTime";
    public String profileId = "profileId";
    public String auth_id = "auth_id";
    public String userEmail = "userEmail";
    public String userPassword = "userPassword";
    public String userName = "userName";
    public String userImage = "userImage";
    public String loginType = "loginType";
    public String show_login = "show_login";
    public String notification = "notification";
    public String verification_code = "verification_code";
    public String is_verification = "is_verification";

    public String reg_name = "reg_name";
    public String reg_email = "reg_email";
    public String reg_password = "reg_password";
    public String reg_phoneNo = "reg_phoneNo";
    public String reg_reference = "reg_reference";

    public String language_ids = "language_ids";

    private String filename;
    private DatabaseHandler db;

    @SuppressLint("CommitPrefEdits")
    public Method(Activity activity) {
        this.activity = activity;
        db = new DatabaseHandler(activity);
        pref = activity.getSharedPreferences(myPreference, 0); // 0 - for private mode
        editor = pref.edit();
    }

    @SuppressLint("CommitPrefEdits")
    public Method(Activity activity, VideoAd videoAd) {
        this.activity = activity;
        db = new DatabaseHandler(activity);
        pref = activity.getSharedPreferences(myPreference, 0); // 0 - for private mode
        editor = pref.edit();
        this.videoAd = videoAd;
    }

    @SuppressLint("CommitPrefEdits")
    public Method(Activity activity, OnClick onClick) {
        this.activity = activity;
        db = new DatabaseHandler(activity);
        this.onClick = onClick;
        pref = activity.getSharedPreferences(myPreference, 0); // 0 - for private mode
        editor = pref.edit();
    }

    @SuppressLint("CommitPrefEdits")
    public Method(Activity activity, OnClick onClick, VideoAd videoAd, FullScreen fullScreen) {
        this.activity = activity;
        db = new DatabaseHandler(activity);
        this.onClick = onClick;
        this.videoAd = videoAd;
        this.fullScreen = fullScreen;
        pref = activity.getSharedPreferences(myPreference, 0); // 0 - for private mode
        editor = pref.edit();
    }

    public void login() {
        if (!pref.getBoolean(firstTime, false)) {
            editor.putBoolean(pref_login, false);
            editor.putBoolean(firstTime, true);
            editor.commit();
        }
    }

    //rtl
    public void forceRTLIfSupported() {
        if (activity.getResources().getString(R.string.isRTL).equals("true")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    //Whatsapp application installation or not check
    public boolean isAppInstalled_Whatsapp() {
        String packageName = "com.whatsapp";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    //instagram application installation or not check
    public boolean isAppInstalled_Instagram() {
        String packageName = "com.instagram.android";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    //facebook application installation or not check
    public boolean isAppInstalled_facebook() {
        String packageName = "com.facebook.katana";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    //facebook messenger application installation or not check
    public boolean isAppInstalled_fbMessenger() {
        String packageName = "com.facebook.orca";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    //twitter application installation or not check
    public boolean isAppInstalled_twitter() {
        String packageName = "com.twitter.android";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    //network check
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //get screen width
    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();

        point.x = display.getWidth();
        point.y = display.getHeight();

        columnWidth = point.x;
        return columnWidth;
    }

    //get screen height
    public int getScreenHeight() {
        int columnHeight;
        WindowManager wm = (WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();

        point.x = display.getWidth();
        point.y = display.getHeight();

        columnHeight = point.y;
        return columnHeight;
    }

    //---------------video ad show dialog---------------//

    public void VideoAdDialog(String type, String value) {

        if (Constant_Api.aboutUsList != null) {
            if (Constant_Api.aboutUsList.isRewarded_video_ads()) {
                if (value.equals("view_ad")) {
                    showAdDialog(type);
                } else {
                    Constant_Api.REWARD_VIDEO_AD_COUNT = Constant_Api.REWARD_VIDEO_AD_COUNT + 1;
                    if (Constant_Api.REWARD_VIDEO_AD_COUNT == Constant_Api.REWARD_VIDEO_AD_COUNT_SHOW) {
                        Constant_Api.REWARD_VIDEO_AD_COUNT = 0;
                        showAdDialog(type);
                    } else {
                        callVideoAdData(type);
                    }
                }
            } else {
                callVideoAdData(type);
            }
        } else {
            callVideoAdData(type);
        }

    }

    private void showAdDialog(String type) {
        Dialog dialog = new Dialog(activity);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_view_ad);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.FILL_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        MaterialButton button_yes = dialog.findViewById(R.id.button_yes_viewAd);
        MaterialButton button_no = dialog.findViewById(R.id.button_no_viewAd);

        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                show_video_ad(type);
            }
        });

        button_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (Constant_Api.aboutUsList.isInterstital_ad()) {
                    skip_video_Ad(type);
                } else {
                    callVideoAdData(type);
                }
            }
        });

        dialog.show();
    }

    private void show_video_ad(String type) {

        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        final RewardedVideoAd mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
        if (mRewardedVideoAd != null) {
            AdRequest adRequest;
            if (personalization_ad) {
                Bundle extras = new Bundle();
                extras.putBoolean("_noRefresh", true);
                adRequest = new AdRequest.Builder()
                        .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                        .build();
            } else {
                Bundle extras = new Bundle();
                extras.putString("npa", "1");
                extras.putBoolean("_noRefresh", true);
                adRequest = new AdRequest.Builder()
                        .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                        .build();
            }
            mRewardedVideoAd.loadAd(Constant_Api.aboutUsList.getRewarded_video_ads_id(), adRequest);
            mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewarded(RewardItem reward) {
                    Log.d("reward_video_ad", "reward");
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {
                    Log.d("reward_video_ad", "AdLeftApplication");
                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                    callVideoAdData(type);
                    progressDialog.dismiss();
                    Log.d("reward_video_ad", "Failed");
                }

                @Override
                public void onRewardedVideoAdClosed() {
                    callVideoAdData(type);
                    Log.d("reward_video_ad", "close");
                }

                @Override
                public void onRewardedVideoAdLoaded() {
                    mRewardedVideoAd.show();
                    progressDialog.dismiss();
                    Log.d("reward_video_ad", "load");
                }

                @Override
                public void onRewardedVideoAdOpened() {
                    Log.d("reward_video_ad", "open");
                }

                @Override
                public void onRewardedVideoStarted() {
                    Log.d("reward_video_ad", "start");
                }

                @Override
                public void onRewardedVideoCompleted() {
                    Log.d("reward_video_ad", "completed");
                }
            });
        } else {
            progressDialog.dismiss();
            callVideoAdData(type);
        }
    }

    private void skip_video_Ad(String type) {

        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (Constant_Api.aboutUsList.getInterstitial_ad_type().equals("admob")) {
            final InterstitialAd interstitialAd = new InterstitialAd(activity);
            AdRequest adRequest;
            if (personalization_ad) {
                adRequest = new AdRequest.Builder()
                        .build();
            } else {
                Bundle extras = new Bundle();
                extras.putString("npa", "1");
                adRequest = new AdRequest.Builder()
                        .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                        .build();
            }
            interstitialAd.setAdUnitId(Constant_Api.aboutUsList.getInterstital_ad_id());
            interstitialAd.loadAd(adRequest);
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    interstitialAd.show();
                    progressDialog.dismiss();
                }

                public void onAdClosed() {
                    callVideoAdData(type);
                    super.onAdClosed();
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    Log.d("admob_error", String.valueOf(i));
                    callVideoAdData(type);
                    super.onAdFailedToLoad(i);
                    progressDialog.dismiss();
                }

            });
        } else {
            com.facebook.ads.InterstitialAd interstitialAd = new com.facebook.ads.InterstitialAd(activity, Constant_Api.aboutUsList.getInterstital_ad_id());
            interstitialAd.setAdListener(new com.facebook.ads.InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    // Interstitial ad displayed callback
                    Log.e("fb_ad", "Interstitial ad displayed.");
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                    // Interstitial dismissed callback
                    progressDialog.dismiss();
                    callVideoAdData(type);
                    Log.e("fb_ad", "Interstitial ad dismissed.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    // Ad error callback
                    callVideoAdData(type);
                    progressDialog.dismiss();
                    Log.e("fb_ad", "Interstitial ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Interstitial ad is loaded and ready to be displayed
                    Log.d("fb_ad", "Interstitial ad is loaded and ready to be displayed!");
                    progressDialog.dismiss();
                    // Show the ad
                    interstitialAd.show();
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Ad clicked callback
                    Log.d("fb_ad", "Interstitial ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Ad impression logged callback
                    Log.d("fb_ad", "Interstitial ad impression logged!");
                }
            });

            // For auto play video ads, it's recommended to load the ad
            // at least 30 seconds before it is shown
            interstitialAd.loadAd();
        }

    }

    //call interface
    private void callVideoAdData(String video_ad_type) {
        videoAd.videoAdClick(video_ad_type);
    }

    //---------------video ad show dialog---------------//

    //---------------Interstitial Ad---------------//

    public void onClickData(int position, String title, String type, String
            status_type, String id, String tag) {

        ProgressDialog progressDialog = new ProgressDialog(activity);

        progressDialog.show();
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        if (Constant_Api.aboutUsList != null) {

            if (Constant_Api.aboutUsList.isInterstital_ad()) {

                if (Constant_Api.aboutUsList.getInterstitial_ad_type().equals("admob")) {

                    Constant_Api.AD_COUNT = Constant_Api.AD_COUNT + 1;
                    if (Constant_Api.AD_COUNT == Constant_Api.AD_COUNT_SHOW) {
                        Constant_Api.AD_COUNT = 0;
                        final InterstitialAd interstitialAd = new InterstitialAd(activity);
                        AdRequest adRequest;
                        if (personalization_ad) {
                            adRequest = new AdRequest.Builder()
                                    .build();
                        } else {
                            Bundle extras = new Bundle();
                            extras.putString("npa", "1");
                            adRequest = new AdRequest.Builder()
                                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                                    .build();
                        }
                        interstitialAd.setAdUnitId(Constant_Api.aboutUsList.getInterstital_ad_id());
                        interstitialAd.loadAd(adRequest);
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                interstitialAd.show();
                                progressDialog.dismiss();
                            }

                            public void onAdClosed() {
                                onClick.position(position, title, type, status_type, id, tag);
                                super.onAdClosed();
                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                Log.d("admob_error", String.valueOf(i));
                                onClick.position(position, title, type, status_type, id, tag);
                                super.onAdFailedToLoad(i);
                                progressDialog.dismiss();
                            }

                        });
                    } else {
                        onClick.position(position, title, type, status_type, id, tag);
                        progressDialog.dismiss();
                    }
                } else {

                    Constant_Api.AD_COUNT = Constant_Api.AD_COUNT + 1;
                    if (Constant_Api.AD_COUNT == Constant_Api.AD_COUNT_SHOW) {
                        Constant_Api.AD_COUNT = 0;

                        com.facebook.ads.InterstitialAd interstitialAd = new com.facebook.ads.InterstitialAd(activity, Constant_Api.aboutUsList.getInterstital_ad_id());
                        interstitialAd.setAdListener(new com.facebook.ads.InterstitialAdListener() {
                            @Override
                            public void onInterstitialDisplayed(Ad ad) {
                                // Interstitial ad displayed callback
                                Log.e("fb_ad", "Interstitial ad displayed.");
                            }

                            @Override
                            public void onInterstitialDismissed(Ad ad) {
                                // Interstitial dismissed callback
                                progressDialog.dismiss();
                                onClick.position(position, title, type, status_type, id, tag);
                                Log.e("fb_ad", "Interstitial ad dismissed.");
                            }

                            @Override
                            public void onError(Ad ad, AdError adError) {
                                // Ad error callback
                                onClick.position(position, title, type, status_type, id, tag);
                                progressDialog.dismiss();
                                Log.e("fb_ad", "Interstitial ad failed to load: " + adError.getErrorMessage());
                            }

                            @Override
                            public void onAdLoaded(Ad ad) {
                                // Interstitial ad is loaded and ready to be displayed
                                Log.d("fb_ad", "Interstitial ad is loaded and ready to be displayed!");
                                progressDialog.dismiss();
                                // Show the ad
                                interstitialAd.show();
                            }

                            @Override
                            public void onAdClicked(Ad ad) {
                                // Ad clicked callback
                                Log.d("fb_ad", "Interstitial ad clicked!");
                            }

                            @Override
                            public void onLoggingImpression(Ad ad) {
                                // Ad impression logged callback
                                Log.d("fb_ad", "Interstitial ad impression logged!");
                            }
                        });

                        // For auto play video ads, it's recommended to load the ad
                        // at least 30 seconds before it is shown
                        interstitialAd.loadAd();

                    } else {
                        progressDialog.dismiss();
                        onClick.position(position, title, type, status_type, id, tag);
                    }
                }
            } else {
                progressDialog.dismiss();
                onClick.position(position, title, type, status_type, id, tag);
            }
        } else {
            progressDialog.dismiss();
            onClick.position(position, title, type, status_type, id, tag);
        }

    }

    //---------------Interstitial Ad---------------//

    //---------------Banner Ad---------------//

    public void adView(LinearLayout linearLayout) {

        if (Constant_Api.aboutUsList != null) {
            if (Constant_Api.aboutUsList.isBanner_ad()) {
                if (Constant_Api.aboutUsList.getBanner_ad_type().equals("admob")) {
                    if (personalization_ad) {
                        showPersonalizedAds(linearLayout);
                    } else {
                        showNonPersonalizedAds(linearLayout);
                    }
                } else {
                    FbBannerAd(linearLayout);
                }
            } else {
                linearLayout.setVisibility(View.GONE);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }

    }

    private void FbBannerAd(LinearLayout linearLayout) {
        if (Constant_Api.aboutUsList != null) {
            if (Constant_Api.aboutUsList.isBanner_ad()) {
                com.facebook.ads.AdView adView = new com.facebook.ads.AdView(activity, Constant_Api.aboutUsList.getBanner_ad_id(), com.facebook.ads.AdSize.BANNER_HEIGHT_50);
                // Find the Ad Container
                // Add the ad view to your activity layout
                linearLayout.addView(adView);
                // Request an ad
                adView.loadAd();
            } else {
                linearLayout.setVisibility(View.GONE);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }

    public void showPersonalizedAds(LinearLayout linearLayout) {
        if (Constant_Api.aboutUsList != null) {
            if (Constant_Api.aboutUsList.isBanner_ad()) {
                AdView adView = new AdView(activity);
                AdRequest adRequest = new AdRequest.Builder()
                        .build();
                adView.setAdUnitId(Constant_Api.aboutUsList.getBanner_ad_id());
                adView.setAdSize(AdSize.BANNER);
                linearLayout.addView(adView);
                adView.loadAd(adRequest);
            } else {
                linearLayout.setVisibility(View.GONE);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }

    public void showNonPersonalizedAds(LinearLayout linearLayout) {
        if (Constant_Api.aboutUsList != null) {
            if (Constant_Api.aboutUsList.isBanner_ad()) {
                Bundle extras = new Bundle();
                extras.putString("npa", "1");
                AdView adView = new AdView(activity);
                AdRequest adRequest = new AdRequest.Builder()
                        .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                        .build();
                adView.setAdUnitId(Constant_Api.aboutUsList.getBanner_ad_id());
                adView.setAdSize(AdSize.BANNER);
                linearLayout.addView(adView);
                adView.loadAd(adRequest);
            } else {
                linearLayout.setVisibility(View.GONE);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }

    //---------------Banner Ad---------------//


    //---------------Full Screen---------------//

    //call interface full screen
    public void ShowFullScreen(boolean isFullScreen) {
        fullScreen.fullscreen(isFullScreen);
    }

    //---------------Full Screen---------------//

    //---------------Download status video---------------//

    public void download(String id, String status_name, String category, String
            status_image_s, String status_image_b,
                         String video_uri, String layout_type, String status_type, String
                                 watermark_image, String watermark_on_off) {

        String file_path = null;

        if (status_type.equals("video")) {

            filename = "filename-" + id + ".mp4";

            //video file save folder name
            File root_video = new File(Constant_Api.video_path);
            if (!root_video.exists()) {
                root_video.mkdirs();
            }

            File file = new File(Constant_Api.video_path, filename);
            file_path = file.toString();

            //check file exist or not
            if (!file.exists()) {

                Method.isDownload = false;

                Intent serviceIntent = new Intent(activity, DownloadVideoService.class);
                serviceIntent.setAction(DownloadVideoService.ACTION_START);
                serviceIntent.putExtra("video_id", id);
                serviceIntent.putExtra("downloadUrl", video_uri);
                serviceIntent.putExtra("file_path", root_video.toString());
                serviceIntent.putExtra("file_name", filename);
                serviceIntent.putExtra("layout_type", layout_type);
                serviceIntent.putExtra("status_type", status_type);
                serviceIntent.putExtra("watermark_image", watermark_image);
                serviceIntent.putExtra("watermark_on_off", watermark_on_off);
                activity.startService(serviceIntent);


            } else {
                alertBox(activity.getResources().getString(R.string.you_have_already_download_video));
            }

        } else if (status_type.equals("image") || status_type.equals("gif")) {

            if (status_type.equals("image")) {
                filename = "filename-" + id + ".jpg";
            } else {
                filename = "filename-" + id + ".gif";
            }

            File root_file = new File(Constant_Api.image_path);
            if (!root_file.exists()) {
                root_file.mkdirs();
            }

            File file = new File(Constant_Api.image_path, filename);
            file_path = file.toString();

            Intent serviceIntent = new Intent(activity, DownloadIGService.class);
            serviceIntent.setAction(DownloadIGService.ACTION_START);
            serviceIntent.putExtra("id", id);
            serviceIntent.putExtra("downloadUrl", status_image_b);
            serviceIntent.putExtra("file_path", root_file.toString());
            serviceIntent.putExtra("file_name", filename);
            serviceIntent.putExtra("status_type", status_type);
            activity.startService(serviceIntent);

        }

        new DownloadImage().execute(status_image_b, id, status_name, category, layout_type, status_type, file_path);

    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadImage extends AsyncTask<String, String, String> {

        private String filePath = null;
        private String iconsStoragePath;
        private String id, status_name, category, layout_type, status_type, file_path;

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(params[0]);
                id = params[1];
                status_name = params[2];
                category = params[3];
                layout_type = params[4];
                status_type = params[5];
                file_path = params[6];

                if (status_type.equals("video")) {

                    iconsStoragePath = Constant_Api.video_path;

                    File sdIconStorageDir = new File(iconsStoragePath);
                    //create storage directories, if they don't exist
                    if (!sdIconStorageDir.exists()) {
                        sdIconStorageDir.mkdirs();
                    }

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap bitmapDownload = BitmapFactory.decodeStream(input);

                    String fname = "Image-" + id;
                    filePath = iconsStoragePath + fname + ".jpg";

                    File file = new File(iconsStoragePath, filePath);
                    if (file.exists()) {
                        Log.d("file_exists", "file_exists");
                    } else {
                        try {
                            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
                            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
                            //choose another format if PNG doesn't suit you
                            bitmapDownload.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                            bos.flush();
                            bos.close();
                        } catch (IOException e) {
                            Log.w("TAG", "Error saving image file: " + e.getMessage());
                        }
                    }

                }

            } catch (IOException e) {
                // Log exception
                Log.w("error", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (status_type.equals("image")) {
                Toast.makeText(activity, activity.getResources().getString(R.string.download), Toast.LENGTH_SHORT).show();
            }

            if (db.checkId_status_download(id, status_type)) {
                if (status_type.equals("video")) {
                    db.addVideoDownload(new SubCategoryList(id, status_name, iconsStoragePath + filename, "", filePath, filePath, category, layout_type, status_type));
                } else if (status_type.equals("image")) {
                    db.addVideoDownload(new SubCategoryList(id, status_name, "", "", file_path, file_path, category, layout_type, status_type));
                } else {
                    db.addVideoDownload(new SubCategoryList(id, status_name, "", file_path, file_path, file_path, category, layout_type, status_type));
                }
            }

            super.onPostExecute(s);
        }

    }

    //---------------Download status video---------------//

    //add to favourite
    public void addToFav(String id, String user_id, String status_type, FavouriteIF favouriteIF) {

        ProgressDialog progressDialog = new ProgressDialog(activity);

        progressDialog.show();
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(activity));
        jsObj.addProperty("post_id", id);
        jsObj.addProperty("user_id", user_id);
        jsObj.addProperty("type", status_type);
        jsObj.addProperty("method_name", "status_favourite");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            suspend(message);
                        } else {
                            alertBox(message);
                        }

                        favouriteIF.isFavourite("", "");

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");
                        String msg = object.getString("msg");
                        if (success.equals("1")) {
                            String is_favourite = object.getString("is_favourite");
                            favouriteIF.isFavourite(is_favourite, msg);
                        } else {
                            favouriteIF.isFavourite("", msg);
                        }

                        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    favouriteIF.isFavourite("", activity.getResources().getString(R.string.failed_try_again));
                    alertBox(activity.getResources().getString(R.string.failed_try_again));
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                favouriteIF.isFavourite("", activity.getResources().getString(R.string.failed_try_again));
                alertBox(activity.getResources().getString(R.string.failed_try_again));
            }
        });

    }

    //alert message box
    public void alertBox(String message) {

        if (!activity.isFinishing()) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
            builder.setMessage(Html.fromHtml(message));
            builder.setCancelable(false);
            builder.setPositiveButton(activity.getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

    }

    //alert message box
    public void suspend(String message) {

        if (pref.getBoolean(pref_login, false)) {

            String type_login = pref.getString(loginType, "");
            if (type_login.equals("google")) {

                //Google login
                GoogleSignInClient mGoogleSignInClient;

                // Configure sign-in to request the user's ID, email address, and basic
                // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                // Build a GoogleSignInClient with the options specified by gso.
                mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);

                mGoogleSignInClient.signOut()
                        .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                            }
                        });
            } else if (type_login.equals("facebook")) {
                LoginManager.getInstance().logOut();
            }

            editor.putBoolean(pref_login, false);
            editor.commit();
            Events.Login loginNotify = new Events.Login("");
            GlobalBus.getBus().post(loginNotify);
        }

        if (!activity.isFinishing()) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
            builder.setMessage(Html.fromHtml(message));
            builder.setCancelable(false);
            builder.setPositiveButton(activity.getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

    }

    //view count and user video like format
    public String format(Number number) {
        char[] suffix = {' ', 'k', 'M', 'B', 'T', 'P', 'E'};
        long numValue = number.longValue();
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            return new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base];
        } else {
            return new DecimalFormat("#,##0").format(numValue);
        }
    }

    //check dark mode or not
    public boolean isDarkMode() {
        int currentNightMode = activity.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        switch (currentNightMode) {
            case Configuration.UI_MODE_NIGHT_NO:
                // Night mode is not active, we're using the light theme
                return false;
            case Configuration.UI_MODE_NIGHT_YES:
                // Night mode is active, we're using dark theme
                return true;
            default:
                return false;
        }
    }

}
