package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;

import java.util.List;


public class PortraitAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Method method;
    private String type;
    private Animation myAnim;
    private List<SubCategoryList> subCategoryLists;

    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 0;

    public PortraitAdapter(Activity activity, List<SubCategoryList> subCategoryLists, OnClick interstitialAdView, String type) {
        this.activity = activity;
        this.type = type;
        this.subCategoryLists = subCategoryLists;
        method = new Method(activity, interstitialAdView);
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.portrait_adapter, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder.getItemViewType() == VIEW_TYPE_ITEM) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            String status_type = subCategoryLists.get(position).getStatus_type();
            if (status_type.equals("video")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
            } else if (status_type.equals("image")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
            } else {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
            }

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_portable).into(viewHolder.imageView);
            } else {
                Glide.with(activity).load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_portable).into(viewHolder.imageView);
            }

            if (subCategoryLists.get(position).getIs_favourite().equals("true")) {
                viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
            }

            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, subCategoryLists.get(position).getStatus_title(), type, subCategoryLists.get(position).getStatus_type(), subCategoryLists.get(position).getId(), "");
                }
            });

            viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.imageView_favourite.startAnimation(myAnim);
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        FavouriteIF favouriteIF = new FavouriteIF() {
                            @Override
                            public void isFavourite(String isFavourite, String message) {
                                if (!isFavourite.equals("")) {
                                    if (isFavourite.equals("true")) {
                                        viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                    }
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                                    Events.FavouriteNotify homeNotify = new Events.FavouriteNotify(subCategoryLists.get(position).getId(), subCategoryLists.get(position).getStatus_layout(), isFavourite, subCategoryLists.get(position).getStatus_type());
                                    GlobalBus.getBus().post(homeNotify);
                                }
                            }
                        };
                        method.addToFav(subCategoryLists.get(position).getId(), method.pref.getString(method.profileId, ""), subCategoryLists.get(position).getStatus_type(), favouriteIF);
                    } else {
                        Method.loginBack = true;
                        activity.startActivity(new Intent(activity, Login.class));
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return subCategoryLists.size() + 1;
    }

    private boolean isHeader(int position) {
        return position == subCategoryLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void hideHeader() {
        PortraitAdapter.ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relativeLayout;
        private ImageView imageView, imageView_type, imageView_favourite;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_portrait_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_portrait_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_portrait_adapter);
            relativeLayout = itemView.findViewById(R.id.relativeLayout_fav_portrait_adapter);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public static ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

}
