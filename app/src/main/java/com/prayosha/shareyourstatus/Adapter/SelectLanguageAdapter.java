package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.InterFace.LanguageIF;
import com.prayosha.shareyourstatus.Item.LanguageList;
import com.prayosha.shareyourstatus.R;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class SelectLanguageAdapter extends RecyclerView.Adapter<SelectLanguageAdapter.ViewHolder> {

    private Activity activity;
    private LanguageIF languageIF;
    private boolean isSelectedAll = false;
    private List<LanguageList> languageLists;

    public SelectLanguageAdapter(Activity activity, List<LanguageList> languageLists, LanguageIF languageIF) {
        this.activity = activity;
        this.languageLists = languageLists;
        this.languageIF = languageIF;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.select_language_adapter, parent, false);

        return new SelectLanguageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (!isSelectedAll) {
            holder.checkBox.setChecked(false);
        }

        holder.textView.setText(languageLists.get(position).getLanguage_name());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                languageIF.selectLanguage(languageLists.get(position).getLanguage_id(), "", position, isChecked);
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    holder.checkBox.setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                }
            }
        });

    }

    public void clearCheckBox() {
        isSelectedAll = false;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return languageLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private MaterialTextView textView;
        private MaterialCheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView_sl_adapter);
            checkBox = itemView.findViewById(R.id.checkbox_sl_adapter);
            textView = itemView.findViewById(R.id.textView_sl_adapter);

        }
    }
}
