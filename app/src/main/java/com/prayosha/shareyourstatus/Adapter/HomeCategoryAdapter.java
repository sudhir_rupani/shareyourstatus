package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.CategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;


public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.ViewHolder> {

    private Activity activity;
    private Method method;
    private String type;
    private int row_index = -1;
    private List<CategoryList> categoryLists;

    public HomeCategoryAdapter(Activity activity, List<CategoryList> categoryLists, String type, OnClick interstitialAdView) {
        this.activity = activity;
        method = new Method(activity, interstitialAdView);
        this.type = type;
        this.categoryLists = categoryLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.home_category_adapter, parent, false);

        return new HomeCategoryAdapter.ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (categoryLists.get(position).getCategory_name().equals(activity.getResources().getString(R.string.view_all))) {
            GradientDrawable gd = new GradientDrawable(
                    GradientDrawable.Orientation.LEFT_RIGHT,
                    new int[]{activity.getResources().getColor(R.color.textView_app_color),
                            activity.getResources().getColor(R.color.textView_app_color)});
            gd.setCornerRadius(70);
            holder.relativeLayout.setBackground(gd);
            Glide.with(activity).load(activity.getResources().getDrawable(R.drawable.view_all_ic))
                    .placeholder(R.drawable.placeholder_landscape).into(holder.imageView);
        } else {

            GradientDrawable gd = new GradientDrawable(
                    GradientDrawable.Orientation.LEFT_RIGHT,
                    new int[]{Color.parseColor(categoryLists.get(position).getStart_color()), Color.parseColor(categoryLists.get(position).getEnd_color())});
            gd.setCornerRadius(70);
            holder.relativeLayout.setBackground(gd);

            Glide.with(activity).load(categoryLists.get(position).getCategory_image())
                    .placeholder(R.drawable.placeholder_landscape).into(holder.imageView);
        }

        holder.textView.setText(categoryLists.get(position).getCategory_name());

        holder.relativeLayout_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
                method.onClickData(position, categoryLists.get(position).getCategory_name(), type, "", categoryLists.get(position).getCid(), "");
            }
        });

        if (row_index == position) {
            holder.textView.setTextColor(activity.getResources().getColor(R.color.textView_homeCat_select_adapter));
        } else {
            holder.textView.setTextColor(activity.getResources().getColor(R.color.textView_homeCat_adapter));
        }

    }

    @Override
    public int getItemCount() {
        return categoryLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private MaterialTextView textView;
        private RelativeLayout relativeLayout_main, relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_homeCat_adapter);
            textView = itemView.findViewById(R.id.textView_homeCat_adapter);
            relativeLayout = itemView.findViewById(R.id.rel_homeCat_adapter);
            relativeLayout_main = itemView.findViewById(R.id.rel_main_homeCat_adapter);

        }
    }
}
