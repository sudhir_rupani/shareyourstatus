package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.CategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Activity activity;
    private Method method;
    private int columnWidth;
    private String string;
    private List<CategoryList> categoryLists;

    public CategoryAdapter(Activity activity, List<CategoryList> categoryLists, String string, OnClick interstitialAdView) {
        this.activity = activity;
        method = new Method(activity, interstitialAdView);
        columnWidth = (method.getScreenWidth());
        this.string = string;
        this.categoryLists = categoryLists;
        Resources r = activity.getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, r.getDisplayMetrics());
        columnWidth = (int) ((method.getScreenWidth() - ((10 + 3) * padding)));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.category_adapter, parent, false);

        return new CategoryAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        int image_height = columnWidth / 4;

        LinearLayout.LayoutParams ll_layoutParams = new LinearLayout.LayoutParams(columnWidth / 2, columnWidth / 3);
        ll_layoutParams.gravity = Gravity.CENTER;
        holder.linearLayout.setLayoutParams(ll_layoutParams);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(columnWidth / 4, columnWidth / 4);
        holder.imageView.setLayoutParams(layoutParams);

        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[]{Color.parseColor(categoryLists.get(position).getStart_color()), Color.parseColor(categoryLists.get(position).getEnd_color())});
        gd.setCornerRadius(50); 
        holder.linearLayout.setBackgroundDrawable(gd);

        Glide.with(activity).load(categoryLists.get(position).getCategory_image())
                .placeholder(R.drawable.placeholder_landscape).into(holder.imageView);

        holder.textView.setText(categoryLists.get(position).getCategory_name());

        holder.linearLayout_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method.onClickData(position, categoryLists.get(position).getCategory_name(), string, "", categoryLists.get(position).getCid(), "");
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private MaterialTextView textView;
        private LinearLayout linearLayout_main, linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_cat_adapter);
            textView = itemView.findViewById(R.id.textView_cat_adapter);
            linearLayout = itemView.findViewById(R.id.ll_cat_adapter);
            linearLayout_main = itemView.findViewById(R.id.ll_main_cat_adapter);

        }
    }
}
