package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.EnchantedViewPager;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class SliderAdapter extends PagerAdapter {

    private Method method;
    private Activity activity;
    private String type;
    private int columnWidth;
    private LayoutInflater inflater;
    private List<SubCategoryList> subCategoryLists;

    public SliderAdapter(Activity activity, String type, List<SubCategoryList> subCategoryLists, OnClick onClick) {
        this.activity = activity;
        this.subCategoryLists = subCategoryLists;
        this.type = type;
        inflater = activity.getLayoutInflater();
        method = new Method(activity, onClick);
        columnWidth = (method.getScreenWidth());
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        if (subCategoryLists.get(position).getStatus_type().equals("quote")) {

            View view = inflater.inflate(R.layout.slider_quotes_adapter, container, false);

            view.setTag(EnchantedViewPager.ENCHANTED_VIEWPAGER_POSITION + position);

            MaterialTextView textView = view.findViewById(R.id.textView_slider_quotes);
            MaterialTextView textView_view = view.findViewById(R.id.textView_view_slider_quotes);
            RelativeLayout relativeLayout_ = view.findViewById(R.id.rel_slider_quotes);
            RelativeLayout rel_quote = view.findViewById(R.id.rel_quote_slider_quotes);
            MaterialCardView cardView = view.findViewById(R.id.cardView_main_slider_quotes);

            textView_view.setText(subCategoryLists.get(position).getTotal_viewer());
            relativeLayout_.setBackgroundColor(Color.parseColor(subCategoryLists.get(position).getQuote_bg()));

            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "text_font/" + subCategoryLists.get(position).getQuote_font());
            textView.setTypeface(typeface);

            textView.setText(subCategoryLists.get(position).getStatus_title());
            textView.post(new Runnable() {
                @Override
                public void run() {
                    ViewGroup.LayoutParams params = textView.getLayoutParams();
                    if (params == null) {
                        params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    }
                    final int widthSpec = View.MeasureSpec.makeMeasureSpec(textView.getWidth(), View.MeasureSpec.UNSPECIFIED);
                    final int heightSpec = View.MeasureSpec.makeMeasureSpec(textView.getHeight(), View.MeasureSpec.UNSPECIFIED);
                    textView.measure(widthSpec, heightSpec);
                    textView.setMaxLines(heightSpec / textView.getLineHeight());
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                }
            });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, subCategoryLists.get(position).getStatus_title(), type, subCategoryLists.get(position).getStatus_type(), subCategoryLists.get(position).getId(), "");
                }
            });

            container.addView(view, 0);
            return view;

        } else {

            View view = inflater.inflate(R.layout.slider_adapter, container, false);

            assert view != null;
            MaterialTextView textView_title = view.findViewById(R.id.textView_title_slider);
            MaterialTextView textView_view = view.findViewById(R.id.textView_view_slider);
            ImageView imageView = view.findViewById(R.id.imageView_slider_adapter);
            ImageView imageView_type = view.findViewById(R.id.imageView_type_slider);
            LinearLayout linearLayout = view.findViewById(R.id.ll_slider);

            view.setTag(EnchantedViewPager.ENCHANTED_VIEWPAGER_POSITION + position);

            String status_type = subCategoryLists.get(position).getStatus_type();
            if (status_type.equals("external")) {
                linearLayout.setVisibility(View.GONE);
            } else {
                linearLayout.setVisibility(View.VISIBLE);
            }

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape).into(imageView);
            } else {
                Glide.with(activity).load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape).into(imageView);
            }

            String type_layout = subCategoryLists.get(position).getStatus_layout();
            if (type_layout.equals("Portrait")) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(columnWidth / 2 - 60, ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                imageView.setLayoutParams(layoutParams);
            }

            switch (status_type) {
                case "video":
                    imageView_type.setVisibility(View.VISIBLE);
                    imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
                    break;
                case "image":
                    imageView_type.setVisibility(View.VISIBLE);
                    imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
                    break;
                case "gif":
                    imageView_type.setVisibility(View.VISIBLE);
                    imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
                    break;
                case "external":
                    imageView_type.setVisibility(View.GONE);
                    break;
            }

            textView_title.setText(subCategoryLists.get(position).getStatus_title());
            textView_view.setText(subCategoryLists.get(position).getTotal_viewer());

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (status_type.equals("external")) {
                        try {
                            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(subCategoryLists.get(position).getExternal_link())));
                        } catch (Exception e) {
                            method.alertBox(activity.getResources().getString(R.string.wrong));
                        }
                    } else {
                        method.onClickData(position, subCategoryLists.get(position).getStatus_title(), type, subCategoryLists.get(position).getStatus_type(), subCategoryLists.get(position).getId(), "");
                    }
                }
            });

            container.addView(view, 0);
            return view;

        }

    }

    @Override
    public int getCount() {
        return subCategoryLists.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        (container).removeView((View) object);
    }
}

