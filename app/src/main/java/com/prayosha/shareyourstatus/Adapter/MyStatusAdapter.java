package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MyStatusAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Method method;
    private int columnWidth;
    private String userId, type;
    private ProgressDialog progressDialog;
    private List<SubCategoryList> myVideoLists;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_QUOTES = 2;

    public MyStatusAdapter(Activity activity, List<SubCategoryList> myVideoLists, String userId, String type, OnClick interstitialAdView) {
        this.activity = activity;
        method = new Method(activity, interstitialAdView);
        columnWidth = (method.getScreenWidth());
        this.userId = userId;
        this.type = type;
        this.myVideoLists = myVideoLists;
        progressDialog = new ProgressDialog(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.my_status_adapter, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_QUOTES) {
            View v = LayoutInflater.from(activity).inflate(R.layout.my_quotes_adapter, parent, false);
            return new Quotes(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof ViewHolder) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            String status_type = myVideoLists.get(position).getStatus_type();
            if (status_type.equals("video")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
            } else if (status_type.equals("image")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
            } else {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
            }

            if (myVideoLists.get(position).getAlready_like().equals("true")) {
                viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_hov));
            } else {
                if (method.isDarkMode()) {
                    viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_white));
                } else {
                    viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_ic));
                }
            }

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(myVideoLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape)
                        .into(viewHolder.imageView);
            } else {
                Glide.with(activity).load(myVideoLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape)
                        .into(viewHolder.imageView);
            }

            if (userId.equals(method.pref.getString(method.profileId, null))) {
                viewHolder.imageView_delete.setVisibility(View.VISIBLE);
                viewHolder.cardView_review.setVisibility(View.VISIBLE);
                if (myVideoLists.get(position).getIs_reviewed().equals("true")) {
                    viewHolder.textView_review.setText(activity.getResources().getString(R.string.approved));
                } else {
                    viewHolder.textView_review.setText(activity.getResources().getString(R.string.on_review));
                }
            } else {
                viewHolder.imageView_delete.setVisibility(View.GONE);
                viewHolder.cardView_review.setVisibility(View.GONE);
            }

            viewHolder.imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));

            viewHolder.textView_title.setText(myVideoLists.get(position).getStatus_title());
            viewHolder.textView_sub_title.setText(myVideoLists.get(position).getCategory_name());
            viewHolder.textView_like.setText(method.format(Double.parseDouble(myVideoLists.get(position).getTotal_likes())));
            viewHolder.textView_view.setText(method.format(Double.parseDouble(myVideoLists.get(position).getTotal_viewer())));

            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, myVideoLists.get(position).getStatus_title(), type, myVideoLists.get(position).getStatus_type(), myVideoLists.get(position).getId(), "");
                }
            });

            viewHolder.imageView_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delete(position);
                }
            });

        } else if (holder.getItemViewType() == VIEW_TYPE_QUOTES) {

            final Quotes quotes = (Quotes) holder;

            if (myVideoLists.get(position).getAlready_like().equals("true")) {
                quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_hov));
            } else {
                if (method.isDarkMode()) {
                    quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_white));
                } else {
                    quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_ic));
                }
            }

            if (userId.equals(method.pref.getString(method.profileId, null))) {
                quotes.imageView_delete.setVisibility(View.VISIBLE);
                quotes.cardView_review.setVisibility(View.VISIBLE);
                if (myVideoLists.get(position).getIs_reviewed().equals("true")) {
                    quotes.textView_review.setText(activity.getResources().getString(R.string.approved));
                } else {
                    quotes.textView_review.setText(activity.getResources().getString(R.string.on_review));
                }
            } else {
                quotes.imageView_delete.setVisibility(View.GONE);
                quotes.cardView_review.setVisibility(View.GONE);
            }

            quotes.relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, columnWidth / 2));

            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "text_font/" + myVideoLists.get(position).getQuote_font());
            quotes.textView.setTypeface(typeface);

            quotes.textView.setText(myVideoLists.get(position).getStatus_title());
            quotes.textView.post(new Runnable() {
                @Override
                public void run() {
                    ViewGroup.LayoutParams params = quotes.textView.getLayoutParams();
                    if (params == null) {
                        params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    }
                    final int widthSpec = View.MeasureSpec.makeMeasureSpec(quotes.textView.getWidth(), View.MeasureSpec.UNSPECIFIED);
                    final int heightSpec = View.MeasureSpec.makeMeasureSpec(quotes.textView.getHeight(), View.MeasureSpec.UNSPECIFIED);
                    quotes.textView.measure(widthSpec, heightSpec);
                    quotes.textView.setMaxLines(heightSpec / quotes.textView.getLineHeight());
                    quotes.textView.setEllipsize(TextUtils.TruncateAt.END);
                }
            });

            quotes.textView_category.setText(myVideoLists.get(position).getCategory_name());

            quotes.relativeLayout.setBackgroundColor(Color.parseColor(myVideoLists.get(position).getQuote_bg()));

            quotes.textView_view.setText(method.format(Double.parseDouble(myVideoLists.get(position).getTotal_viewer())));
            quotes.textView_like.setText(method.format(Double.parseDouble(myVideoLists.get(position).getTotal_likes())));

            quotes.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, myVideoLists.get(position).getStatus_title(), type, myVideoLists.get(position).getStatus_type(), myVideoLists.get(position).getId(), "");
                }
            });

            quotes.imageView_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delete(position);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        if (myVideoLists.size() != 0) {
            return myVideoLists.size() + 1;
        } else {
            return myVideoLists.size();
        }
    }

    public void hideHeader() {
        ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    @Override
    public int getItemViewType(int position) {

        if (position != myVideoLists.size()) {
            if (myVideoLists.get(position).getStatus_type().equals("quote")) {
                return VIEW_TYPE_QUOTES;
            } else {
                return VIEW_TYPE_ITEM;
            }
        } else {
            return VIEW_TYPE_LOADING;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageView;
        private MaterialCardView cardView_review;
        private ImageView imageView_type, imageView_like, imageView_delete;
        private MaterialTextView textView_title, textView_like, textView_sub_title, textView_view, textView_review;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_myStatus_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_myStatus_adapter);
            imageView_like = itemView.findViewById(R.id.imageView_like_myStatus_adapter);
            imageView_delete = itemView.findViewById(R.id.imageView_delete_myStatus_adapter);
            textView_title = itemView.findViewById(R.id.textView_title_myStatus_adapter);
            textView_sub_title = itemView.findViewById(R.id.textView_cat_myStatus_adapter);
            textView_view = itemView.findViewById(R.id.textView_view_myStatus_adapter);
            textView_like = itemView.findViewById(R.id.textView_like_myStatus_adapter);
            textView_review = itemView.findViewById(R.id.textView_review_myStatus_adapter);
            cardView_review = itemView.findViewById(R.id.cardView_review_myStatus_adapter);

        }
    }

    public class Quotes extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;
        private MaterialCardView cardView_review;
        private ImageView imageView_delete, imageView_like;
        private RelativeLayout relativeLayout;
        private MaterialTextView textView, textView_category, textView_view, textView_like, textView_review;

        public Quotes(@NonNull View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.ll_my_quotes_adapter);
            textView = itemView.findViewById(R.id.textView_my_quotes_adapter);
            textView_category = itemView.findViewById(R.id.textView_cat_my_quotes_adapter);
            relativeLayout = itemView.findViewById(R.id.rel_my_quotes_adapter);
            textView_view = itemView.findViewById(R.id.textView_view_my_quotes_adapter);
            textView_like = itemView.findViewById(R.id.textView_like_my_quotes_adapter);
            imageView_like = itemView.findViewById(R.id.imageView_like_my_quotes_adapter);
            imageView_delete = itemView.findViewById(R.id.imageView_delete_my_quotes_adapter);
            textView_review = itemView.findViewById(R.id.textView_review_my_quotes_adapter);
            cardView_review = itemView.findViewById(R.id.cardView_review_my_quotes_adapter);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public static ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

    private void delete(int position) {
        if (method.isNetworkAvailable()) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
            builder.setMessage(activity.getResources().getString(R.string.delete_msg));
            builder.setCancelable(false);
            builder.setPositiveButton(activity.getResources().getString(R.string.delete),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            delete_video(position, userId);
                        }
                    });
            builder.setNegativeButton(activity.getResources().getString(R.string.cancel_dialog),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();

        } else {
            method.alertBox(activity.getResources().getString(R.string.internet_connection));
        }
    }

    private void delete_video(final int position, String userId) {

        progressDialog.setMessage(activity.getResources().getString(R.string.delete));
        progressDialog.setCancelable(false);
        progressDialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(activity));
        jsObj.addProperty("method_name", "user_status_delete");
        jsObj.addProperty("user_id", userId);
        jsObj.addProperty("post_id", myVideoLists.get(position).getId());
        jsObj.addProperty("type", myVideoLists.get(position).getStatus_type());
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                        String msg = object.getString("msg");
                        String success = object.getString("success");

                        if (success.equals("1")) {
                            myVideoLists.remove(position);
                            notifyDataSetChanged();
                        }

                        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(activity.getResources().getString(R.string.failed_try_again));
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                method.alertBox(activity.getResources().getString(R.string.failed_try_again));
            }
        });
    }

}
