package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Item.CommentList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllCommentAdapter extends RecyclerView.Adapter {

    private Method method;
    private Animation myAnim;
    private Activity activity;
    private ProgressDialog progressDialog;
    private List<CommentList> commentLists;

    private final int VIEW_TYPE_LOADING = 0;
    private final int VIEW_TYPE_ITEM = 1;

    public AllCommentAdapter(Activity activity, List<CommentList> commentLists) {
        this.activity = activity;
        this.commentLists = commentLists;
        method = new Method(activity);
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
        progressDialog = new ProgressDialog(activity);
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.comment_adapter, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, final int position) {

        if (holder.getItemViewType() == VIEW_TYPE_ITEM) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            if (!commentLists.get(position).getUser_image().equals("")) {
                Glide.with(activity).load(commentLists.get(position).getUser_image())
                        .placeholder(R.drawable.profile)
                        .into(viewHolder.circleImageView);
            } else {
                // make sure Glide doesn't load anything into this view until told otherwise
                Glide.with(activity).clear(viewHolder.circleImageView);
            }

            if (method.pref.getBoolean(method.pref_login, false)) {
                if (method.pref.getString(method.profileId, null).equals(commentLists.get(position).getUser_id())) {
                    viewHolder.textView_delete.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.textView_delete.setVisibility(View.GONE);
                }
            } else {
                viewHolder.textView_delete.setVisibility(View.GONE);
            }

            viewHolder.textView_Name.setText(commentLists.get(position).getUser_name());
            viewHolder.textView_date.setText(commentLists.get(position).getComment_date());
            viewHolder.textView_comment.setText(commentLists.get(position).getComment_text());

            viewHolder.textView_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    viewHolder.textView_delete.startAnimation(myAnim);

                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
                    builder.setMessage(activity.getResources().getString(R.string.delete_comment));
                    builder.setCancelable(false);
                    builder.setPositiveButton(activity.getResources().getString(R.string.delete),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    delete(commentLists.get(position).getComment_post_id(), commentLists.get(position).getComment_status_type(),
                                            commentLists.get(position).getComment_id(), position);
                                }
                            });
                    builder.setNegativeButton(activity.getResources().getString(R.string.cancel_dialog),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        if (commentLists.size() != 0) {
            return commentLists.size() + 1;
        } else {
            return commentLists.size();
        }
    }

    public void hideHeader() {
        ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    private boolean isHeader(int position) {
        return position == commentLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView circleImageView;
        private MaterialTextView textView_Name, textView_date, textView_comment, textView_delete;

        public ViewHolder(View itemView) {
            super(itemView);

            circleImageView = itemView.findViewById(R.id.imageView_comment_adapter);
            textView_Name = itemView.findViewById(R.id.textView_userName_comment_adapter);
            textView_date = itemView.findViewById(R.id.textView_date_comment_adapter);
            textView_comment = itemView.findViewById(R.id.textView_comment_adapter);
            textView_delete = itemView.findViewById(R.id.textView_delete_adapter);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public static ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

    public void delete(String post_id, String status_type, String comment_id, int position) {

        progressDialog.show();
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(activity));
        jsObj.addProperty("method_name", "delete_comment");
        jsObj.addProperty("post_id", post_id);
        jsObj.addProperty("type", status_type);
        jsObj.addProperty("comment_id", comment_id);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        String msg = jsonObject.getString("msg");
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {

                            commentLists.remove(position);

                            String status = jsonObject.getString("comment_status");
                            String total_comment = jsonObject.getString("total_comment");

                            notifyDataSetChanged();

                            Events.DeleteComment deleteComment = new Events.DeleteComment(total_comment, post_id, comment_id, "all_comment");
                            GlobalBus.getBus().post(deleteComment);

                        } else {
                            method.alertBox(msg);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(activity.getResources().getString(R.string.failed_try_again));
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                method.alertBox(activity.getResources().getString(R.string.failed_try_again));
            }
        });

    }

}
