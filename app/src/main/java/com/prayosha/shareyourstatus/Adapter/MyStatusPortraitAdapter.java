package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MyStatusPortraitAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Method method;
    private String userId, type;
    private ProgressDialog progressDialog;
    private List<SubCategoryList> myVideoLists;

    private final int VIEW_TYPE_LOADING = 0;
    private final int VIEW_TYPE_ITEM = 1;

    public MyStatusPortraitAdapter(Activity activity, List<SubCategoryList> myVideoLists, String userId, String type, OnClick interstitialAdView) {
        this.activity = activity;
        method = new Method(activity, interstitialAdView);
        this.userId = userId;
        this.type = type;
        this.myVideoLists = myVideoLists;
        progressDialog = new ProgressDialog(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.my_status_por_adapter, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof ViewHolder) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            String status_type = myVideoLists.get(position).getStatus_type();
            if (status_type.equals("video")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
            } else if (status_type.equals("image")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
            } else {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
            }

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(myVideoLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape)
                        .into(viewHolder.imageView);
            } else {
                Glide.with(activity).load(myVideoLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape)
                        .into(viewHolder.imageView);
            }

            if (userId.equals(method.pref.getString(method.profileId, null))) {
                viewHolder.relativeLayout.setVisibility(View.VISIBLE);
                viewHolder.cardView_review.setVisibility(View.VISIBLE);
                if (myVideoLists.get(position).getIs_reviewed().equals("true")) {
                    viewHolder.textView_review.setText(activity.getResources().getString(R.string.approved));
                } else {
                    viewHolder.textView_review.setText(activity.getResources().getString(R.string.on_review));
                }
            } else {
                viewHolder.relativeLayout.setVisibility(View.GONE);
                viewHolder.cardView_review.setVisibility(View.GONE);
            }

            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, myVideoLists.get(position).getStatus_title(), type, myVideoLists.get(position).getStatus_type(), myVideoLists.get(position).getId(), "");
                }
            });

            viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
                    builder.setMessage(activity.getResources().getString(R.string.delete_msg));
                    builder.setCancelable(false);
                    builder.setPositiveButton(activity.getResources().getString(R.string.delete),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    if (method.isNetworkAvailable()) {
                                        delete_video(position, userId);
                                    } else {
                                        method.alertBox(activity.getResources().getString(R.string.internet_connection));
                                    }
                                }
                            });
                    builder.setNegativeButton(activity.getResources().getString(R.string.cancel_dialog),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        if (myVideoLists.size() != 0) {
            return myVideoLists.size() + 1;
        } else {
            return myVideoLists.size();
        }
    }

    public void hideHeader() {
        ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    private boolean isHeader(int position) {
        return position == myVideoLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageView;
        private ImageView imageView_type, imageView_delete;
        private RelativeLayout relativeLayout;
        private MaterialTextView textView_review;
        private MaterialCardView cardView_review;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_myPorStatus_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_myPorStatus_adapter);
            imageView_delete = itemView.findViewById(R.id.imageView_delete_myPorStatus_adapter);
            relativeLayout = itemView.findViewById(R.id.relativeLayout_myPorStatus_adapter);
            textView_review = itemView.findViewById(R.id.textView_review_myPorStatus_adapter);
            cardView_review = itemView.findViewById(R.id.cardView_review_myPorStatus_adapter);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public static ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

    private void delete_video(final int position, String userId) {

        progressDialog.setMessage(activity.getResources().getString(R.string.delete));
        progressDialog.setCancelable(false);
        progressDialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(activity));
        jsObj.addProperty("method_name", "user_status_delete");
        jsObj.addProperty("user_id", userId);
        jsObj.addProperty("post_id", myVideoLists.get(position).getId());
        jsObj.addProperty("type", myVideoLists.get(position).getStatus_type());
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                        String msg = object.getString("msg");
                        String success = object.getString("success");

                        if (success.equals("1")) {
                            myVideoLists.remove(position);
                            notifyDataSetChanged();
                        }

                        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();

                    }

                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    method.alertBox(activity.getResources().getString(R.string.failed_try_again));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                method.alertBox(activity.getResources().getString(R.string.failed_try_again));
            }
        });
    }

}
