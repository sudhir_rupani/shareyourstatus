package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;


public class RelatedScdAdapter extends RecyclerView.Adapter {

    private Method method;
    private Activity activity;
    private String type;
    private Animation myAnim;
    private List<SubCategoryList> relatedLists;

    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_QUOTES = 2;

    public RelatedScdAdapter(Activity activity, List<SubCategoryList> relatedLists, OnClick interstitialAdView, String type) {
        this.activity = activity;
        this.type = type;
        this.relatedLists = relatedLists;
        method = new Method(activity, interstitialAdView);
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.related_adapter, parent, false);
            return new RelatedScdAdapter.ViewHolder(view);
        } else if (viewType == VIEW_TYPE_QUOTES) {
            View v = LayoutInflater.from(activity).inflate(R.layout.related_quotes_adapter, parent, false);
            return new RelatedScdAdapter.Quotes(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder.getItemViewType() == VIEW_TYPE_ITEM) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            String status_type = relatedLists.get(position).getStatus_type();
            if (status_type.equals("video")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
            } else if (status_type.equals("image")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
            } else {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
            }

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(relatedLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_portable).into(viewHolder.imageView);
            } else {
                Glide.with(activity).load(relatedLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_portable).into(viewHolder.imageView);
            }

            if (relatedLists.get(position).getIs_favourite().equals("true")) {
                viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                if (method.isDarkMode()) {
                    viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                } else {
                    viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                }
            }

            viewHolder.textView_title.setText(relatedLists.get(position).getStatus_title());
            viewHolder.textView_subTitle.setText(relatedLists.get(position).getCategory_name());

            viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, relatedLists.get(position).getStatus_title(), type, relatedLists.get(position).getStatus_type(), relatedLists.get(position).getId(), "");
                }
            });

            viewHolder.imageView_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.imageView_favourite.startAnimation(myAnim);
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        FavouriteIF favouriteIF = new FavouriteIF() {
                            @Override
                            public void isFavourite(String isFavourite, String message) {
                                if (!isFavourite.equals("")) {
                                    if (isFavourite.equals("true")) {
                                        viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                                        } else {
                                            viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                        }
                                    }
                                    Events.FavouriteNotify homeNotify = new Events.FavouriteNotify(relatedLists.get(position).getId(), relatedLists.get(position).getStatus_layout(), isFavourite, relatedLists.get(position).getStatus_type());
                                    GlobalBus.getBus().post(homeNotify);
                                }
                            }
                        };
                        method.addToFav(relatedLists.get(position).getId(), method.pref.getString(method.profileId, ""), relatedLists.get(position).getStatus_type(), favouriteIF);
                    } else {
                        Method.loginBack = true;
                        activity.startActivity(new Intent(activity, Login.class));
                    }
                }
            });

        } else if (holder.getItemViewType() == VIEW_TYPE_QUOTES) {

            final Quotes quotes = (Quotes) holder;

            if (relatedLists.get(position).getIs_favourite().equals("true")) {
                quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                if (method.isDarkMode()) {
                    quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                } else {
                    quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                }
            }

            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "text_font/" + relatedLists.get(position).getQuote_font());
            quotes.textView.setTypeface(typeface);
            quotes.textView.setText(relatedLists.get(position).getStatus_title());

            quotes.textView_category.setText(relatedLists.get(position).getCategory_name());

            quotes.relativeLayout.setBackgroundColor(Color.parseColor(relatedLists.get(position).getQuote_bg()));

            quotes.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, relatedLists.get(position).getStatus_title(), type, relatedLists.get(position).getStatus_type(), relatedLists.get(position).getId(), "");
                }
            });

            quotes.imageView_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quotes.imageView_favourite.startAnimation(myAnim);
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        FavouriteIF favouriteIF = new FavouriteIF() {
                            @Override
                            public void isFavourite(String isFavourite, String message) {
                                if (!isFavourite.equals("")) {
                                    if (isFavourite.equals("true")) {
                                        quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                                        } else {
                                            quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                        }
                                    }
                                    Events.FavouriteNotify homeNotify = new Events.FavouriteNotify(relatedLists.get(position).getId(), relatedLists.get(position).getStatus_layout(), isFavourite, relatedLists.get(position).getStatus_type());
                                    GlobalBus.getBus().post(homeNotify);
                                }
                            }
                        };
                        method.addToFav(relatedLists.get(position).getId(), method.pref.getString(method.profileId, ""), relatedLists.get(position).getStatus_type(), favouriteIF);
                    } else {
                        Method.loginBack = true;
                        activity.startActivity(new Intent(activity, Login.class));
                    }
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return relatedLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (relatedLists.get(position).getStatus_type().equals("quote")) {
            return VIEW_TYPE_QUOTES;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageView;
        private ImageView imageView_type, imageView_favourite;
        private MaterialTextView textView_title, textView_subTitle;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeLayout = itemView.findViewById(R.id.linearLayout_home_adapter);
            imageView = itemView.findViewById(R.id.imageView_home_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_home_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_home_adapter);
            textView_title = itemView.findViewById(R.id.textView_title_home_adapter);
            textView_subTitle = itemView.findViewById(R.id.textView_subtitle_home_adapter);

        }
    }

    public class Quotes extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;
        private ImageView imageView_favourite;
        private RelativeLayout relativeLayout;
        private MaterialTextView textView, textView_category;

        public Quotes(@NonNull View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.ll_rel_quotes_adapter);
            textView = itemView.findViewById(R.id.textView_relQuotes_adapter);
            textView_category = itemView.findViewById(R.id.textView_cat_relQuotes_adapter);
            relativeLayout = itemView.findViewById(R.id.rel_relQuotes_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_relQuotes_adapter);

        }
    }

}
