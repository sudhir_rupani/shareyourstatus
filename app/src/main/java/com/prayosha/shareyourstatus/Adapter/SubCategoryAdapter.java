package com.prayosha.shareyourstatus.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;


public class SubCategoryAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private Method method;
    private int columnWidth;
    private String type;
    private Animation myAnim;
    private List<SubCategoryList> subCategoryLists;

    private final int VIEW_TYPE_LOADING = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_QUOTES = 2;

    public SubCategoryAdapter(Activity activity, List<SubCategoryList> subCategoryLists, OnClick interstitialAdView, String type) {
        this.activity = activity;
        this.type = type;
        this.subCategoryLists = subCategoryLists;
        method = new Method(activity, interstitialAdView);
        columnWidth = (method.getScreenWidth());
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.sub_category_adapter, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_QUOTES) {
            View v = LayoutInflater.from(activity).inflate(R.layout.quotes_adapter, parent, false);
            return new Quotes(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            return new ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        if (holder.getItemViewType() == VIEW_TYPE_ITEM) {

            final ViewHolder viewHolder = (ViewHolder) holder;

            String status_type = subCategoryLists.get(position).getStatus_type();
            if (status_type.equals("video")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
            } else if (status_type.equals("image")) {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
            } else {
                viewHolder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
            }

            if (subCategoryLists.get(position).getIs_favourite().equals("true")) {
                viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                if (method.isDarkMode()) {
                    viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                } else {
                    viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                }
            }

            if (subCategoryLists.get(position).getAlready_like().equals("true")) {
                viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_hov));
            } else {
                if (method.isDarkMode()) {
                    viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_white));
                } else {
                    viewHolder.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_ic));
                }
            }

            viewHolder.imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2 - 60));

            if (status_type.equals("gif")) {
                Glide.with(activity)
                        .asBitmap()
                        .load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape).into(viewHolder.imageView);

            } else {
                Glide.with(activity)
                        .load(subCategoryLists.get(position).getStatus_thumbnail_s())
                        .placeholder(R.drawable.placeholder_landscape).into(viewHolder.imageView);
            }


            viewHolder.textView_title.setText(subCategoryLists.get(position).getStatus_title());
            viewHolder.textView_subTitle.setText(subCategoryLists.get(position).getCategory_name());
            viewHolder.textView_view.setText(method.format(Double.parseDouble(subCategoryLists.get(position).getTotal_viewer())));
            viewHolder.textView_like.setText(method.format(Double.parseDouble(subCategoryLists.get(position).getTotal_likes())));

            viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, subCategoryLists.get(position).getStatus_title(), type, subCategoryLists.get(position).getStatus_type(), subCategoryLists.get(position).getId(), "");
                }
            });

            viewHolder.imageView_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.imageView_favourite.startAnimation(myAnim);
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        FavouriteIF favouriteIF = new FavouriteIF() {
                            @Override
                            public void isFavourite(String isFavourite, String message) {
                                if (!isFavourite.equals("")) {
                                    if (isFavourite.equals("true")) {
                                        viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                                        } else {
                                            viewHolder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                        }
                                    }
                                }
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            }
                        };
                        method.addToFav(subCategoryLists.get(position).getId(), method.pref.getString(method.profileId, ""), subCategoryLists.get(position).getStatus_type(), favouriteIF);
                    } else {
                        Method.loginBack = true;
                        activity.startActivity(new Intent(activity, Login.class));
                    }
                }
            });

        } else if (holder.getItemViewType() == VIEW_TYPE_QUOTES) {

            final Quotes quotes = (Quotes) holder;

            if (subCategoryLists.get(position).getIs_favourite().equals("true")) {
                quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                if (method.isDarkMode()) {
                    quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                } else {
                    quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                }
            }

            if (subCategoryLists.get(position).getAlready_like().equals("true")) {
                quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_hov));
            } else {
                if (method.isDarkMode()) {
                    quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_white));
                } else {
                    quotes.imageView_like.setImageDrawable(activity.getResources().getDrawable(R.drawable.like_ic));
                }
            }

            quotes.relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, columnWidth / 2));

            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "text_font/" + subCategoryLists.get(position).getQuote_font());
            quotes.textView.setTypeface(typeface);

            quotes.textView.setText(subCategoryLists.get(position).getStatus_title());
            quotes.textView.post(new Runnable() {
                @Override
                public void run() {
                    ViewGroup.LayoutParams params = quotes.textView.getLayoutParams();
                    if (params == null) {
                        params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    }
                    final int widthSpec = View.MeasureSpec.makeMeasureSpec(quotes.textView.getWidth(), View.MeasureSpec.UNSPECIFIED);
                    final int heightSpec = View.MeasureSpec.makeMeasureSpec(quotes.textView.getHeight(), View.MeasureSpec.UNSPECIFIED);
                    quotes.textView.measure(widthSpec, heightSpec);
                    quotes.textView.setMaxLines(heightSpec / quotes.textView.getLineHeight());
                    quotes.textView.setEllipsize(TextUtils.TruncateAt.END);
                }
            });

            quotes.textView_category.setText(subCategoryLists.get(position).getCategory_name());

            quotes.relativeLayout.setBackgroundColor(Color.parseColor(subCategoryLists.get(position).getQuote_bg()));

            quotes.textView_view.setText(method.format(Double.parseDouble(subCategoryLists.get(position).getTotal_viewer())));
            quotes.textView_like.setText(method.format(Double.parseDouble(subCategoryLists.get(position).getTotal_likes())));

            quotes.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    method.onClickData(position, subCategoryLists.get(position).getStatus_title(), type, subCategoryLists.get(position).getStatus_type(), subCategoryLists.get(position).getId(), "");
                }
            });

            quotes.imageView_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quotes.imageView_favourite.startAnimation(myAnim);
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        FavouriteIF favouriteIF = new FavouriteIF() {
                            @Override
                            public void isFavourite(String isFavourite, String message) {
                                if (!isFavourite.equals("")) {
                                    if (isFavourite.equals("true")) {
                                        quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.fav_white_ic));
                                        } else {
                                            quotes.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                        }
                                    }
                                }
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            }
                        };
                        method.addToFav(subCategoryLists.get(position).getId(), method.pref.getString(method.profileId, ""), subCategoryLists.get(position).getStatus_type(), favouriteIF);
                    } else {
                        Method.loginBack = true;
                        activity.startActivity(new Intent(activity, Login.class));
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return subCategoryLists.size() + 1;
    }

    public void hideHeader() {
        ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    @Override
    public int getItemViewType(int position) {

        if (position != subCategoryLists.size()) {
            if (subCategoryLists.get(position).getStatus_type().equals("quote")) {
                return VIEW_TYPE_QUOTES;
            } else {
                return VIEW_TYPE_ITEM;
            }
        } else {
            return VIEW_TYPE_LOADING;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relativeLayout;
        private ImageView imageView, imageView_type, imageView_favourite, imageView_like;
        private MaterialTextView textView_title, textView_subTitle, textView_view, textView_like;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeLayout = itemView.findViewById(R.id.relativeLayout_subCat_adapter);
            imageView = itemView.findViewById(R.id.imageView_subCat_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_subCat_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_subCat_adapter);
            imageView_like = itemView.findViewById(R.id.imageView_like_subCat_adapter);
            textView_title = itemView.findViewById(R.id.textView_title_subCat_adapter);
            textView_subTitle = itemView.findViewById(R.id.textView_cat_subCat_adapter);
            textView_view = itemView.findViewById(R.id.textView_view_subCat_adapter);
            textView_like = itemView.findViewById(R.id.textView_like_subCategory_adapter);

        }
    }

    public class Quotes extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;
        private ImageView imageView_favourite, imageView_like;
        private RelativeLayout relativeLayout;
        private MaterialTextView textView, textView_category, textView_view, textView_like;

        public Quotes(@NonNull View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.ll_quotes_adapter);
            textView = itemView.findViewById(R.id.textView_quotes_adapter);
            textView_category = itemView.findViewById(R.id.textView_cat_quotes_adapter);
            relativeLayout = itemView.findViewById(R.id.rel_quotes_adapter);
            textView_view = itemView.findViewById(R.id.textView_view_quotes_adapter);
            textView_like = itemView.findViewById(R.id.textView_like_quotes_adapter);
            imageView_like = itemView.findViewById(R.id.imageView_like_quotes_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_quotes_adapter);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public static ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }

}
