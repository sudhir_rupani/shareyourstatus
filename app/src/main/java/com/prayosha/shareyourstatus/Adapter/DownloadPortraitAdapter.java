package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.VideoPlayer;
import com.prayosha.shareyourstatus.Activity.ViewImage;
import com.prayosha.shareyourstatus.DataBase.DatabaseHandler;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.List;


public class DownloadPortraitAdapter extends RecyclerView.Adapter<DownloadPortraitAdapter.ViewHolder> {

    private Activity activity;
    private Method method;
    private DatabaseHandler db;
    private Animation myAnim;
    private List<SubCategoryList> downloadLists;

    public DownloadPortraitAdapter(Activity activity, List<SubCategoryList> subCategoryLists) {
        this.activity = activity;
        method = new Method(activity);
        db = new DatabaseHandler(activity);
        this.downloadLists = subCategoryLists;
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.download_portrait_adapter, parent, false);

        return new DownloadPortraitAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        String status_type = downloadLists.get(position).getStatus_type();
        if (status_type.equals("video")) {
            holder.imageView_play.setVisibility(View.VISIBLE);
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
        } else if (status_type.equals("image")) {
            holder.imageView_play.setVisibility(View.GONE);
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
        } else {
            holder.imageView_play.setVisibility(View.GONE);
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
        }

        if (status_type.equals("gif")) {
            Glide.with(activity)
                    .asBitmap()
                    .load("file://" + downloadLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_landscape).into(holder.imageView);
        } else {
            Glide.with(activity)
                    .load("file://" + downloadLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_landscape).into(holder.imageView);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (downloadLists.get(position).getStatus_type().equals("video")) {
                    Intent intent = new Intent(activity, VideoPlayer.class);
                    intent.putExtra("Video_url", downloadLists.get(position).getVideo_url());
                    intent.putExtra("video_type", downloadLists.get(position).getStatus_layout());
                    activity.startActivity(intent);
                } else if (downloadLists.get(position).getStatus_type().equals("gif")) {
                    activity.startActivity(new Intent(activity, ViewImage.class)
                            .putExtra("path", downloadLists.get(position).getGif_url()));
                } else {
                    activity.startActivity(new Intent(activity, ViewImage.class)
                            .putExtra("path", downloadLists.get(position).getStatus_thumbnail_b()));
                }
            }
        });

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imageView_delete.startAnimation(myAnim);
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.DialogTitleTextStyle);
                builder.setMessage(activity.getResources().getString(R.string.delete_msg));
                builder.setCancelable(false);
                builder.setPositiveButton(activity.getResources().getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                db.delete_status_download(downloadLists.get(position).getId(), downloadLists.get(position).getStatus_type());
                                File file = new File(downloadLists.get(position).getVideo_url());
                                file.delete();
                                if (downloadLists.get(position).getStatus_type().equals("gif")) {
                                    File file_image = new File(downloadLists.get(position).getGif_url());
                                    file_image.delete();
                                } else {
                                    File file_image = new File(downloadLists.get(position).getStatus_thumbnail_b());
                                    file_image.delete();
                                }
                                downloadLists.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                builder.setNegativeButton(activity.getResources().getString(R.string.cancel_dialog),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return downloadLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageView;
        private ImageView imageView_type, imageView_play, imageView_delete;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_download_porAdapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_download_porAdapter);
            imageView_play = itemView.findViewById(R.id.imageView_play_download_porAdapter);
            imageView_delete = itemView.findViewById(R.id.imageView_delete_download_porAdapter);
            relativeLayout = itemView.findViewById(R.id.relativeLayout_download_porAdapter);

        }
    }
}
