package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;


public class PortraitHomeAdapter extends RecyclerView.Adapter<PortraitHomeAdapter.ViewHolder> {

    private Method method;
    private Activity activity;
    private String type;
    private Animation myAnim;
    private List<SubCategoryList> portraitLists;

    public PortraitHomeAdapter(Activity activity, List<SubCategoryList> portraitLists, OnClick interstitialAdView, String type) {
        this.activity = activity;
        this.type = type;
        this.portraitLists = portraitLists;
        method = new Method(activity, interstitialAdView);
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.portrait_home_adapter, parent, false);

        return new PortraitHomeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String status_type = portraitLists.get(position).getStatus_type();
        if (status_type.equals("video")) {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
        } else if (status_type.equals("image")) {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
        } else {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
        }

        if (status_type.equals("gif")) {
            Glide.with(activity)
                    .asBitmap()
                    .load(portraitLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_portable).into(holder.imageView);
        } else {
            Glide.with(activity).load(portraitLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_portable).into(holder.imageView);
        }

        if (portraitLists.get(position).getIs_favourite().equals("true")) {
            holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
        } else {
            holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method.onClickData(position, portraitLists.get(position).getId(), type, portraitLists.get(position).getStatus_type(), portraitLists.get(position).getId(), "");
            }
        });

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imageView_favourite.startAnimation(myAnim);
                if (method.pref.getBoolean(method.pref_login, false)) {
                    FavouriteIF favouriteIF = new FavouriteIF() {
                        @Override
                        public void isFavourite(String isFavourite, String message) {
                            if (!isFavourite.equals("")) {
                                if (isFavourite.equals("true")) {
                                    holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                } else {
                                    holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                }
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    };
                    method.addToFav(portraitLists.get(position).getId(), method.pref.getString(method.profileId, ""), portraitLists.get(position).getStatus_type(), favouriteIF);
                } else {
                    Method.loginBack = true;
                    activity.startActivity(new Intent(activity, Login.class));
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return portraitLists.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relativeLayout;
        private RoundedImageView imageView;
        private ImageView imageView_favourite, imageView_type;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView_portrait_home_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_portrait_home_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_portrait_home_adapter);
            relativeLayout = itemView.findViewById(R.id.relativeLayout_fav_portrait_home_adapter);

        }
    }
}
