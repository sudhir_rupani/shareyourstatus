package com.prayosha.shareyourstatus.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;


public class RelatedScdPortraitAdapter extends RecyclerView.Adapter<RelatedScdPortraitAdapter.ViewHolder> {

    private Method method;
    private Activity activity;
    private String type;
    private Animation myAnim;
    private List<SubCategoryList> relatedLists;

    public RelatedScdPortraitAdapter(Activity activity, List<SubCategoryList> relatedLists, OnClick interstitialAdView, String type) {
        this.activity = activity;
        method = new Method(activity, interstitialAdView);
        this.relatedLists = relatedLists;
        this.type = type;
        myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.related_portrait_adapter, parent, false);

        return new RelatedScdPortraitAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String status_type = relatedLists.get(position).getStatus_type();
        if (status_type.equals("video")) {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.video_ic));
        } else if (status_type.equals("image")) {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.img_ic));
        } else {
            holder.imageView_type.setImageDrawable(activity.getResources().getDrawable(R.drawable.gif_ic));
        }

        if (relatedLists.get(position).getIs_favourite().equals("true")) {
            holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
        } else {
            holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
        }

        if (status_type.equals("gif")) {
            Glide.with(activity)
                    .asBitmap()
                    .load(relatedLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_portable).into(holder.imageView);
        } else {
            Glide.with(activity).load(relatedLists.get(position).getStatus_thumbnail_s())
                    .placeholder(R.drawable.placeholder_portable).into(holder.imageView);
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method.onClickData(position, relatedLists.get(position).getStatus_title(), type, relatedLists.get(position).getStatus_type(), relatedLists.get(position).getId(), "");
            }
        });

        holder.imageView_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imageView_favourite.startAnimation(myAnim);
                if (method.pref.getBoolean(method.pref_login, false)) {
                    FavouriteIF favouriteIF = new FavouriteIF() {
                        @Override
                        public void isFavourite(String isFavourite, String message) {
                            if (!isFavourite.equals("")) {
                                if (isFavourite.equals("true")) {
                                    holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav_hov));
                                } else {
                                    holder.imageView_favourite.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_fav));
                                }
                                Events.FavouriteNotify homeNotify = new Events.FavouriteNotify(relatedLists.get(position).getId(), relatedLists.get(position).getStatus_layout(), isFavourite, relatedLists.get(position).getStatus_type());
                                GlobalBus.getBus().post(homeNotify);
                            }
                        }
                    };
                    method.addToFav(relatedLists.get(position).getId(), method.pref.getString(method.profileId, ""), relatedLists.get(position).getStatus_type(), favouriteIF);
                } else {
                    Method.loginBack = true;
                    activity.startActivity(new Intent(activity, Login.class));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return relatedLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageView;
        private ImageView imageView_type, imageView_favourite;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeLayout = itemView.findViewById(R.id.relativeLayout_rel_por_adapter);
            imageView = itemView.findViewById(R.id.imageView_rel_por_adapter);
            imageView_type = itemView.findViewById(R.id.imageView_type_rel_por_adapter);
            imageView_favourite = itemView.findViewById(R.id.imageView_fav_rel_por_adapter);

        }
    }
}
