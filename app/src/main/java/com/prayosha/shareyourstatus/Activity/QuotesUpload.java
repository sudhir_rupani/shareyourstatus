package com.prayosha.shareyourstatus.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.prayosha.shareyourstatus.Adapter.SelectLanguageAdapter;
import com.prayosha.shareyourstatus.InterFace.LanguageIF;
import com.prayosha.shareyourstatus.Item.CategoryList;
import com.prayosha.shareyourstatus.Item.LanguageList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hootsuite.nachos.NachoTextView;
import com.hootsuite.nachos.terminator.ChipTerminatorHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import top.defaults.colorpicker.ColorObserver;
import top.defaults.colorpicker.ColorPickerView;

public class QuotesUpload extends AppCompatActivity {

    private Method method;
    private LanguageIF languageIF;
    private int position;
    private String categoryId, string_font;
    private int quotesColor_bg = 0x7F313C93;
    private MaterialToolbar toolbar;
    private ProgressBar progressBar;
    private List<CategoryList> categoryLists;
    private Dialog dialog;
    private Spinner spinner_cat;
    private RecyclerView recyclerView;
    private NachoTextView nachoTextView;
    private TextInputEditText editText;
    private MaterialButton button_upload;
    private LinearLayout linearLayout;
    private ConstraintLayout constraintLayout;
    private InputMethodManager imm;
    private ArrayList<String> languageIds_list;
    private List<LanguageList> languageLists;
    private SelectLanguageAdapter selectLanguageAdapter;
    private ImageView imageView_color;
    private RelativeLayout relativeLayout_textStyle;

    private String[] font = {"Anton.ttf", "Cinzel.ttf", "Lemonada.ttf", "Pacifico.ttf", "Poppins.ttf", "Roboto.ttf"};

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes_upload);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        method = new Method(QuotesUpload.this);
        method.forceRTLIfSupported();

        categoryLists = new ArrayList<>();
        languageIds_list = new ArrayList<>();
        languageLists = new ArrayList<>();

        languageIF = new LanguageIF() {
            @Override
            public void selectLanguage(String id, String type, int position, boolean isValue) {
                if (isValue) {
                    languageIds_list.add(id);
                } else {
                    new SelectLanguage().execute(id);
                }
            }
        };

        toolbar = findViewById(R.id.toolbar_qu);
        toolbar.setTitle(getResources().getString(R.string.upload_quotes));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = findViewById(R.id.progressBar_qu);
        spinner_cat = findViewById(R.id.spinner_qu);
        imageView_color = findViewById(R.id.imageView_colorSelect_qu);
        relativeLayout_textStyle = findViewById(R.id.rel_textStyle_qu);
        button_upload = findViewById(R.id.button_qu);
        editText = findViewById(R.id.editText_qu);
        recyclerView = findViewById(R.id.recyclerView_qu);
        nachoTextView = findViewById(R.id.nacho_qu);
        constraintLayout = findViewById(R.id.constrainLayout_bg_qu);

        dialog = new Dialog(QuotesUpload.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.FILL_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        ColorPickerView colorPickerView = dialog.findViewById(R.id.colorPicker_dialog_color);
        MaterialButton button_dialog = dialog.findViewById(R.id.button_dialog_color);
        colorPickerView.setInitialColor(0x7F313C93);
        colorPickerView.subscribe(new ColorObserver() {
            @Override
            public void onColor(int color, boolean fromUser, boolean shouldPropagate) {
                constraintLayout.setBackgroundColor(color);
                quotesColor_bg = color;
            }
        });

        button_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        constraintLayout.setBackgroundColor(quotesColor_bg);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(QuotesUpload.this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);

        nachoTextView.addChipTerminator(',', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_CURRENT_TOKEN);

        linearLayout = findViewById(R.id.linearLayout_qu);
        method.adView(linearLayout);

        imageView_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "text_font/" + font[position]);
        editText.setTypeface(typeface);
        string_font = font[position];

        relativeLayout_textStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position++;
                if (position > font.length - 1) {
                    position = 0;
                }
                string_font = font[position];
                Typeface typeface = Typeface.createFromAsset(getAssets(), "text_font/" + font[position]);
                editText.setTypeface(typeface);
            }
        });

        button_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });


        if (method.isNetworkAvailable()) {
            category_language();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    @SuppressLint("StaticFieldLeak")
    class SelectLanguage extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            for (int i = 0; i < languageIds_list.size(); i++) {
                if (languageIds_list.get(i).equals(strings[0])) {
                    languageIds_list.remove(i);
                }
            }

            return languageIds_list.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public void category_language() {

        categoryLists.clear();
        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(QuotesUpload.this));
        jsObj.addProperty("method_name", "get_cat_lang_list");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object_main = jsonObject.getJSONObject(Constant_Api.tag);

                        JSONArray jsonArray_cat = object_main.getJSONArray("category_list");

                        for (int i = 0; i < jsonArray_cat.length(); i++) {

                            JSONObject object = jsonArray_cat.getJSONObject(i);
                            String cid = object.getString("cid");
                            String category_name = object.getString("category_name");

                            categoryLists.add(new CategoryList(cid, category_name, null, null, "",""));
                        }

                        categoryLists.add(0, new CategoryList("", getResources().getString(R.string.selected_category), "", "", "",""));


                        JSONArray jsonArray_language = object_main.getJSONArray("language_list");

                        for (int i = 0; i < jsonArray_language.length(); i++) {

                            JSONObject object = jsonArray_language.getJSONObject(i);
                            String language_id = object.getString("language_id");
                            String language_name = object.getString("language_name");

                            languageLists.add(new LanguageList(language_id, language_name, null, null, null));

                        }

                        if (languageLists.size() != 0) {
                            selectLanguageAdapter = new SelectLanguageAdapter(QuotesUpload.this, languageLists, languageIF);
                            recyclerView.setAdapter(selectLanguageAdapter);
                        }

                        // Spinner Drop down elements
                        List<String> categories = new ArrayList<String>();
                        for (int i = 0; i < categoryLists.size(); i++) {
                            categories.add(categoryLists.get(i).getCategory_name());
                        }
                        // Creating adapter for spinner_cat
                        ArrayAdapter<String> dataAdapter_cat = new ArrayAdapter<String>(QuotesUpload.this, android.R.layout.simple_spinner_item, categories);
                        // Drop down layout style - list view with radio button
                        dataAdapter_cat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to spinner_cat
                        spinner_cat.setAdapter(dataAdapter_cat);

                        // Spinner click listener
                        spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_upload));
                                    categoryId = categoryLists.get(position).getCid();
                                } else {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_app_color));
                                    categoryId = categoryLists.get(position).getCid();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    private void submit() {

        String quotes = editText.getText().toString();

        if (quotes.equals("") || quotes.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_enter_quotes));
        } else if (categoryId.equals("") || categoryId.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_category));
        } else {

            if (method.isNetworkAvailable()) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    upload_quotes(method.pref.getString(method.profileId, null), categoryId, quotes, string_font, quotesColor_bg);
                } else {
                    Method.loginBack = true;
                    startActivity(new Intent(QuotesUpload.this, Login.class));
                }
            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }

        }

    }

    public void upload_quotes(String user_id, String cat_id, String quote, String quote_font, int quotesColor_bg) {

        button_upload.setVisibility(View.GONE);

        String quotes_tags = "";
        for (int i = 0; i < nachoTextView.getAllChips().size(); i++) {
            if (i == 0) {
                quotes_tags = quotes_tags.concat(nachoTextView.getAllChips().get(i).toString());
            } else {
                quotes_tags = quotes_tags.concat(",");
                quotes_tags = quotes_tags.concat(nachoTextView.getAllChips().get(i).toString());
            }
        }

        String lang_ids = "";
        for (int i = 0; i < languageIds_list.size(); i++) {
            if (i == 0) {
                lang_ids = lang_ids.concat(languageIds_list.get(i));
            } else {
                lang_ids = lang_ids.concat(",");
                lang_ids = lang_ids.concat(languageIds_list.get(i));
            }
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(QuotesUpload.this));
        jsObj.addProperty("method_name", "upload_quote_status");
        jsObj.addProperty("user_id", user_id);
        jsObj.addProperty("cat_id", cat_id);
        jsObj.addProperty("lang_ids", lang_ids);
        jsObj.addProperty("quote_tags", quotes_tags);
        jsObj.addProperty("quote", quote);
        jsObj.addProperty("quote_font", quote_font);
        jsObj.addProperty("bg_color", quotesColor_bg);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object_main = jsonObject.getJSONObject(Constant_Api.tag);
                        String msg = object_main.getString("msg");
                        String success = object_main.getString("success");

                        if (success.equals("1")) {
                            editText.setText("");
                            nachoTextView.setText("");
                            categoryId = "";
                            spinner_cat.setSelection(0);
                            languageIds_list.clear();
                            if(selectLanguageAdapter!=null){
                                selectLanguageAdapter.clearCheckBox();
                            }
                        }

                        Toast.makeText(QuotesUpload.this, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                button_upload.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                button_upload.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
