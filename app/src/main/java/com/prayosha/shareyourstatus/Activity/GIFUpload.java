package com.prayosha.shareyourstatus.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Adapter.SelectLanguageAdapter;
import com.prayosha.shareyourstatus.InterFace.LanguageIF;
import com.prayosha.shareyourstatus.Item.CategoryList;
import com.prayosha.shareyourstatus.Item.LanguageList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.prayosha.shareyourstatus.Service.UIGService;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hootsuite.nachos.NachoTextView;
import com.hootsuite.nachos.terminator.ChipTerminatorHandler;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class GIFUpload extends AppCompatActivity {

    private Method method;
    private LanguageIF languageIF;
    private MaterialToolbar toolbar;
    private String size_msg, imagePath, categoryId, imageType;
    private ProgressBar progressBar;
    private TextInputEditText editText;
    private ImageView imageView;
    private MaterialTextView textView_msg;
    private RecyclerView recyclerView;
    private NachoTextView nachoTextView;
    private MaterialButton button;
    private LinearLayout linearLayout;
    private Spinner spinner_cat, spinner_imageType;
    private String[] imageTypeList;
    private List<CategoryList> categoryLists;
    private ArrayList<String> languageIds_list;
    private List<LanguageList> languageLists;
    private SelectLanguageAdapter selectLanguageAdapter;
    private InputMethodManager imm;
    private int gif_size;
    private int REQUEST_CODE_CHOOSE = 109;
    private int positionImageType = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_upload);

        GlobalBus.getBus().register(this);

        Intent intent = getIntent();
        gif_size = intent.getIntExtra("size", 0);
        size_msg = intent.getStringExtra("size_msg");

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        method = new Method(GIFUpload.this);
        method.forceRTLIfSupported();

        imageTypeList = new String[]{getResources().getString(R.string.selected_gif_type),
                getResources().getString(R.string.landscape),
                getResources().getString(R.string.portrait)};

        categoryLists = new ArrayList<>();
        languageIds_list = new ArrayList<>();
        languageLists = new ArrayList<>();

        languageIF = new LanguageIF() {
            @Override
            public void selectLanguage(String id, String type, int position, boolean isValue) {
                if (isValue) {
                    languageIds_list.add(id);
                } else {
                    new SelectLanguage().execute(id);
                }
            }
        };

        toolbar = findViewById(R.id.toolbar_gif_upload);
        toolbar.setTitle(getResources().getString(R.string.upload_gif));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearLayout = findViewById(R.id.ll_ad_gif_upload);
        method.adView(linearLayout);

        progressBar = findViewById(R.id.progressBar_gif_upload);
        editText = findViewById(R.id.editText_gif_upload);
        imageView = findViewById(R.id.imageView_gif_upload);
        spinner_cat = findViewById(R.id.spinner_cat_gif_upload);
        spinner_imageType = findViewById(R.id.spinner_gifType_upload);
        textView_msg = findViewById(R.id.textView_msg_gif_upload);
        recyclerView = findViewById(R.id.recyclerView_gif_upload);
        nachoTextView = findViewById(R.id.nacho_gif_upload);
        button = findViewById(R.id.button_gif_upload);

        textView_msg.setVisibility(View.GONE);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(GIFUpload.this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);

        nachoTextView.addChipTerminator(',', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_CURRENT_TOKEN);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(GIFUpload.this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                Intent intent_upload = new Intent();
                                intent_upload.setType("image/gif");
                                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(intent_upload, REQUEST_CODE_CHOOSE);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                // check for permanent denial of permission
                                if (response.isPermanentlyDenied()) {
                                    // navigate user to app settings
                                }
                                method.alertBox(getResources().getString(R.string.cannot_use_save_permission));
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_gif();
            }
        });

        if (Method.isUpload) {
            button.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.GONE);
        }

        if (method.isNetworkAvailable()) {
            category_language();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }


    }

    @Subscribe
    public void getData(Events.UploadFinish uploadFinish) {
        if (editText != null) {
            finishUpload();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //gif get code
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            File file = null;
            Uri uri = data.getData();
            String file_path = getPath(GIFUpload.this, uri);
            assert file_path != null;
            try {

                textView_msg.setVisibility(View.VISIBLE);

                file = new File(file_path);
                if (file.getName().contains(".gif")) {
                    int file_size = (int) file.length() / (1024 * 1024);
                    if (file_size <= gif_size) {
                        imagePath = file_path;
                        textView_msg.setTextColor(getResources().getColor(R.color.textView_upload));
                        textView_msg.setText(imagePath);
                        Glide.with(GIFUpload.this).load(imagePath)
                                .placeholder(R.drawable.placeholder_landscape)
                                .into(imageView);
                    } else {
                        imagePath = "";
                        textView_msg.setTextColor(getResources().getColor(R.color.green));
                        textView_msg.setText(size_msg);
                    }
                } else {
                    imagePath = "";
                    textView_msg.setTextColor(getResources().getColor(R.color.green));
                    textView_msg.setText(getResources().getString(R.string.file_type_gif));
                }
            } catch (Exception e) {
                method.alertBox(getResources().getString(R.string.upload_folder_error));
            }

        }

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                try {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(context, contentUri, null, null);
                } catch (Exception e) {
                    Log.d("error_data", e.toString());
                }

            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @SuppressLint("StaticFieldLeak")
    class SelectLanguage extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            for (int i = 0; i < languageIds_list.size(); i++) {
                if (languageIds_list.get(i).equals(strings[0])) {
                    languageIds_list.remove(i);
                }
            }

            return languageIds_list.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public void category_language() {

        categoryLists.clear();
        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(GIFUpload.this));
        jsObj.addProperty("method_name", "get_cat_lang_list");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object_main = jsonObject.getJSONObject(Constant_Api.tag);

                        JSONArray jsonArray_cat = object_main.getJSONArray("category_list");

                        for (int i = 0; i < jsonArray_cat.length(); i++) {

                            JSONObject object = jsonArray_cat.getJSONObject(i);
                            String cid = object.getString("cid");
                            String category_name = object.getString("category_name");

                            categoryLists.add(new CategoryList(cid, category_name, null, null, "", ""));
                        }

                        categoryLists.add(0, new CategoryList("", getResources().getString(R.string.selected_category), "", "", "", ""));


                        JSONArray jsonArray_language = object_main.getJSONArray("language_list");

                        for (int i = 0; i < jsonArray_language.length(); i++) {

                            JSONObject object = jsonArray_language.getJSONObject(i);
                            String language_id = object.getString("language_id");
                            String language_name = object.getString("language_name");

                            languageLists.add(new LanguageList(language_id, language_name, null, null, null));

                        }

                        if (languageLists.size() != 0) {
                            selectLanguageAdapter = new SelectLanguageAdapter(GIFUpload.this, languageLists, languageIF);
                            recyclerView.setAdapter(selectLanguageAdapter);
                        }

                        // Spinner Drop down elements
                        List<String> categories = new ArrayList<String>();
                        for (int i = 0; i < categoryLists.size(); i++) {
                            categories.add(categoryLists.get(i).getCategory_name());
                        }
                        // Creating adapter for spinner_cat
                        ArrayAdapter<String> dataAdapter_cat = new ArrayAdapter<String>(GIFUpload.this, android.R.layout.simple_spinner_item, categories);
                        // Drop down layout style - list view with radio button
                        dataAdapter_cat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to spinner_cat
                        spinner_cat.setAdapter(dataAdapter_cat);


                        // Creating adapter for spinner image type
                        ArrayAdapter<String> dataAdapter_videoType = new ArrayAdapter<String>(GIFUpload.this, android.R.layout.simple_spinner_item, imageTypeList);
                        // Drop down layout style - list view with radio button
                        dataAdapter_videoType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to video type
                        spinner_imageType.setAdapter(dataAdapter_videoType);

                        // Spinner click listener
                        spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_upload));
                                    categoryId = categoryLists.get(position).getCid();
                                } else {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_app_color));
                                    categoryId = categoryLists.get(position).getCid();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // Spinner click listener
                        spinner_imageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_upload));
                                    imageType = imageTypeList[position];
                                } else {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_app_color));
                                    imageType = imageTypeList[position];
                                    positionImageType = position;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    public void submit_gif() {

        String title = editText.getText().toString();

        editText.setError(null);

        if (title.equals("") || title.isEmpty()) {
            editText.requestFocus();
            editText.setError(getResources().getString(R.string.please_enter_title));
        } else if (imagePath == null || imagePath.equals("") || imagePath.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_image));
        } else if (categoryId.equals("") || categoryId.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_category));
        } else if (imageType.equals(getResources().getString(R.string.selected_gif_type)) || imageType.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_gif));
        } else {

            editText.clearFocus();
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

            if (method.isNetworkAvailable()) {

                //get all tag
                String image_tags = "";
                for (int i = 0; i < nachoTextView.getAllChips().size(); i++) {
                    if (i == 0) {
                        image_tags = image_tags.concat(nachoTextView.getAllChips().get(i).toString());
                    } else {
                        image_tags = image_tags.concat(",");
                        image_tags = image_tags.concat(nachoTextView.getAllChips().get(i).toString());
                    }
                }

                //get all language id
                String lang_ids = "";
                for (int i = 0; i < languageIds_list.size(); i++) {
                    if (i == 0) {
                        lang_ids = lang_ids.concat(languageIds_list.get(i));
                    } else {
                        lang_ids = lang_ids.concat(",");
                        lang_ids = lang_ids.concat(languageIds_list.get(i));
                    }
                }

                gif_upload(method.pref.getString(method.profileId, null), categoryId, lang_ids, image_tags, title, imagePath);

            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }

    }

    private void gif_upload(String user_id, String cat_id, String lang_ids, String image_tags, String image_title, String image_file) {

        Method.isUpload = false;
        button.setVisibility(View.GONE);

        String image_type = null;

        if (positionImageType == 1) {
            image_type = "Landscape";
        } else {
            image_type = "Portrait";
        }

        Intent serviceIntent = new Intent(GIFUpload.this, UIGService.class);
        serviceIntent.setAction(UIGService.ACTION_START);
        serviceIntent.putExtra("user_id", user_id);
        serviceIntent.putExtra("cat_id", cat_id);
        serviceIntent.putExtra("lang_ids", lang_ids);
        serviceIntent.putExtra("image_tags", image_tags);
        serviceIntent.putExtra("image_title", image_title);
        serviceIntent.putExtra("image_layout", image_type);
        serviceIntent.putExtra("image_file", image_file);
        serviceIntent.putExtra("status_type", "gif");
        startService(serviceIntent);

    }

    public void finishUpload() {
        button.setVisibility(View.VISIBLE);
        editText.setText("");
        categoryId = "";
        nachoTextView.setText("");
        imagePath = "";
        languageIds_list.clear();
        spinner_cat.setSelection(0);
        spinner_imageType.setSelection(0);
        if(selectLanguageAdapter!=null){
            selectLanguageAdapter.clearCheckBox();
        }
        Glide.with(GIFUpload.this).load(R.drawable.placeholder_landscape).into(imageView);
        textView_msg.setTextColor(getResources().getColor(R.color.textView_upload));
        textView_msg.setText("");
        textView_msg.setVisibility(View.GONE);
        Toast.makeText(this, getResources().getString(R.string.upload_gif), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        GlobalBus.getBus().unregister(this);
        super.onDestroy();
    }

}
