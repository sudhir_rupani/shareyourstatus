package com.prayosha.shareyourstatus.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Adapter.LanguageAdapter;
import com.prayosha.shareyourstatus.InterFace.LanguageIF;
import com.prayosha.shareyourstatus.Item.LanguageList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.prayosha.shareyourstatus.Util.PrefManager;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class Language extends AppCompatActivity {

    private Method method;
    private LanguageIF languageIF;
    private PrefManager prefManager;
    private String type, language_id = "";
    private ProgressBar progressBar;
    private ImageView imageView_close;
    private MaterialTextView textView_skip, textView_noData;
    private MaterialButton button_continue;
    private RecyclerView recyclerView;
    private List<LanguageList> languageLists;
    private ArrayList<String> languageIds_list;
    private LanguageAdapter languageAdapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        method = new Method(Language.this);
        method.forceRTLIfSupported();

        type = getIntent().getStringExtra("type");

        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
        if (!prefManager.isLanguage()) {
            if (type.equals("welcome")) {
                launchHomeScreen();
            }
        }

        setContentView(R.layout.activity_language);

        languageLists = new ArrayList<>();
        languageIds_list = new ArrayList<>();

        languageIF = new LanguageIF() {
            @Override
            public void selectLanguage(String id, String get_type, int position, boolean isValue) {
                if (isValue) {
                    languageIds_list.add(id);
                    if (type.equals("welcome")) {
                        if (languageIds_list.size() == 0) {
                            button_continue.setVisibility(View.GONE);
                            textView_skip.setVisibility(View.VISIBLE);
                        } else {
                            button_continue.setVisibility(View.VISIBLE);
                            textView_skip.setVisibility(View.GONE);
                        }
                    }
                } else {
                    new LanguageSelect().execute(id);
                }
            }
        };

        progressBar = findViewById(R.id.progressBar_language);
        textView_noData = findViewById(R.id.textView_noData_language);
        imageView_close = findViewById(R.id.imageView_close_language);
        textView_skip = findViewById(R.id.textView_skip_language);
        button_continue = findViewById(R.id.button_language);
        recyclerView = findViewById(R.id.recyclerView_language);

        button_continue.setVisibility(View.GONE);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_language);
        method.adView(linearLayout);

        if (type.equals("setting") || type.equals("menu")) {
            textView_skip.setVisibility(View.GONE);
            imageView_close.setVisibility(View.VISIBLE);
        } else {
            textView_skip.setVisibility(View.VISIBLE);
            imageView_close.setVisibility(View.GONE);
        }


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Language.this);
        recyclerView.setLayoutManager(layoutManager);

        textView_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });

        imageView_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (method.isNetworkAvailable()) {
            language();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    @SuppressLint("StaticFieldLeak")
    class LanguageSelect extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            for (int i = 0; i < languageIds_list.size(); i++) {
                if (languageIds_list.get(i).equals(strings[0])) {
                    languageIds_list.remove(i);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (type.equals("welcome")) {
                if (languageIds_list.size() == 0) {
                    button_continue.setVisibility(View.GONE);
                    textView_skip.setVisibility(View.VISIBLE);
                } else {
                    button_continue.setVisibility(View.VISIBLE);
                    textView_skip.setVisibility(View.GONE);
                }
            }
        }
    }

    private void launchHomeScreen() {
        prefManager.setFirstLanguage(false);
        startActivity(new Intent(Language.this, SplashScreen.class)
                .putExtra("type", "welcome"));
        finish();
    }

    private void language() {

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(Language.this));
        jsObj.addProperty("method_name", "get_language");
        jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject object = jsonArray.getJSONObject(i);
                            String language_id = object.getString("language_id");
                            String language_name = object.getString("language_name");
                            String language_image = object.getString("language_image");
                            String language_image_thumb = object.getString("language_image_thumb");
                            String is_selected = object.getString("is_selected");

                            if (is_selected.equals("true")) {
                                languageIds_list.add(language_id);
                            }

                            languageLists.add(new LanguageList(language_id, language_name, language_image, language_image_thumb, is_selected));

                        }

                        if (languageLists.size() != 0) {

                            textView_noData.setVisibility(View.GONE);

                            if (type.equals("welcome")) {
                                if (languageIds_list.size() == 0) {
                                    button_continue.setVisibility(View.GONE);
                                    textView_skip.setVisibility(View.VISIBLE);
                                } else {
                                    button_continue.setVisibility(View.VISIBLE);
                                    textView_skip.setVisibility(View.GONE);
                                }
                            } else {
                                button_continue.setVisibility(View.VISIBLE);
                            }

                            button_continue.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    method.editor.putString(method.language_ids, "");

                                    for (int i = 0; i < languageIds_list.size(); i++) {
                                        if (i == 0) {
                                            language_id = language_id.concat(languageIds_list.get(i));
                                        } else {
                                            language_id = language_id.concat(",");
                                            language_id = language_id.concat(languageIds_list.get(i));
                                        }
                                    }

                                    method.editor.putString(method.language_ids, language_id);
                                    method.editor.commit();

                                    if (type.equals("setting")) {
                                        onBackPressed();
                                    } else if (type.equals("menu")) {
                                        onBackPressed();
                                        Events.Language language=new Events.Language(type);
                                        GlobalBus.getBus().post(language);
                                    } else {
                                        launchHomeScreen();
                                    }

                                }
                            });

                            languageAdapter = new LanguageAdapter(Language.this, languageLists, languageIF);
                            recyclerView.setAdapter(languageAdapter);

                        } else {
                            textView_noData.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }

        });

    }

}
