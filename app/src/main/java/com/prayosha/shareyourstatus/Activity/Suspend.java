package com.prayosha.shareyourstatus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class Suspend extends AppCompatActivity {

    private Method method;
    private MaterialToolbar toolbar;
    private String account_id;
    private CircleImageView imageView;
    private ProgressBar progressBar;
    private MaterialButton button;
    private LinearLayout linearLayout_msg;
    private RelativeLayout relativeLayout;
    private MaterialTextView textView_noData, textView_userName, textView_statusMsg, textView_status, textView_date, textView_admin_msg;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspend);

        method = new Method(Suspend.this);
        method.forceRTLIfSupported();

        toolbar = findViewById(R.id.toolbar_suspend);
        toolbar.setTitle(getResources().getString(R.string.account_status));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        account_id = getIntent().getStringExtra("id");

        progressBar = findViewById(R.id.progressbar_suspend);
        textView_noData = findViewById(R.id.textView_noData_suspend);
        relativeLayout = findViewById(R.id.relativeLayout_suspend);
        button = findViewById(R.id.button_suspend);
        imageView = findViewById(R.id.imageView_suspend);
        linearLayout_msg = findViewById(R.id.linearLayout_msg_suspend);
        textView_userName = findViewById(R.id.textView_userName_suspend);
        textView_statusMsg = findViewById(R.id.textView_statusMsg_suspend);
        textView_status = findViewById(R.id.textView_suspend);
        textView_date = findViewById(R.id.textView_date_suspend);
        textView_admin_msg = findViewById(R.id.textView_admin_msg_suspend);

        relativeLayout.setVisibility(View.GONE);

        if (method.isNetworkAvailable()) {
            userAccount();
        } else {
            relativeLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            textView_noData.setText(getResources().getString(R.string.no_data_found));
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });

    }

    public void userAccount() {

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(Suspend.this));
        jsObj.addProperty("method_name", "user_suspend");
        jsObj.addProperty("account_id", account_id);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        method.alertBox(message);

                    } else {

                        textView_noData.setVisibility(View.GONE);
                        relativeLayout.setVisibility(View.VISIBLE);

                        String status = jsonObject.getString("success");
                        String user_image = jsonObject.getString("user_image");
                        String user_name = jsonObject.getString("user_name");
                        String is_verified = jsonObject.getString("is_verified");
                        String date = jsonObject.getString("date");
                        String msg = jsonObject.getString("msg");

                        if (!user_image.equals("")) {
                            Glide.with(Suspend.this).load(user_image)
                                    .placeholder(R.drawable.user_profile).into(imageView);
                        }

                        if (status.equals("1")) {
                            linearLayout_msg.setVisibility(View.GONE);
                            textView_status.setText(getResources().getString(R.string.active));
                            textView_status.setTextColor(getResources().getColor(R.color.green));
                            textView_statusMsg.setText(getResources().getString(R.string.msg_approved));
                            textView_statusMsg.setTextColor(getResources().getColor(R.color.green));
                        } else {

                            linearLayout_msg.setVisibility(View.VISIBLE);

                            textView_status.setText(getResources().getString(R.string.suspend));
                            textView_status.setTextColor(getResources().getColor(R.color.red));
                            textView_statusMsg.setText(getResources().getString(R.string.msg_suspend));
                            textView_statusMsg.setTextColor(getResources().getColor(R.color.red));

                            if (method.pref.getBoolean(method.pref_login, false)) {

                                String type_login = method.pref.getString(method.loginType, "");
                                if (type_login.equals("google")) {

                                    //Google login
                                    GoogleSignInClient mGoogleSignInClient;

                                    // Configure sign-in to request the user's ID, email address, and basic
                                    // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
                                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                            .requestEmail()
                                            .build();

                                    // Build a GoogleSignInClient with the options specified by gso.
                                    mGoogleSignInClient = GoogleSignIn.getClient(Suspend.this, gso);

                                    mGoogleSignInClient.signOut()
                                            .addOnCompleteListener(Suspend.this, new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                }
                                            });
                                } else if (type_login.equals("facebook")) {
                                    LoginManager.getInstance().logOut();
                                }

                                method.editor.putBoolean(method.pref_login, false);
                                method.editor.commit();
                                Events.Login loginNotify = new Events.Login("");
                                GlobalBus.getBus().post(loginNotify);
                            }

                        }

                        textView_userName.setText(user_name);
                        if (is_verified.equals("true")) {
                            textView_userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_verification, 0);
                        }
                        textView_date.setText(date);
                        textView_admin_msg.setText(msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
