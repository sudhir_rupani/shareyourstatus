package com.prayosha.shareyourstatus.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.prayosha.shareyourstatus.Adapter.VPAdapterSS;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class StatusSaver extends AppCompatActivity implements PermissionListener {

    private Method method;
    private MaterialToolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private VPAdapterSS vpAdapterSS;
    private String type;
    private LinearLayout linearLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_saver);

        method = new Method(StatusSaver.this);
        method.forceRTLIfSupported();

        String[] tab_title = {getResources().getString(R.string.image), getResources().getString(R.string.video)};

        toolbar = findViewById(R.id.toolbar_ss);
        toolbar.setTitle(getResources().getString(R.string.status_saver));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        type = getIntent().getStringExtra("type");

        tabLayout = findViewById(R.id.tab_layout_ss);
        viewPager = findViewById(R.id.viewpager_ss);

        linearLayout = findViewById(R.id.linearLayout_ss);
        method.adView(linearLayout);

        for (int i = 0; i < 2; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tab_title[i]));
        }

        Dexter.withActivity(StatusSaver.this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(StatusSaver.this).check();

        //change selected tab when viewpager changed page
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //change viewpager page when tab selected
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.status_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.download_status_menu);
        if (type.equals("status")) {
            menuItem.setVisible(true);
        } else {
            menuItem.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.download_status_menu:
                startActivity(new Intent(StatusSaver.this, StatusSaver.class)
                        .putExtra("type", "download_status"));
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        //create and set ViewPager adapter
        vpAdapterSS = new VPAdapterSS(getSupportFragmentManager(), tabLayout.getTabCount(), StatusSaver.this, type);
        viewPager.setAdapter(vpAdapterSS);
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {
        // check for permanent denial of permission
        if (response.isPermanentlyDenied()) {
            // navigate user to app settings
        }
        dialog(getResources().getString(R.string.allow_storage));
    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
        token.continuePermissionRequest();
    }

    public void dialog(String message) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StatusSaver.this, R.style.DialogTitleTextStyle);
        builder.setMessage(Html.fromHtml(message));
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.allow_permission),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Dexter.withActivity(StatusSaver.this)
                                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .withListener(StatusSaver.this).check();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
