package com.prayosha.shareyourstatus.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class DeleteAccount extends AppCompatActivity {

    private Method method;
    private MaterialToolbar toolbar;
    private ProgressBar progressBar;
    private WebView webView;
    private CircleImageView imageView;
    private MaterialButton button;
    private MaterialTextView textView_userName, textView_quoteStatus, textView_imageStatus, textView_gifStatus, textView_videoStatus,
            textView_followers, textView_following, textView_earnPoint, textView_pendingPoint, textView_reference_code;
    private InputMethodManager imm;

    //Google login
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);

        method = new Method(DeleteAccount.this);
        method.forceRTLIfSupported();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        toolbar = findViewById(R.id.toolbar_delete);
        toolbar.setTitle(getResources().getString(R.string.delete_my_account));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        progressBar = findViewById(R.id.progressbar_delete);
        imageView = findViewById(R.id.imageView_delete);
        webView = findViewById(R.id.webview_delete);
        button = findViewById(R.id.button_delete);
        textView_userName = findViewById(R.id.textView_userName_delete);
        textView_quoteStatus = findViewById(R.id.textView_quoteStatus_delete);
        textView_imageStatus = findViewById(R.id.textView_imageStatus_delete);
        textView_gifStatus = findViewById(R.id.textView_gifStatus_delete);
        textView_videoStatus = findViewById(R.id.textView_videoStatus_delete);
        textView_followers = findViewById(R.id.textView_followers_delete);
        textView_following = findViewById(R.id.textView_following_delete);
        textView_earnPoint = findViewById(R.id.textView_earnPoint_delete);
        textView_pendingPoint = findViewById(R.id.textView_pendingPoint_delete);
        textView_reference_code = findViewById(R.id.textView_reference_code_delete);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_delete);
        method.adView(linearLayout);

        if (method.isNetworkAvailable()) {
            String user_id = method.pref.getString(method.profileId, "");
            accountDetail(user_id);
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }


    public void accountDetail(String user_id) {

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(DeleteAccount.this));
        jsObj.addProperty("method_name", "get_user_data");
        jsObj.addProperty("user_id", user_id);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");

                        if (success.equals("1")) {

                            String user_id = object.getString("user_id");
                            String name = object.getString("name");
                            String is_verified = object.getString("is_verified");
                            String email = object.getString("email");
                            String phone = object.getString("phone");
                            String user_image = object.getString("user_image");
                            String user_code = object.getString("user_code");
                            String total_point = object.getString("total_point");
                            String pending_point = object.getString("pending_point");
                            String total_video = object.getString("total_video");
                            String total_image = object.getString("total_image");
                            String total_gif = object.getString("total_gif");
                            String total_quote = object.getString("total_quote");
                            String total_followers = object.getString("total_followers");
                            String total_following = object.getString("total_following");
                            String delete_note = object.getString("delete_note");

                            Glide.with(DeleteAccount.this)
                                    .load(user_image).placeholder(R.drawable.user_profile)
                                    .into(imageView);

                            textView_userName.setText(name);
                            if (is_verified.equals("true")) {
                                textView_userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_verification, 0);
                            }

                            textView_quoteStatus.setText(total_quote);
                            textView_imageStatus.setText(total_image);
                            textView_gifStatus.setText(total_gif);
                            textView_videoStatus.setText(total_video);
                            textView_followers.setText(total_followers);
                            textView_following.setText(total_following);
                            textView_earnPoint.setText(total_point);
                            textView_pendingPoint.setText(pending_point);
                            textView_reference_code.setText(user_code);

                            webView.setBackgroundColor(Color.TRANSPARENT);
                            webView.setFocusableInTouchMode(false);
                            webView.setFocusable(false);
                            webView.getSettings().setDefaultTextEncodingName("UTF-8");
                            String mimeType = "text/html";
                            String encoding = "utf-8";

                            String color;
                            if (method.isDarkMode()) {
                                color = "#FFFFFF;";
                            } else {
                                color = "#8b8b8b;";
                            }
                            String text = "<html><head>"
                                    + "<style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/montserrat_regular.ttf\")}body{font-family: MyFont;color: " + color + "line-height:1.6}"
                                    + "</style></head>"
                                    + "<body>"
                                    + delete_note
                                    + "</body></html>";

                            webView.loadDataWithBaseURL(null, text, mimeType, encoding, null);

                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    final Dialog dialog = new Dialog(DeleteAccount.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.dialog_delete);
                                    dialog.getWindow().setLayout(ViewPager.LayoutParams.FILL_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                                    MaterialButton button_dialog = dialog.findViewById(R.id.button_dialog_delete);
                                    TextInputEditText editText_email = dialog.findViewById(R.id.editText_dialog_delete);

                                    editText_email.setText(email);

                                    button_dialog.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            String email_id = editText_email.getText().toString();

                                            editText_email.clearFocus();
                                            imm.hideSoftInputFromWindow(editText_email.getWindowToken(), 0);

                                            if (!isValidMail(email_id) || email_id.isEmpty()) {
                                                editText_email.requestFocus();
                                                editText_email.setError(getResources().getString(R.string.please_enter_email));
                                            } else {

                                                if (method.isNetworkAvailable()) {
                                                    delete_account(email_id, user_id);
                                                } else {
                                                    method.alertBox(getResources().getString(R.string.internet_connection));
                                                }

                                            }

                                        }
                                    });

                                    dialog.show();

                                }
                            });

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });

    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void delete_account(String email_id, String user_id) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(DeleteAccount.this));
        jsObj.addProperty("method_name", "delete_user_account");
        jsObj.addProperty("email", email_id);
        jsObj.addProperty("user_id", user_id);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");
                        String msg = object.getString("msg");

                        if (success.equals("1")) {

                            OneSignal.sendTag("user_id", method.pref.getString(method.profileId, null));

                            String type_login = method.pref.getString(method.loginType, "");
                            if (type_login.equals("google")) {
                                mGoogleSignInClient.signOut()
                                        .addOnCompleteListener(DeleteAccount.this, new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });
                            } else if (type_login.equals("facebook")) {
                                LoginManager.getInstance().logOut();
                            }

                            method.editor.putBoolean(method.pref_login, false);
                            method.editor.commit();

                            startActivity(new Intent(DeleteAccount.this, Login.class));
                            finishAffinity();

                        }

                        Toast.makeText(DeleteAccount.this, msg, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
