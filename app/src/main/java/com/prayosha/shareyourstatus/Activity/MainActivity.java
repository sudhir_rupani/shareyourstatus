package com.prayosha.shareyourstatus.Activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.prayosha.shareyourstatus.Fragment.CategoryFragment;
import com.prayosha.shareyourstatus.Fragment.DownloadFragment;
import com.prayosha.shareyourstatus.Fragment.FavouriteFragment;
import com.prayosha.shareyourstatus.Fragment.HomeMainFragment;
import com.prayosha.shareyourstatus.Fragment.ProfileFragment;
import com.prayosha.shareyourstatus.Fragment.ReferenceCodeFragment;
import com.prayosha.shareyourstatus.Fragment.RewardPointFragment;
import com.prayosha.shareyourstatus.Fragment.SCDetailFragment;
import com.prayosha.shareyourstatus.Fragment.SettingFragment;
import com.prayosha.shareyourstatus.Fragment.SubCategoryFragment;
import com.prayosha.shareyourstatus.InterFace.FullScreen;
import com.prayosha.shareyourstatus.Item.AboutUsList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.facebook.login.LoginManager;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.irfaan008.irbottomnavigation.SpaceItem;
import com.irfaan008.irbottomnavigation.SpaceNavigationView;
import com.irfaan008.irbottomnavigation.SpaceOnClickListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onesignal.OneSignal;
import com.prayosha.shareyourstatus.BuildConfig;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.SharedPreferences;

import java.net.MalformedURLException;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


@SuppressLint("StaticFieldLeak")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Activity activity;
    private Method method;
    public static MaterialToolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private SpaceNavigationView bottom_navigation;
    private MaterialTextView textView_appName;
    private ConsentForm form;
    boolean doubleBackToExitPressedOnce = false;
    private String id, type = "", status_type, title,app_Url = "";
    private Integer app_update = 0;
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GlobalBus.getBus().register(this);

        FullScreen fullScreen = new FullScreen() {
            @Override
            public void fullscreen(boolean isFull) {
                checkFullScreen(isFull);
            }
        };
        pref = getSharedPreferences("updatePref", 0); // 0 - for private mode
        editor = pref.edit();

        method = new Method(MainActivity.this, null, null, fullScreen);
        method.forceRTLIfSupported();

        if (getIntent().hasExtra("type")) {
            id = getIntent().getStringExtra("id");
            type = getIntent().getStringExtra("type");
            status_type = getIntent().getStringExtra("status_type");
            title = getIntent().getStringExtra("title");
        }
        toolbar = findViewById(R.id.toolbar_main);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(toolbar);

        linearLayout = findViewById(R.id.linearLayout_adView_main);
        progressBar = findViewById(R.id.progressbar_main);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_main);

        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.initWithSaveInstanceState(savedInstanceState);
        bottom_navigation.changeCenterButtonIcon(R.drawable.add_ic);
        bottom_navigation.addSpaceItem(new SpaceItem(getResources().getString(R.string.home), R.drawable.home_white));
        bottom_navigation.addSpaceItem(new SpaceItem(getResources().getString(R.string.reward), R.drawable.reward_white));
        bottom_navigation.addSpaceItem(new SpaceItem(getResources().getString(R.string.favorites), R.drawable.fav_white));
        bottom_navigation.addSpaceItem(new SpaceItem(getResources().getString(R.string.profile), R.drawable.profile_white));

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.ic_side_nav);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        textView_appName = headerLayout.findViewById(R.id.textView_name_nav);

        if (method.pref.getBoolean(method.pref_login, false)) {
            navigationView.getMenu().getItem(7).setIcon(R.drawable.logout);
            navigationView.getMenu().getItem(7).setTitle(getResources().getString(R.string.action_logout));
        }
        if (type.equals("payment_withdraw")) {
            bottom_navigation.changeCurrentItem(1);
        } else if (type.equals("category")) {
            selectDrawerItem(1);
            invisible_bottomNavigation();
        } else {
            navigationView.getMenu().getItem(0).setChecked(true);
        }
        navigationView.getMenu().getItem(5).setVisible(false);

        bottom_navigation.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                stopPlaying();
                status();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                bottomNavigation(itemIndex);
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                bottomNavigation(itemIndex);
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                startActivity(new Intent(MainActivity.this, StatusSaver.class)
                        .putExtra("type", "status"));
                drawer.closeDrawers();
            }
        });

        if (method.isNetworkAvailable()) {
            aboutUs();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
            progressBar.setVisibility(View.GONE);
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        showAppUpdateDialog();
    }
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
            }
            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                String title = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1).getTag();
                if (title != null) {
                    method.ShowFullScreen(false);
                    toolbar.setTitle(title);

                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    getWindow().clearFlags(1024);

                    stopPlaying();

                }
                super.onBackPressed();
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getResources().getString(R.string.Please_click_BACK_again_to_exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    public void stopPlaying() {
        Events.StopPlay stopPlay = new Events.StopPlay("");
        GlobalBus.getBus().post(stopPlay);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle bottom_navigation view item clicks here.
        //Checking if the item is in checked state or not, if not make it in checked state
        if (item.isChecked())
            item.setChecked(false);
        else
            item.setChecked(true);

        //Closing drawer on item click
        drawer.closeDrawers();

        // Handle bottom_navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.home:
                selectDrawerItem(0);
                bottom_navigation.changeCurrentItem(0);
                return true;

            case R.id.category:
                selectDrawerItem(1);
                invisible_bottomNavigation();
                stopPlaying();
                backStackRemove();
                CategoryFragment categoryFragment = new CategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "drawer_category");
                categoryFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, categoryFragment, getResources().getString(R.string.category)).commit();
                return true;

            case R.id.download:
                selectDrawerItem(2);
                invisible_bottomNavigation();
                stopPlaying();
                backStackRemove();
                DownloadFragment downloadFragment = new DownloadFragment();
                Bundle bundle_download = new Bundle();
                bundle_download.putString("typeLayout", "Landscape");
                downloadFragment.setArguments(bundle_download);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, downloadFragment, getResources().getString(R.string.download)).commit();
                return true;

            case R.id.upload:
                deselectDrawerItem(3);
                invisible_bottomNavigation();
                stopPlaying();
                status();
                return true;

            case R.id.reference_code:
                selectDrawerItem(4);
                invisible_bottomNavigation();
                stopPlaying();
                backStackRemove();
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, new ReferenceCodeFragment(), getResources().getString(R.string.reference_code)).commit();
                return true;

            case R.id.earn_point:
                deselectDrawerItem(5);
                invisible_bottomNavigation();
                stopPlaying();
                startActivity(new Intent(MainActivity.this, Spinner.class));
                return true;

            case R.id.setting:
                selectDrawerItem(6);
                invisible_bottomNavigation();
                stopPlaying();
                backStackRemove();
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, new SettingFragment(), getResources().getString(R.string.setting)).commit();
                return true;

            case R.id.login:
                stopPlaying();
                if (method.pref.getBoolean(method.pref_login, false)) {

                    OneSignal.sendTag("user_id", method.pref.getString(method.profileId, null));

                    if (method.pref.getString(method.loginType, null).equals("google")) {

                        // Configure sign-in to request the user's ID, email address, and basic
                        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestEmail()
                                .build();

                        // Build a GoogleSignInClient with the options specified by gso.
                        //Google login
                        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

                        mGoogleSignInClient.signOut()
                                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        method.editor.putBoolean(method.pref_login, false);
                                        method.editor.commit();
                                        startActivity(new Intent(MainActivity.this, Login.class));
                                        finishAffinity();
                                    }
                                });
                    } else if (method.pref.getString(method.loginType, null).equals("facebook")) {
                        LoginManager.getInstance().logOut();
                        method.editor.putBoolean(method.pref_login, false);
                        method.editor.commit();
                        startActivity(new Intent(MainActivity.this, Login.class));
                        finishAffinity();
                    } else {
                        method.editor.putBoolean(method.pref_login, false);
                        method.editor.commit();
                        startActivity(new Intent(MainActivity.this, Login.class));
                        finishAffinity();
                    }
                } else {
                    startActivity(new Intent(MainActivity.this, Login.class));
                    finishAffinity();
                }

                return true;

            default:
                return true;
        }
    }

    private void bottomNavigation(int itemIndex) {

        unCheck();

        switch (itemIndex) {
            case 0:
                selectDrawerItem(0);
                stopPlaying();
                backStackRemove();
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, new HomeMainFragment(), getResources().getString(R.string.home)).commitAllowingStateLoss();
                break;
            case 1:
                stopPlaying();
                backStackRemove();
                RewardPointFragment rewardPointFragment_nav = new RewardPointFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", type);
                rewardPointFragment_nav.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, rewardPointFragment_nav, getResources().getString(R.string.reward_point)).commit();
                type = "";
                break;
            case 2:
                stopPlaying();
                backStackRemove();
                FavouriteFragment favouriteFragment = new FavouriteFragment();
                Bundle bundle_fav = new Bundle();
                bundle_fav.putString("typeLayout", "Landscape");
                favouriteFragment.setArguments(bundle_fav);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, favouriteFragment, getResources().getString(R.string.favorites)).commit();
                break;
            case 3:
                stopPlaying();
                backStackRemove();
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle_profile = new Bundle();
                bundle_profile.putString("type", "user");
                bundle_profile.putString("id", method.pref.getString(method.profileId, null));
                profileFragment.setArguments(bundle_profile);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, profileFragment, getResources().getString(R.string.profile)).commit();
                break;
        }

    }

    public void invisible_bottomNavigation() {
        if (bottom_navigation != null) {
            bottom_navigation.changeCurrentItem(-1);
        }
    }

    public void selectDrawerItem(int position) {
        navigationView.getMenu().getItem(position).setChecked(true);
    }

    public void deselectDrawerItem(int position) {
        navigationView.getMenu().getItem(position).setCheckable(false);
        navigationView.getMenu().getItem(position).setChecked(false);
    }

    public void backStackRemove() {
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void unCheck() {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    public void showAppUpdateDialog(){
        if (app_update.equals(1)){
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(MainActivity.this, R.style.DialogTitleTextStyle);
            builder.setTitle("Update Required");
            builder.setMessage("An update to app is required to continue");
            builder.setCancelable(false);
            builder.setPositiveButton("UPDATE",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(app_Url));
                            startActivity(browserIntent);
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else if (app_update.equals(2) && pref.getInt("update_Count",0) == 0){
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(MainActivity.this, R.style.DialogTitleTextStyle);
            builder.setTitle("New Update Available");
            builder.setMessage("Please, update app to new version to continue reposting.");
            builder.setCancelable(false);
            builder.setPositiveButton("UPDATE",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(app_Url));
                            startActivity(browserIntent);
                        }
                    });
            builder.setNegativeButton("NO, THANKS",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    public void aboutUs() {

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(MainActivity.this));

        jsObj.addProperty("method_name", "app_settings");
        jsObj.addProperty("app_version", BuildConfig.VERSION_NAME);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);
                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");

                        if (success.equals("1")) {

                            String app_name = object.getString("app_name");
                            String publisher_id = object.getString("publisher_id");
                            String spinner_opt = object.getString("spinner_opt");
                            String banner_ad_type = object.getString("banner_ad_type");
                            String banner_ad_id = object.getString("banner_ad_id");
                            String interstitial_ad_type = object.getString("interstital_ad_type");
                            String interstital_ad_id = object.getString("interstital_ad_id");
                            String interstital_ad_click = object.getString("interstital_ad_click");
                            String rewarded_video_ads_id = object.getString("rewarded_video_ads_id");
                            String rewarded_video_click = object.getString("rewarded_video_click");
                            boolean banner_ad = Boolean.parseBoolean(object.getString("banner_ad"));
                            boolean interstital_ad = Boolean.parseBoolean(object.getString("interstital_ad"));
                            boolean rewarded_video_ads = Boolean.parseBoolean(object.getString("rewarded_video_ads"));
                            app_update = object.getInt("app_update_type");
                            app_Url = object.getString("url");

                            showAppUpdateDialog();
                            if (pref.getInt("update_Count",0) == 10){
                                editor.putInt("update_Count",0);
                            }else if (pref.getInt("update_Count",0) < 10){
                                editor.putInt("update_Count",pref.getInt("update_Count",0) + 1);
                            }
                            editor.commit();
                            Constant_Api.aboutUsList = new AboutUsList(app_name, publisher_id, banner_ad_type, banner_ad_id, interstitial_ad_type, interstital_ad_id, interstital_ad_click, rewarded_video_ads_id, rewarded_video_click, spinner_opt,
                                    banner_ad, interstital_ad, rewarded_video_ads);

                            if (Constant_Api.aboutUsList.getSpinner_opt().equals("true")) {
                                navigationView.getMenu().getItem(5).setVisible(true);
                            } else {
                                navigationView.getMenu().getItem(5).setVisible(false);
                            }

                            if (!Constant_Api.aboutUsList.getInterstital_ad_click().equals("")) {
                                Constant_Api.AD_COUNT_SHOW = Integer.parseInt(Constant_Api.aboutUsList.getInterstital_ad_click());
                            }

                            if (!Constant_Api.aboutUsList.getRewarded_video_click().equals("")) {
                                Constant_Api.REWARD_VIDEO_AD_COUNT_SHOW = Integer.parseInt(Constant_Api.aboutUsList.getRewarded_video_click());
                            }

                            if (Constant_Api.aboutUsList.getBanner_ad_type().equals("admob")) {
                                checkForConsent();
                            } else {
                                method.adView(linearLayout);
                            }

                            textView_appName.setText(Constant_Api.aboutUsList.getApp_name());

                            switch (type) {
                                case "payment_withdraw":
                                    try {
                                        stopPlaying();
                                        backStackRemove();
                                        toolbar.setTitle(getResources().getString(R.string.reward_point));
                                        RewardPointFragment rewardPointFragment = new RewardPointFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("type", type);
                                        rewardPointFragment.setArguments(bundle);
                                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, rewardPointFragment, getResources().getString(R.string.reward_point)).commit();
                                        type = "";
                                    } catch (Exception e) {
                                        Toast.makeText(MainActivity.this, getResources().getString(R.string.wrong), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case "category":
                                    try {
                                        SubCategoryFragment subCategoryFragment = new SubCategoryFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", id);
                                        bundle.putString("category_name", title);
                                        bundle.putString("type", "category");
                                        bundle.putString("typeLayout", "Landscape");
                                        subCategoryFragment.setArguments(bundle);
                                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, subCategoryFragment, title).commitAllowingStateLoss();
                                    } catch (Exception e) {
                                        Toast.makeText(MainActivity.this, getResources().getString(R.string.wrong), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case "single_status":
                                    try {
                                        SCDetailFragment scDetailFragment = new SCDetailFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", id);
                                        bundle.putString("type", "notification");
                                        bundle.putInt("position", 0);//dummy value
                                        bundle.putString("status_type", status_type);//status type value
                                        scDetailFragment.setArguments(bundle);
                                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, scDetailFragment, title).commitAllowingStateLoss();
                                    } catch (Exception e) {
                                        Toast.makeText(MainActivity.this, getResources().getString(R.string.wrong), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                default:
                                    try {
                                        HomeMainFragment homeMainFragment = new HomeMainFragment();
                                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, homeMainFragment, getResources().getString(R.string.home)).commitAllowingStateLoss();
                                    } catch (Exception e) {
                                        Toast.makeText(MainActivity.this, getResources().getString(R.string.wrong), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                            }

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    public void checkForConsent() {

        ConsentInformation consentInformation = ConsentInformation.getInstance(MainActivity.this);
        String[] publisherIds = {Constant_Api.aboutUsList.getPublisher_id()};
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                Log.d("consentStatus", consentStatus.toString());
                // User's consent status successfully updated.
                switch (consentStatus) {
                    case PERSONALIZED:
                        method.personalization_ad = true;
                        method.adView(linearLayout);
                        break;
                    case NON_PERSONALIZED:
                        method.personalization_ad = false;
                        method.adView(linearLayout);
                        break;
                    case UNKNOWN:
                        if (ConsentInformation.getInstance(getBaseContext()).isRequestLocationInEeaOrUnknown()) {
                            requestConsent();
                        } else {
                            method.personalization_ad = true;
                            method.adView(linearLayout);
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                // User's consent status failed to update.
            }
        });

    }

    public void requestConsent() {
        URL privacyUrl = null;
        try {
            // TODO: Replace with your app's privacy policy URL.
            privacyUrl = new URL(getResources().getString(R.string.pas_privacy_link));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            // Handle error.
        }
        form = new ConsentForm.Builder(MainActivity.this, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        showForm();
                        // Consent form loaded successfully.
                    }

                    @Override
                    public void onConsentFormOpened() {
                        // Consent form was displayed.
                    }

                    @Override
                    public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                        Log.d("consentStatus_form", consentStatus.toString());
                        switch (consentStatus) {
                            case PERSONALIZED:
                                method.personalization_ad = true;
                                break;
                            case NON_PERSONALIZED:
                                method.personalization_ad = false;
                                break;
                            case UNKNOWN:
                                method.personalization_ad = false;
                        }
                        method.adView(linearLayout);
                    }

                    @Override
                    public void onConsentFormError(String errorDescription) {
                        Log.d("errorDescription", errorDescription);
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        form.load();
    }

    private void showForm() {
        if (form != null) {
            form.show();
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onPause() {
        stopPlaying();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().clearFlags(1024);
        checkFullScreen(false);
        super.onPause();
    }


    @Subscribe
    public void getLogin(Events.Login login) {
        if (method != null) {
            if (navigationView != null) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    navigationView.getMenu().getItem(7).setIcon(R.drawable.logout);
                    navigationView.getMenu().getItem(7).setTitle(getResources().getString(R.string.action_logout));
                } else {
                    navigationView.getMenu().getItem(7).setIcon(R.drawable.login);
                    navigationView.getMenu().getItem(7).setTitle(getResources().getString(R.string.login));
                }
            }
        }
    }

    @Subscribe
    public void getFullscreen(Events.FullScreenNotify fullScreenNotify) {
        checkFullScreen(fullScreenNotify.isFullscreen());
    }

    @Subscribe
    public void geString(Events.Select Select) {
        if (navigationView != null) {
            navigationView.getMenu().getItem(1).setChecked(true);
        }
        invisible_bottomNavigation();
    }

    public void checkFullScreen(boolean isFull) {
        if (isFull) {
            toolbar.setVisibility(View.GONE);
            bottom_navigation.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
        } else {
            toolbar.setVisibility(View.VISIBLE);
            bottom_navigation.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
        }
    }

    public void status() {
        startActivity(new Intent(MainActivity.this, UploadStatus.class));
    }

    @Override
    protected void onDestroy() {
        GlobalBus.getBus().unregister(this);
        super.onDestroy();
    }

}
