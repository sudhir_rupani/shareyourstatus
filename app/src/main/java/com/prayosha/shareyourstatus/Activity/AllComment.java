package com.prayosha.shareyourstatus.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Adapter.AllCommentAdapter;
import com.prayosha.shareyourstatus.Item.CommentList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EndlessRecyclerViewScrollListener;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class AllComment extends AppCompatActivity {

    private Method method;
    private MaterialToolbar toolbar;
    private String post_id, type;
    private ProgressBar progressBar;
    private MaterialTextView textView_NoData;
    private RecyclerView recyclerView;
    private AllCommentAdapter allCommentAdapter;
    private TextInputEditText editTextComment;
    private ImageView imageView_send;
    private List<CommentList> commentLists;
    private InputMethodManager inputMethodManager;
    private Boolean isOver = false;
    private int pagination_index = 1;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_comment);

        method = new Method(AllComment.this);
        method.forceRTLIfSupported();

        commentLists = new ArrayList<>();

        Intent intent = getIntent();
        post_id = intent.getStringExtra("post_id");
        type = intent.getStringExtra("type");

        toolbar = findViewById(R.id.toolbar_all_comment);
        toolbar.setTitle(getResources().getString(R.string.allcomment));
        setSupportActionBar(toolbar);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = findViewById(R.id.progressbar_all_Comment);
        textView_NoData = findViewById(R.id.textView_noComment_all_Comment);
        editTextComment = findViewById(R.id.EditText_comment_allComment);
        ImageView imageView = findViewById(R.id.imageView_allComment);
        imageView_send = findViewById(R.id.imageView_send_allComment);
        recyclerView = findViewById(R.id.recyclerView_all_comment);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_adView_all_comment);
        method.adView(linearLayout);

        if (method.pref.getBoolean(method.pref_login, false)) {
            String image = method.pref.getString(method.userImage, null);
            if (image != null && !image.equals("")) {
                Glide.with(AllComment.this).load(image)
                        .placeholder(R.drawable.user_profile)
                        .into(imageView);
            }
        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(AllComment.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pagination_index++;
                            callData();
                        }
                    }, 1000);
                } else {
                    allCommentAdapter.hideHeader();
                }
            }
        });

        imageView_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (method.pref.getBoolean(method.pref_login, false)) {

                    editTextComment.setError(null);
                    String comment = editTextComment.getText().toString();

                    if (comment.equals("") || comment.isEmpty()) {
                        editTextComment.requestFocus();
                        editTextComment.setError(getResources().getString(R.string.please_enter_comment));
                    } else {
                        if (method.isNetworkAvailable()) {
                            editTextComment.clearFocus();
                            inputMethodManager.hideSoftInputFromWindow(editTextComment.getWindowToken(), 0);
                            Comment(method.pref.getString(method.profileId, null), comment, post_id, type);
                        } else {
                            Toast.makeText(AllComment.this, getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    Method.loginBack = true;
                    startActivity(new Intent(AllComment.this, Login.class));
                }
            }
        });

        callData();


    }

    private void callData() {
        if (method.isNetworkAvailable()) {
            if (method.pref.getBoolean(method.pref_login, false)) {
                String user_id = method.pref.getString(method.profileId, null);
                getComment(post_id, user_id, type);
            } else {
                getComment(post_id, "0", type);
            }
        } else {
            progressBar.setVisibility(View.GONE);
            method.alertBox(getResources().getString(R.string.internet_connection));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getComment(String post_id, String user_id, String type) {

        if (allCommentAdapter == null) {
            commentLists.clear();
            progressBar.setVisibility(View.VISIBLE);
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(AllComment.this));
        jsObj.addProperty("method_name", "get_all_comments");
        jsObj.addProperty("post_id", post_id);
        jsObj.addProperty("user_id", user_id);
        jsObj.addProperty("type", type);
        jsObj.addProperty("page", pagination_index);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                        for (int k = 0; k < jsonArray.length(); k++) {

                            JSONObject object = jsonArray.getJSONObject(k);
                            String id = object.getString("comment_id");
                            String user_id = object.getString("user_id");
                            String user_name = object.getString("user_name");
                            String user_image = object.getString("user_image");
                            String post_id = object.getString("post_id");
                            String status_type = object.getString("status_type");
                            String comment_text = object.getString("comment_text");
                            String comment_date = object.getString("comment_date");

                            commentLists.add(new CommentList(id, user_id, user_name, user_image, post_id, status_type, comment_text, comment_date));

                        }

                        if (jsonArray.length() == 0) {
                            if (allCommentAdapter != null) {
                                allCommentAdapter.hideHeader();
                                isOver = true;
                            }
                        }

                        if (allCommentAdapter == null) {
                            if (commentLists.size() == 0) {
                                textView_NoData.setVisibility(View.VISIBLE);
                            } else {
                                textView_NoData.setVisibility(View.GONE);
                                allCommentAdapter = new AllCommentAdapter(AllComment.this, commentLists);
                                recyclerView.setAdapter(allCommentAdapter);
                            }
                        } else {
                            allCommentAdapter.notifyDataSetChanged();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });

    }

    public void Comment(final String userId, final String comment, String post_id, String type) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(AllComment.this));
        jsObj.addProperty("method_name", "add_status_comment");
        jsObj.addProperty("comment_text", comment);
        jsObj.addProperty("user_id", userId);
        jsObj.addProperty("post_id", post_id);
        jsObj.addProperty("type", type);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        String msg = jsonObject.getString("msg");
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {

                            editTextComment.setText("");

                            String total_comment = jsonObject.getString("total_comment");
                            String id = jsonObject.getString("comment_id");
                            String user_id = jsonObject.getString("user_id");
                            String user_name = jsonObject.getString("user_name");
                            String user_image = jsonObject.getString("user_image");
                            String post_id = jsonObject.getString("post_id");
                            String status_type = jsonObject.getString("status_type");
                            String comment_text = jsonObject.getString("comment_text");
                            String comment_date = jsonObject.getString("comment_date");

                            textView_NoData.setVisibility(View.GONE);

                            commentLists.add(0, new CommentList(id, user_id, user_name, user_image, post_id, status_type, comment_text, comment_date));

                            if (allCommentAdapter == null) {
                                allCommentAdapter = new AllCommentAdapter(AllComment.this, commentLists);
                                recyclerView.setAdapter(allCommentAdapter);
                            } else {
                                allCommentAdapter.notifyDataSetChanged();
                            }
                            Events.AddComment addComment = new Events.AddComment(id, user_id, user_name, user_image, post_id, status_type, comment_text, comment_date, total_comment);
                            GlobalBus.getBus().post(addComment);

                            Toast.makeText(AllComment.this, msg, Toast.LENGTH_SHORT).show();

                        } else {
                            method.alertBox(msg);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

}
