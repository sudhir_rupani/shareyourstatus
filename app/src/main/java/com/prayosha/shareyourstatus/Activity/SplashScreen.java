package com.prayosha.shareyourstatus.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SplashScreen extends AppCompatActivity {

    // splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    private Boolean isCancelled = false;
    private Method method;
    private ProgressBar progressBar;
    private String id = "", type = "", status_type = "", title = "";
    //Google login
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splace_screen);

        method = new Method(SplashScreen.this);
        method.forceRTLIfSupported();
        method.login();

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        progressBar = findViewById(R.id.progressBar_splash_screen);
        progressBar.setVisibility(View.GONE);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (getIntent().hasExtra("type")) {
            type = getIntent().getStringExtra("type");
            assert type != null;
            if (type.equals("single_status")) {
                status_type = getIntent().getStringExtra("status_type");
            }
            if (type.equals("category") || type.equals("single_status")) {
                title = getIntent().getStringExtra("title");
            }
        }

        if (getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
        }

        splashScreen();

    }

    public void splashScreen() {

        if (method.isNetworkAvailable()) {

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app search_menu activity
                    if (!isCancelled) {
                        switch (type) {
                            case "payment_withdraw":
                                call_Activity();
                                break;
                            case "account_verification":
                                startActivity(new Intent(SplashScreen.this, AVStatus.class));
                                finishAffinity();
                                break;
                            case "account_status":
                                startActivity(new Intent(SplashScreen.this, Suspend.class)
                                        .putExtra("id", id));
                                finishAffinity();
                                break;
                            default:
                                if (method.pref.getBoolean(method.pref_login, false)) {
                                    if (method.pref.getString(method.loginType, "").equals("google")) {
                                        googleLogin();
                                    } else if (method.pref.getString(method.loginType, "").equals("facebook")) {
                                        facebookLogin();
                                    } else {
                                        String email = method.pref.getString(method.userEmail, null);
                                        String password = method.pref.getString(method.userPassword, null);
                                        login(email, "", password, "normal");
                                    }
                                } else {
                                    if (method.pref.getBoolean(method.is_verification, false)) {
                                        startActivity(new Intent(SplashScreen.this, Verification.class));
                                        finishAffinity();
                                    } else {
                                        if (method.pref.getBoolean(method.show_login, true)) {
                                            method.editor.putBoolean(method.show_login, false);
                                            method.editor.commit();
                                            Intent i = new Intent(SplashScreen.this, Login.class);
                                            startActivity(i);
                                            finishAffinity();
                                        } else {
                                            call_Activity();
                                        }
                                    }
                                }
                                break;
                        }
                    }

                }
            }, SPLASH_TIME_OUT);

        } else {
            call_Activity();
        }

    }

    public void googleLogin() {

        String password = method.pref.getString(method.userPassword, null);
        String auth_id = method.pref.getString(method.auth_id, null);
        login("", auth_id, password, "google");

    }

    public void facebookLogin() {

        String password = method.pref.getString(method.userPassword, null);
        String auth_id = method.pref.getString(method.auth_id, null);
        login("", auth_id, password, "facebook");

    }

    public void login(final String sendEmail, String auth_id, final String sendPassword, String type) {

        progressBar.setVisibility(View.VISIBLE);

        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        status.getPermissionStatus().getEnabled();

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(SplashScreen.this));
        jsObj.addProperty("method_name", "user_login");
        jsObj.addProperty("type", type);
        jsObj.addProperty("auth_id", auth_id);
        jsObj.addProperty("email", sendEmail);
        jsObj.addProperty("password", sendPassword);
        jsObj.addProperty("player_id", status.getSubscriptionStatus().getUserId());
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                        String success = object.getString("success");
                        String msg = object.getString("msg");

                        if (success.equals("1")) {

                            OneSignal.sendTag("user_id", method.pref.getString(method.profileId, null));
                            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
                            status.getPermissionStatus().getEnabled();
                            OneSignal.sendTag("player_id", status.getSubscriptionStatus().getUserId());

                            if (type.equals("google")) {
                                if (GoogleSignIn.getLastSignedInAccount(SplashScreen.this) != null) {
                                    call_Activity();
                                } else {
                                    method.editor.putBoolean(method.pref_login, false);
                                    method.editor.commit();
                                    startActivity(new Intent(SplashScreen.this, Login.class));
                                    finishAffinity();
                                }
                            } else if (type.equals("facebook")) {

                                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

                                if (isLoggedIn) {
                                    call_Activity();
                                } else {

                                    LoginManager.getInstance().logOut();

                                    method.editor.putBoolean(method.pref_login, false);
                                    method.editor.commit();
                                    startActivity(new Intent(SplashScreen.this, Login.class));
                                    finishAffinity();

                                }

                            } else {
                                call_Activity();
                            }
                        } else {
                            OneSignal.sendTag("user_id", method.pref.getString(method.profileId, null));

                            if (type.equals("google")) {

                                mGoogleSignInClient.signOut()
                                        .addOnCompleteListener(SplashScreen.this, new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });


                            } else if (type.equals("facebook")) {
                                LoginManager.getInstance().logOut();
                            }

                            method.editor.putBoolean(method.pref_login, false);
                            method.editor.commit();
                            startActivity(new Intent(SplashScreen.this, Login.class));
                            finishAffinity();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    public void call_Activity() {
        startActivity(new Intent(SplashScreen.this, MainActivity.class)
                .putExtra("type", type)
                .putExtra("id", id)
                .putExtra("status_type", status_type)
                .putExtra("title", title));
        finishAffinity();
    }

    @Override
    protected void onDestroy() {
        isCancelled = true;
        super.onDestroy();
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

}


