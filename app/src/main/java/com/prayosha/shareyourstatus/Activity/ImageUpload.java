package com.prayosha.shareyourstatus.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Adapter.SelectLanguageAdapter;
import com.prayosha.shareyourstatus.InterFace.LanguageIF;
import com.prayosha.shareyourstatus.Item.CategoryList;
import com.prayosha.shareyourstatus.Item.LanguageList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Service.UIGService;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hootsuite.nachos.NachoTextView;
import com.hootsuite.nachos.terminator.ChipTerminatorHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class ImageUpload extends AppCompatActivity {

    private Method method;
    private LanguageIF languageIF;
    private MaterialToolbar toolbar;
    private String size_msg, imagePath, categoryId, imageType;
    private ProgressBar progressBar;
    private TextInputEditText editText;
    private ImageView imageView;
    private MaterialTextView textView_msg;
    private RecyclerView recyclerView;
    private NachoTextView nachoTextView;
    private MaterialButton button;
    private LinearLayout linearLayout;
    private Spinner spinner_cat, spinner_imageType;
    private String[] imageTypeList;
    private ArrayList<Image> galleryImages;
    private ArrayList<String> languageIds_list;
    private List<CategoryList> categoryLists;
    private List<LanguageList> languageLists;
    private SelectLanguageAdapter selectLanguageAdapter;
    private InputMethodManager imm;
    private int image_size;
    private int REQUEST_CODE_IMAGE = 108;
    private int positionImageType = 0;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        GlobalBus.getBus().register(this);

        Intent intent = getIntent();
        image_size = intent.getIntExtra("size", 0);
        size_msg = intent.getStringExtra("size_msg");

        languageIds_list = new ArrayList<>();
        categoryLists = new ArrayList<>();
        languageLists = new ArrayList<>();
        galleryImages = new ArrayList<>();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        method = new Method(ImageUpload.this);
        method.forceRTLIfSupported();

        imageTypeList = new String[]{getResources().getString(R.string.selected_image_type),
                getResources().getString(R.string.landscape),
                getResources().getString(R.string.portrait)};

        languageIF = new LanguageIF() {
            @Override
            public void selectLanguage(String id, String type, int position, boolean isValue) {
                if (isValue) {
                    languageIds_list.add(id);
                } else {
                    new SelectLanguage().execute(id);
                }
            }
        };

        toolbar = findViewById(R.id.toolbar_image_upload);
        toolbar.setTitle(getResources().getString(R.string.upload_image));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearLayout = findViewById(R.id.ll_ad_image_upload);
        method.adView(linearLayout);

        progressBar = findViewById(R.id.progressBar_image_upload);
        editText = findViewById(R.id.editText_image_upload);
        spinner_cat = findViewById(R.id.spinner_cat_image_upload);
        spinner_imageType = findViewById(R.id.spinner_imageType_upload);
        imageView = findViewById(R.id.imageView_image_upload);
        textView_msg = findViewById(R.id.textView_msg_image_upload);
        recyclerView = findViewById(R.id.recyclerView_image_upload);
        nachoTextView = findViewById(R.id.nacho_image_upload);
        button = findViewById(R.id.button_image_upload);

        textView_msg.setVisibility(View.GONE);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ImageUpload.this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);

        nachoTextView.addChipTerminator(',', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_CURRENT_TOKEN);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseGalleryImage();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_image();
            }
        });

        if (Method.isUpload) {
            button.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.GONE);
        }

        if (method.isNetworkAvailable()) {
            category_language();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    @Subscribe
    public void getData(Events.UploadFinish uploadFinish) {
        if (editText != null) {
            finishUpload();
        }
    }

    public void chooseGalleryImage() {
        ImagePicker.with(this)
                .setFolderMode(true)
                .setFolderTitle(getResources().getString(R.string.app_name))
                .setImageTitle(getResources().getString(R.string.app_name))
                .setStatusBarColor("#960070")
                .setToolbarColor("#960070")
                .setProgressBarColor("#960070")
                .setMultipleMode(false)
                .setShowCamera(false)
                .setRequestCode(REQUEST_CODE_IMAGE)
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_IMAGE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                assert galleryImages != null;
                galleryImages = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
                assert galleryImages != null;
                if (galleryImages.size() != 0) {

                    textView_msg.setVisibility(View.VISIBLE);

                    Uri uri_banner = Uri.fromFile(new File(galleryImages.get(0).getPath()));
                    imagePath = galleryImages.get(0).getPath();

                    File file = new File(imagePath);
                    int file_size = (int) file.length() / (1024 * 1024);
                    if (file_size <= image_size) {
                        textView_msg.setTextColor(getResources().getColor(R.color.textView_upload));
                        textView_msg.setText(imagePath);
                        Glide.with(ImageUpload.this).load(uri_banner)
                                .placeholder(R.drawable.placeholder_landscape)
                                .into(imageView);
                        CropImage.activity(uri_banner).start(ImageUpload.this);
                    } else {
                        imagePath = "";
                        textView_msg.setTextColor(getResources().getColor(R.color.green));
                        textView_msg.setText(size_msg);
                    }

                }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imagePath = resultUri.getPath();
                Glide.with(ImageUpload.this).load(resultUri).into(imageView);
                textView_msg.setText(galleryImages.get(0).getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    class SelectLanguage extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            for (int i = 0; i < languageIds_list.size(); i++) {
                if (languageIds_list.get(i).equals(strings[0])) {
                    languageIds_list.remove(i);
                }
            }

            return languageIds_list.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public void category_language() {

        categoryLists.clear();
        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(ImageUpload.this));
        jsObj.addProperty("method_name", "get_cat_lang_list");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object_main = jsonObject.getJSONObject(Constant_Api.tag);

                        JSONArray jsonArray_cat = object_main.getJSONArray("category_list");

                        for (int i = 0; i < jsonArray_cat.length(); i++) {

                            JSONObject object = jsonArray_cat.getJSONObject(i);
                            String cid = object.getString("cid");
                            String category_name = object.getString("category_name");

                            categoryLists.add(new CategoryList(cid, category_name, null, null, "", ""));
                        }

                        categoryLists.add(0, new CategoryList("", getResources().getString(R.string.selected_category), "", "", "", ""));


                        JSONArray jsonArray_language = object_main.getJSONArray("language_list");

                        for (int i = 0; i < jsonArray_language.length(); i++) {

                            JSONObject object = jsonArray_language.getJSONObject(i);
                            String language_id = object.getString("language_id");
                            String language_name = object.getString("language_name");

                            languageLists.add(new LanguageList(language_id, language_name, null, null, null));

                        }

                        if (languageLists.size() != 0) {
                            selectLanguageAdapter = new SelectLanguageAdapter(ImageUpload.this, languageLists, languageIF);
                            recyclerView.setAdapter(selectLanguageAdapter);
                        }

                        // Spinner Drop down elements
                        List<String> categories = new ArrayList<String>();
                        for (int i = 0; i < categoryLists.size(); i++) {
                            categories.add(categoryLists.get(i).getCategory_name());
                        }
                        // Creating adapter for spinner_cat
                        ArrayAdapter<String> dataAdapter_cat = new ArrayAdapter<String>(ImageUpload.this, android.R.layout.simple_spinner_item, categories);
                        // Drop down layout style - list view with radio button
                        dataAdapter_cat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to spinner_cat
                        spinner_cat.setAdapter(dataAdapter_cat);


                        // Creating adapter for spinner image type
                        ArrayAdapter<String> dataAdapter_videoType = new ArrayAdapter<String>(ImageUpload.this, android.R.layout.simple_spinner_item, imageTypeList);
                        // Drop down layout style - list view with radio button
                        dataAdapter_videoType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to video type
                        spinner_imageType.setAdapter(dataAdapter_videoType);

                        // Spinner click listener
                        spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_upload));
                                    categoryId = categoryLists.get(position).getCid();
                                } else {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_app_color));
                                    categoryId = categoryLists.get(position).getCid();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        // Spinner click listener
                        spinner_imageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_upload));
                                    imageType = imageTypeList[position];
                                } else {
                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textView_app_color));
                                    imageType = imageTypeList[position];
                                    positionImageType = position;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    public void submit_image() {

        String title = editText.getText().toString();

        editText.setError(null);

        if (title.equals("") || title.isEmpty()) {
            editText.requestFocus();
            editText.setError(getResources().getString(R.string.please_enter_title));
        } else if (imagePath == null || imagePath.equals("") || imagePath.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_image));
        } else if (categoryId.equals("") || categoryId.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_category));
        } else if (imageType.equals(getResources().getString(R.string.selected_image_type)) || imageType.isEmpty()) {
            method.alertBox(getResources().getString(R.string.please_select_image_type));
        } else {

            editText.clearFocus();
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

            if (method.isNetworkAvailable()) {

                //get all tag
                String image_tags = "";
                for (int i = 0; i < nachoTextView.getAllChips().size(); i++) {
                    if (i == 0) {
                        image_tags = image_tags.concat(nachoTextView.getAllChips().get(i).toString());
                    } else {
                        image_tags = image_tags.concat(",");
                        image_tags = image_tags.concat(nachoTextView.getAllChips().get(i).toString());
                    }
                }

                //get all language id
                String lang_ids = "";
                for (int i = 0; i < languageIds_list.size(); i++) {
                    if (i == 0) {
                        lang_ids = lang_ids.concat(languageIds_list.get(i));
                    } else {
                        lang_ids = lang_ids.concat(",");
                        lang_ids = lang_ids.concat(languageIds_list.get(i));
                    }
                }

                image_upload(method.pref.getString(method.profileId, null), categoryId, lang_ids, image_tags, title, imagePath);

            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }

    }

    private void image_upload(String user_id, String cat_id, String lang_ids, String image_tags, String image_title, String image_file) {

        Method.isUpload = false;
        button.setVisibility(View.GONE);

        String image_type = null;

        if (positionImageType == 1) {
            image_type = "Landscape";
        } else {
            image_type = "Portrait";
        }

        Intent serviceIntent = new Intent(ImageUpload.this, UIGService.class);
        serviceIntent.setAction(UIGService.ACTION_START);
        serviceIntent.putExtra("user_id", user_id);
        serviceIntent.putExtra("cat_id", cat_id);
        serviceIntent.putExtra("lang_ids", lang_ids);
        serviceIntent.putExtra("image_tags", image_tags);
        serviceIntent.putExtra("image_title", image_title);
        serviceIntent.putExtra("image_layout", image_type);
        serviceIntent.putExtra("image_file", image_file);
        serviceIntent.putExtra("status_type", "image");
        startService(serviceIntent);

    }

    public void finishUpload() {
        button.setVisibility(View.VISIBLE);
        editText.setText("");
        categoryId = "";
        nachoTextView.setText("");
        imagePath = "";
        languageIds_list.clear();
        spinner_cat.setSelection(0);
        spinner_imageType.setSelection(0);
        if (selectLanguageAdapter != null) {
            selectLanguageAdapter.clearCheckBox();
        }
        Glide.with(ImageUpload.this).load(R.drawable.placeholder_landscape).into(imageView);
        textView_msg.setTextColor(getResources().getColor(R.color.textView_upload));
        textView_msg.setText("");
        textView_msg.setVisibility(View.GONE);
        Toast.makeText(this, getResources().getString(R.string.upload_image), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        GlobalBus.getBus().unregister(this);
        super.onDestroy();
    }

}
