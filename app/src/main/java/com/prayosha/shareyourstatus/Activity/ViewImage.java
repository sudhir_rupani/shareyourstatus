package com.prayosha.shareyourstatus.Activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;
import com.prayosha.shareyourstatus.Util.TouchImageView;
import com.google.android.material.appbar.MaterialToolbar;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class ViewImage extends AppCompatActivity {

    private Method method;
    private MaterialToolbar toolbar;
    private String string;
    private TouchImageView imageView;
    private LinearLayout linearLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        method = new Method(ViewImage.this);
        method.forceRTLIfSupported();

        string = getIntent().getStringExtra("path");

        toolbar = findViewById(R.id.toolbar_view_image);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearLayout = findViewById(R.id.linearLayout_view_image);
        method.adView(linearLayout);

        imageView = findViewById(R.id.imageView_view_image);

        Glide.with(ViewImage.this).load(string)
                .placeholder(R.drawable.placeholder_landscape)
                .into(imageView);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
