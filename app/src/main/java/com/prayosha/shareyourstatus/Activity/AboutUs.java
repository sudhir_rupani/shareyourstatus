package com.prayosha.shareyourstatus.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class AboutUs extends AppCompatActivity {

    private Method method;
    private MaterialToolbar toolbar;
    private ProgressBar progressBar;
    private WebView webView;
    private ImageView imageView;
    private MaterialCardView cardView_email, cardView_website, cardView_phone;
    private MaterialTextView textView_app_name, textView_app_version, textView_app_author, textView_app_contact,
            textView_app_email, textView_app_website;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        method = new Method(AboutUs.this);
        method.forceRTLIfSupported();

        toolbar = findViewById(R.id.toolbar_about_us);
        toolbar.setTitle(getResources().getString(R.string.about_us));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = findViewById(R.id.progressBar_about_us);

        imageView = findViewById(R.id.app_logo_about_us);
        webView = findViewById(R.id.webView_about_us);
        cardView_email = findViewById(R.id.cardView_email_about);
        cardView_website = findViewById(R.id.cardView_website_about);
        cardView_phone = findViewById(R.id.cardView_phone_about);
        textView_app_name = findViewById(R.id.textView_app_name_about_us);
        textView_app_version = findViewById(R.id.textView_app_version_about_us);
        textView_app_author = findViewById(R.id.textView_app_author_about_us);
        textView_app_contact = findViewById(R.id.textView_app_contact_about_us);
        textView_app_email = findViewById(R.id.textView_app_email_about_us);
        textView_app_website = findViewById(R.id.textView_app_website_about_us);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_about_us);
        method.adView(linearLayout);

        if (method.isNetworkAvailable()) {
            about();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }


    }

    public void about() {

        progressBar.setVisibility(View.VISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(AboutUs.this));
        jsObj.addProperty("method_name", "app_about");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");

                        if (success.equals("1")) {

                            String app_name = object.getString("app_name");
                            String app_logo = object.getString("app_logo");
                            String app_version = object.getString("app_version");
                            String app_author = object.getString("app_author");
                            String app_contact = object.getString("app_contact");
                            String app_email = object.getString("app_email");
                            String app_website = object.getString("app_website");
                            String app_description = object.getString("app_description");

                            textView_app_name.setText(app_name);

                            Glide.with(AboutUs.this).load(app_logo)
                                    .placeholder(R.drawable.app_icon)
                                    .into(imageView);

                            textView_app_version.setText(app_version);
                            textView_app_author.setText(app_author);
                            textView_app_contact.setText(app_contact);
                            textView_app_email.setText(app_email);
                            textView_app_website.setText(app_website);

                            webView.setBackgroundColor(Color.TRANSPARENT);
                            webView.setFocusableInTouchMode(false);
                            webView.setFocusable(false);
                            webView.getSettings().setDefaultTextEncodingName("UTF-8");
                            String mimeType = "text/html";
                            String encoding = "utf-8";

                            String color;
                            if (method.isDarkMode()) {
                                color = "#FFFFFF;";
                            } else {
                                color = "#8b8b8b;";
                            }
                            String text = "<html><head>"
                                    + "<style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/montserrat_regular.ttf\")}body{font-family: MyFont;color: " + color + "line-height:1.6}"
                                    + "</style></head>"
                                    + "<body>"
                                    + app_description
                                    + "</body></html>";

                            webView.loadDataWithBaseURL(null, text, mimeType, encoding, null);

                            cardView_email.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Intent emailIntent = new Intent(Intent.ACTION_VIEW);
                                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{app_email});
                                        emailIntent.setData(Uri.parse("mailto:"));
                                        startActivity(emailIntent);
                                    } catch (android.content.ActivityNotFoundException ex) {
                                        method.alertBox(getResources().getString(R.string.wrong));
                                    }
                                }
                            });

                            cardView_website.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        String url = app_website;
                                        if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                            url = "http://" + url;
                                        }
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                        startActivity(browserIntent);
                                    } catch (Exception e) {
                                        method.alertBox(getResources().getString(R.string.wrong));
                                    }

                                }
                            });

                            cardView_phone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                        callIntent.setData(Uri.parse("tel:" + app_contact));
                                        startActivity(callIntent);
                                    } catch (Exception e) {
                                        method.alertBox(getResources().getString(R.string.wrong));
                                    }
                                }
                            });

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
