package com.prayosha.shareyourstatus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Adapter.UploadStatusAdapter;
import com.prayosha.shareyourstatus.InterFace.UploadStatusIF;
import com.prayosha.shareyourstatus.InterFace.VideoAd;
import com.prayosha.shareyourstatus.Item.UploadControlList;
import com.prayosha.shareyourstatus.Item.UploadStatusList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class UploadStatus extends AppCompatActivity {


    private Method method;
    private VideoAd videoAd;
    private UploadStatusIF uploadStatusIF;
    private ProgressDialog progressDialog;
    private ImageView imageView;
    private RecyclerView recyclerView;
    private List<UploadStatusList> uploadStatusLists;
    private UploadControlList uploadControlLists;
    private UploadStatusAdapter uploadStatusAdapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_status);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        videoAd = new VideoAd() {
            @Override
            public void videoAdClick(String type) {
                choseOption(type);
            }
        };
        method = new Method(UploadStatus.this, videoAd);
        method.forceRTLIfSupported();

        uploadStatusLists = new ArrayList<>();

        progressDialog = new ProgressDialog(UploadStatus.this);

        imageView = findViewById(R.id.imageView_close_us);
        recyclerView = findViewById(R.id.recyclerView_us);

        LinearLayout linearLayout = findViewById(R.id.linearLayout_us);
        method.adView(linearLayout);


        GridLayoutManager layoutManager = new GridLayoutManager(UploadStatus.this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (uploadStatusAdapter.getItemViewType(position)) {
                    case 1:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);

        uploadStatusIF = new UploadStatusIF() {
            @Override
            public void UploadType(String type) {

                if (method.pref.getBoolean(method.pref_login, false)) {
                    String user_id = method.pref.getString(method.profileId, null);
                    checkUploadLimit(user_id, type);
                } else {
                    Method.loginBack = true;
                    startActivity(new Intent(UploadStatus.this, Login.class));
                }

            }
        };

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (method.isNetworkAvailable()) {
            upload();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    public void upload() {

        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(UploadStatus.this));
        jsObj.addProperty("method_name", "upload_status_opt");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");

                        if (success.equals("1")) {

                            String video_upload_opt = object.getString("video_upload_opt");
                            String image_upload_opt = object.getString("image_upload_opt");
                            String gif_upload_opt = object.getString("gif_upload_opt");
                            String quotes_upload_opt = object.getString("quotes_upload_opt");
                            String video_add = object.getString("video_add");
                            String image_add = object.getString("image_add");
                            String gif_add = object.getString("gif_add");
                            String quotes_add = object.getString("quotes_add");
                            String video_file_size = object.getString("video_file_size");
                            String video_file_duration = object.getString("video_file_duration");
                            String image_file_size = object.getString("image_file_size");
                            String gif_file_size = object.getString("gif_file_size");
                            String video_msg = object.getString("video_msg");
                            String video_size_msg = object.getString("video_size_msg");
                            String video_duration_msg = object.getString("video_duration_msg");
                            String img_size_msg = object.getString("img_size_msg");
                            String gif_size_msg = object.getString("gif_size_msg");

                            uploadControlLists = new UploadControlList(video_add, gif_add, quotes_add, image_add,
                                    video_file_size, video_file_duration, image_file_size, gif_file_size, video_msg, video_size_msg, video_duration_msg, img_size_msg, gif_size_msg);

                            if (video_upload_opt.equals("true")) {
                                uploadStatusLists.add(new UploadStatusList(getResources().getString(R.string.upload_video), "video",
                                        R.drawable.video_status_ic, R.drawable.line_video_us));
                            }
                            if (quotes_upload_opt.equals("true")) {
                                uploadStatusLists.add(new UploadStatusList(getResources().getString(R.string.upload_quotes), "quote",
                                        R.drawable.quotes_status_ic, R.drawable.line_quotes_us));
                            }
                            if (image_upload_opt.equals("true")) {
                                uploadStatusLists.add(new UploadStatusList(getResources().getString(R.string.upload_image), "image",
                                        R.drawable.photos_status_ic, R.drawable.line_image_us));
                            }
                            if (gif_upload_opt.equals("true")) {
                                uploadStatusLists.add(new UploadStatusList(getResources().getString(R.string.upload_gif), "gif",
                                        R.drawable.gif_status_ic, R.drawable.line_gif_us));
                            }

                            uploadStatusAdapter = new UploadStatusAdapter(UploadStatus.this, uploadStatusLists, uploadStatusIF);
                            recyclerView.setAdapter(uploadStatusAdapter);

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });
    }

    public void checkUploadLimit(String user_id, String type) {

        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(UploadStatus.this));
        jsObj.addProperty("user_id", user_id);
        jsObj.addProperty("type", type);
        jsObj.addProperty("method_name", "daily_upload_limit");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has(Constant_Api.STATUS)) {

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("-2")) {
                            method.suspend(message);
                        } else {
                            method.alertBox(message);
                        }

                    } else {

                        JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                        String success = object.getString("success");
                        String msg = object.getString("msg");

                        if (success.equals("1")) {

                            switch (type) {
                                case "video":
                                    if (uploadControlLists.getVideo_ad().equals("true")) {
                                        method.VideoAdDialog(type, "");
                                    } else {
                                        choseOption(type);
                                    }
                                    break;
                                case "quote":
                                    if (uploadControlLists.getQuote_ad().equals("true")) {
                                        method.VideoAdDialog(type, "");
                                    } else {
                                        choseOption(type);
                                    }
                                    break;
                                case "image":
                                    if (uploadControlLists.getImage_ad().equals("true")) {
                                        method.VideoAdDialog(type, "");
                                    } else {
                                        choseOption(type);
                                    }
                                    break;
                                default:
                                    if (uploadControlLists.getGif_ad().equals("true")) {
                                        method.VideoAdDialog(type, "");
                                    } else {
                                        choseOption(type);
                                    }
                                    break;
                            }

                        } else {
                            method.alertBox(msg);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                method.alertBox(getResources().getString(R.string.failed_try_again));
            }
        });

    }

    public void choseOption(String type) {
        switch (type) {
            case "video":
                startActivity(new Intent(UploadStatus.this, VideoUpload.class)
                        .putExtra("size", Integer.parseInt(uploadControlLists.getVideo_file_size()))
                        .putExtra("duration", Integer.parseInt(uploadControlLists.getVideo_file_duration()))
                        .putExtra("video_msg", uploadControlLists.getVideo_msg())
                        .putExtra("size_msg", uploadControlLists.getVideo_size_msg())
                        .putExtra("duration_msg", uploadControlLists.getVideo_duration_msg()));
                break;
            case "quote":
                startActivity(new Intent(UploadStatus.this, QuotesUpload.class));
                break;
            case "image":
                startActivity(new Intent(UploadStatus.this, ImageUpload.class)
                        .putExtra("size", Integer.parseInt(uploadControlLists.getImage_file_size()))
                        .putExtra("size_msg", uploadControlLists.getImage_file_size()));
                break;
            default:
                startActivity(new Intent(UploadStatus.this, GIFUpload.class)
                        .putExtra("size", Integer.parseInt(uploadControlLists.getImage_file_size()))
                        .putExtra("size_msg", uploadControlLists.getGif_size_msg()));
                break;
        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

}
