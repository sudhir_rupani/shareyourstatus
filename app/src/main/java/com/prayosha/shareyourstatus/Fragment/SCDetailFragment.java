package com.prayosha.shareyourstatus.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prayosha.shareyourstatus.Activity.AllComment;
import com.prayosha.shareyourstatus.Activity.Login;
import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Activity.ViewImage;
import com.prayosha.shareyourstatus.Adapter.CommentAdapter;
import com.prayosha.shareyourstatus.Adapter.RelatedScdAdapter;
import com.prayosha.shareyourstatus.Adapter.RelatedScdPortraitAdapter;
import com.prayosha.shareyourstatus.InterFace.FavouriteIF;
import com.prayosha.shareyourstatus.InterFace.FullScreen;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.InterFace.VideoAd;
import com.prayosha.shareyourstatus.Item.CommentList;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class SCDetailFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private Animation myAnim;
    private InputMethodManager inputMethodManager;

    //Video player
    private SimpleExoPlayer player;
    private PlayerView playerView;

    //other variable
    private String passId, type, status_type;
    private boolean isFullScreen = false, isView = true;
    private int position, columnWidth, columnHeight;

    private MaterialButton button;
    private ProgressDialog progressDialog;
    private CircleImageView circleImageView;
    private MaterialCardView cardView_user;
    private TextInputEditText editText_comment;
    private CircleImageView imageView_profile_comment;
    private ProgressBar progressBar, progressBar_player;

    private SubCategoryList statusLists;
    private List<SubCategoryList> relatedLists;
    private CommentAdapter commentAdapter;
    private RelatedScdAdapter relatedAdapter;
    private RelatedScdPortraitAdapter relatedPortraitAdapter;
    private RecyclerView recyclerView, recyclerViewComment;

    private RelativeLayout rel_imageView, rel_related;
    private ImageView imageViewFacebook, imageViewInstagram, imageViewWhatsApp, imageViewTwitter, imageView_more;
    private LinearLayout linearLayout_download, linearLayout_comment, linearLayout_detail, linearLayout_main;
    private ImageView imageView, imageView_play, imageView_download, imageView_fullscreen, imageView_like, imageView_fav, imageView_comment;
    private MaterialTextView textView_quotes, textView_view, textView_like, textView_downLoad, textView_userName,
            textView_follow, textView_all_comment, textView_noDataMain;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.scdetail_fragment, container, false);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        GlobalBus.getBus().register(this);

        assert getArguments() != null;
        passId = getArguments().getString("id");//id
        type = getArguments().getString("type");//get which type of single detail
        status_type = getArguments().getString("status_type");// status type (image, gif, quotes, video)
        position = getArguments().getInt("position");//use in subcategory array

        progressDialog = new ProgressDialog(getActivity());

        myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {

                viewHide();
                playerStop();

                getActivity().getSupportFragmentManager().popBackStack();

                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, relatedLists.get(position).getStatus_title()).addToBackStack(relatedLists.get(position).getStatus_title()).commitAllowingStateLoss();

            }
        };
        VideoAd videoAd = new VideoAd() {
            @Override
            public void videoAdClick(String type) {
                switch (type) {
                    case "download":
                        downloadStatus();
                        break;
                    case "like":
                        like_status();
                        break;
                    default:
                        playVideo();
                        break;
                }
            }
        };

        FullScreen fullScreen = new FullScreen() {
            @Override
            public void fullscreen(boolean isFull) {
                Events.FullScreenNotify fullScreenNotify = new Events.FullScreenNotify(isFull);
                GlobalBus.getBus().post(fullScreenNotify);
            }
        };
        method = new Method(getActivity(), onClick, videoAd, fullScreen);

        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        columnWidth = (method.getScreenWidth());
        columnHeight = (method.getScreenHeight());
        relatedLists = new ArrayList<>();

        progressBar = view.findViewById(R.id.progressbar_scd_fragment);
        progressBar_player = view.findViewById(R.id.progressbar_player_scd_fragment);

        button = view.findViewById(R.id.button_related_scd);

        imageView = view.findViewById(R.id.imageView_scd);
        imageView_play = view.findViewById(R.id.imageView_play_scd);
        imageView_download = view.findViewById(R.id.imageView_download_scd);
        imageView_fullscreen = view.findViewById(R.id.imageView_fullscreen_scd);
        imageView_fav = view.findViewById(R.id.imageView_fav_scd);
        imageView_like = view.findViewById(R.id.imageView_like_scd);
        imageViewFacebook = view.findViewById(R.id.imageView_facebook_scd);
        imageViewInstagram = view.findViewById(R.id.imageView_instagram_scd);
        imageViewWhatsApp = view.findViewById(R.id.imageView_whatsapp_scd);
        imageViewTwitter = view.findViewById(R.id.imageView_twitter_scd);
        imageView_more = view.findViewById(R.id.imageView_more_scd);
        imageView_profile_comment = view.findViewById(R.id.imageView_commentPro_scd);
        imageView_comment = view.findViewById(R.id.imageView_comment_scd);

        textView_quotes = view.findViewById(R.id.textView_quotes_scd);
        textView_view = view.findViewById(R.id.textView_view_scd);
        textView_like = view.findViewById(R.id.textView_like_scd);
        textView_downLoad = view.findViewById(R.id.textView_download_scd);
        textView_userName = view.findViewById(R.id.textView_userName_scd);
        textView_follow = view.findViewById(R.id.textView_follow_scd);
        textView_all_comment = view.findViewById(R.id.textView_comment_scd);
        textView_noDataMain = view.findViewById(R.id.textView_noData_main_scd_fragment);

        rel_imageView = view.findViewById(R.id.rel_image_scd);
        rel_related = view.findViewById(R.id.relativeLayout_related_scd);
        linearLayout_download = view.findViewById(R.id.ll_download_scd);
        linearLayout_comment = view.findViewById(R.id.ll_data_comment_scd);
        linearLayout_detail = view.findViewById(R.id.ll_detail_scd);

        editText_comment = view.findViewById(R.id.editText_comment_scd);
        recyclerViewComment = view.findViewById(R.id.recyclerView_comment_scd);
        recyclerView = view.findViewById(R.id.recyclerView_scd_fragment);
        circleImageView = view.findViewById(R.id.imageView_profile_scd);
        cardView_user = view.findViewById(R.id.cardView_user_scd);

        linearLayout_main = view.findViewById(R.id.linearLayout_parent_scd);
        linearLayout_main.setVisibility(View.GONE);

        RecyclerView.LayoutManager layoutManager_comment = new LinearLayoutManager(getActivity());
        recyclerViewComment.setLayoutManager(layoutManager_comment);
        recyclerViewComment.setFocusable(false);
        recyclerViewComment.setNestedScrollingEnabled(false);

        imageView_fullscreen.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SourceLockedOrientationActivity")
            @Override
            public void onClick(View v) {

                imageView_fullscreen.startAnimation(myAnim);

                if (isFullScreen) {
                    isFullScreen = false;

                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    getActivity().getWindow().clearFlags(1024);

                    imageView_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.full_screen));
                    if (statusLists.getStatus_layout().equals("Portrait")) {
                        imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                        playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                    } else {
                        imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                        playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                    }

                    rel_imageView.setVisibility(View.VISIBLE);
                    linearLayout_detail.setVisibility(View.VISIBLE);

                    method.ShowFullScreen(isFullScreen);

                } else {
                    isFullScreen = true;

                    imageView_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.exitfull_screen));
                    playerView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                    if (!statusLists.getStatus_layout().equals("Portrait")) {
                        getActivity().getWindow().setFlags(1024, 1024);
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    }

                    rel_imageView.setVisibility(View.GONE);
                    linearLayout_detail.setVisibility(View.GONE);

                    method.ShowFullScreen(isFullScreen);

                }
            }
        });

        playerView = view.findViewById(R.id.player_view);
        playerView.setVisibility(View.GONE);

        progressBar_player.setVisibility(View.GONE);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);

        if (method.isNetworkAvailable()) {
            if (method.pref.getBoolean(method.pref_login, false)) {
                detail(passId, method.pref.getString(method.profileId, null), status_type);
            } else {
                detail(passId, "0", status_type);
            }
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

        setHasOptionsMenu(true);

        return view;

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.ic_searchView);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (method.isNetworkAvailable()) {
                    backStackRemove();
                    SearchFragment searchFragment = new SearchFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("search_menu", query);
                    bundle.putString("typeLayout", "Landscape");
                    searchFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().replace(R.id.frameLayout_main, searchFragment, query).commitAllowingStateLoss();
                    return false;
                } else {
                    method.alertBox(getResources().getString(R.string.internet_connection));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        }));

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void backStackRemove() {
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Subscribe
    public void getPlay(Events.StopPlay stopPlay) {
        if (imageView_fullscreen != null) {
            isFullScreen = false;
            if (statusLists != null) {
                imageView_fullscreen.setImageDrawable(getResources().getDrawable(R.drawable.full_screen));
                if (statusLists.getStatus_layout().equals("Portrait")) {
                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                    playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                } else {
                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                    playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                }
            }
        }
        viewHide();
        playerStop();
    }

    @Subscribe
    public void getNotify(Events.InfoUpdate infoUpdate) {
        if (statusLists != null) {
            if (statusLists.getId().equals(infoUpdate.getId())) {
                if (statusLists.getStatus_type().equals(infoUpdate.getStatus_type())) {
                    switch (infoUpdate.getType()) {
                        case "all":
                            textView_view.setText(infoUpdate.getView());
                            textView_like.setText(infoUpdate.getTotal_like());
                            if (infoUpdate.getAlready_like().equals("true")) {
                                imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_hov));
                            } else {
                                if (method.isDarkMode()) {
                                    imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_white));
                                } else {
                                    imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_ic));
                                }
                            }
                            break;
                        case "view":
                            textView_view.setText(infoUpdate.getView());
                            break;
                        case "like":
                            textView_like.setText(infoUpdate.getTotal_like());
                            if (infoUpdate.getAlready_like().equals("true")) {
                                imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_hov));
                            } else {
                                if (method.isDarkMode()) {
                                    imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_white));
                                } else {
                                    imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_ic));
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    @Subscribe
    public void getNotify(Events.DownloadUpdate downloadUpdate) {
        if (statusLists.getId().equals(downloadUpdate.getId())) {
            if (statusLists.getStatus_type().equals(downloadUpdate.getStatus_type())) {
                if (textView_downLoad != null) {
                    textView_downLoad.setText(method.format(Double.parseDouble(downloadUpdate.getDownload_count())));
                }
            }
        }
    }

    @Subscribe
    public void getNotify(Events.AddComment comment) {
        if (statusLists.getId().equals(comment.getPost_id())) {
            statusLists.getCommentLists().add(0, new CommentList(comment.getComment_id(),
                    comment.getUser_id(), comment.getUser_name(), comment.getUser_image(),
                    comment.getPost_id(), comment.getStatus_type(), comment.getComment_text(), comment.getComment_date()));
            if (commentAdapter != null) {
                commentAdapter.notifyDataSetChanged();
                String textView_total = getResources().getString(R.string.view_all) + " " + "(" + comment.getTotal_comment() + ")";
                textView_all_comment.setText(textView_total);
            }
        }
        if (statusLists.getCommentLists().size() == 0) {
            linearLayout_comment.setVisibility(View.GONE);
        } else {
            linearLayout_comment.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void getNotify(Events.DeleteComment totalComment) {
        if (statusLists.getId().equals(totalComment.getPost_id())) {
            if (textView_all_comment != null) {
                String buttonTotal = getResources().getString(R.string.view_all) + " " + "(" + totalComment.getTotal_comment() + ")";
                textView_all_comment.setText(buttonTotal);
            }
        }
        if (totalComment.getType().equals("all_comment")) {
            if (statusLists.getId().equals(totalComment.getPost_id())) {
                for (int i = 0; i < statusLists.getCommentLists().size(); i++) {
                    if (totalComment.getComment_id().equals(statusLists.getCommentLists().get(i).getComment_id())) {
                        statusLists.getCommentLists().remove(i);
                        if (commentAdapter != null) {
                            commentAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
        if (statusLists.getCommentLists().size() == 0) {
            linearLayout_comment.setVisibility(View.GONE);
        } else {
            linearLayout_comment.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void getNotify(Events.FavouriteNotify favouriteNotify) {
        for (int i = 0; i < relatedLists.size(); i++) {
            if (relatedLists.get(i).getId().equals(favouriteNotify.getId())) {
                if (relatedLists.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                    relatedLists.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                    if (relatedAdapter != null) {
                        relatedAdapter.notifyItemChanged(i);
                    }
                    if (relatedPortraitAdapter != null) {
                        relatedPortraitAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
        if (statusLists.getId().equals(favouriteNotify.getId())) {
            statusLists.setIs_favourite(favouriteNotify.getIs_favourite());
            if (statusLists.getIs_favourite().equals("true")) {
                imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav_hov));
            } else {
                if (method.isDarkMode()) {
                    imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.fav_white_ic));
                } else {
                    imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav));
                }
            }
        }
    }

    private void viewHide() {
        if (imageView_fullscreen != null) {
            playerView.setVisibility(View.GONE);
            rel_imageView.setVisibility(View.VISIBLE);
            linearLayout_detail.setVisibility(View.VISIBLE);
        }
    }

    private void playerStop() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.stop();
            player.release();
        }
    }

    private void detail(final String id, final String user_id, String status_type) {

        relatedLists.clear();
        progressBar.setVisibility(View.VISIBLE);

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "single_status");
            jsObj.addProperty("status_id", id);
            jsObj.addProperty("user_id", user_id);
            jsObj.addProperty("type", status_type);
            jsObj.addProperty("lang_ids", status_type);
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String success = object.getString("success");

                                if (success.equals("1")) {

                                    String id = object.getString("id");
                                    String status_type_main = object.getString("status_type");
                                    String status_layout = object.getString("status_layout");
                                    String category_name = object.getString("category_name");
                                    String cat_id = object.getString("cat_id");
                                    String status_title = object.getString("status_title");
                                    String video_url = object.getString("video_url");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String quote_bg = object.getString("quote_bg");
                                    String quote_font = object.getString("quote_font");
                                    String total_likes = object.getString("total_likes");
                                    String total_download = object.getString("total_download");
                                    String total_viewer = object.getString("total_viewer");
                                    String already_like = object.getString("already_like");
                                    String user_id = object.getString("user_id");
                                    String user_name_single = object.getString("user_name");
                                    String user_image = object.getString("user_image");
                                    String already_follow = object.getString("already_follow");
                                    String is_verified = object.getString("is_verified");
                                    String total_comment = object.getString("total_comment");
                                    String watermark_image = object.getString("watermark_image");
                                    String watermark_on_off = object.getString("watermark_on_off");
                                    String is_favourite = object.getString("is_favourite");
                                    boolean video_views_status_ad = Boolean.parseBoolean(object.getString("video_views_status_ad"));
                                    boolean like_video_status_ad = Boolean.parseBoolean(object.getString("like_video_status_ad"));
                                    boolean download_video_status_ad = Boolean.parseBoolean(object.getString("download_video_status_ad"));
                                    boolean like_image_status_ad = Boolean.parseBoolean(object.getString("like_image_status_ad"));
                                    boolean download_image_status_ad = Boolean.parseBoolean(object.getString("download_image_status_ad"));
                                    boolean like_gif_points_status_ad = Boolean.parseBoolean(object.getString("like_gif_points_status_ad"));
                                    boolean download_gif_status_ad = Boolean.parseBoolean(object.getString("download_gif_status_ad"));
                                    boolean like_quotes_status_ad = Boolean.parseBoolean(object.getString("like_quotes_status_ad"));

                                    JSONArray jsonArray_related = object.getJSONArray("related");

                                    for (int j = 0; j < jsonArray_related.length(); j++) {

                                        JSONObject object_related = jsonArray_related.getJSONObject(j);
                                        String id_related = object_related.getString("id");
                                        String status_type_related = object_related.getString("status_type");
                                        String status_title_related = object_related.getString("status_title");
                                        String status_layout_related = object_related.getString("status_layout");
                                        String status_thumbnail_b_related = object_related.getString("status_thumbnail_b");
                                        String status_thumbnail_s_related = object_related.getString("status_thumbnail_s");
                                        String total_likes_related = object_related.getString("total_likes");
                                        String total_viewer_related = object_related.getString("total_viewer");
                                        String category_name_related = object_related.getString("category_name");
                                        String already_like_related = object_related.getString("already_like");
                                        String quote_bg_related = object_related.getString("quote_bg");
                                        String quote_font_related = object_related.getString("quote_font");
                                        String is_favourite_related = object_related.getString("is_favourite");

                                        relatedLists.add(new SubCategoryList("", id_related, status_type_related, status_title_related, status_layout_related, status_thumbnail_b_related, status_thumbnail_s_related, total_viewer_related, total_likes_related, already_like_related, category_name_related, quote_bg_related, quote_font_related, is_favourite_related, "", ""));

                                    }

                                    JSONArray jsonArray_comment = object.getJSONArray("user_comments");
                                    List<CommentList> commentLists = new ArrayList<>();

                                    for (int k = 0; k < jsonArray_comment.length(); k++) {

                                        JSONObject object_comment = jsonArray_comment.getJSONObject(k);
                                        String comment_id = object_comment.getString("comment_id");
                                        String comment_user_id = object_comment.getString("user_id");
                                        String comment_user_name = object_comment.getString("user_name");
                                        String comment_user_image = object_comment.getString("user_image");
                                        String comment_post_id = object_comment.getString("post_id");
                                        String comment_status_type = object_comment.getString("status_type");
                                        String comment_text = object_comment.getString("comment_text");
                                        String comment_date = object_comment.getString("comment_date");

                                        commentLists.add(new CommentList(comment_id, comment_user_id, comment_user_name, comment_user_image, comment_post_id, comment_status_type, comment_text, comment_date));

                                    }

                                    statusLists = new SubCategoryList("", id, cat_id, status_type_main, status_title, video_url, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, total_download,
                                            already_like, category_name, quote_bg, quote_font, user_id, user_name_single, user_image, already_follow, is_verified, total_comment, watermark_image, watermark_on_off, is_favourite, "", "", commentLists);

                                    textView_noDataMain.setVisibility(View.GONE);
                                    //visible single status detail
                                    linearLayout_main.setVisibility(View.VISIBLE);

                                    String status_type = statusLists.getStatus_type();

                                    //toolbar name set
                                    if (MainActivity.toolbar != null) {
                                        MainActivity.toolbar.setTitle(statusLists.getStatus_title());
                                    }

                                    //check status type
                                    if (status_type.equals("image") || status_type.equals("gif") || status_type.equals("quote")) {
                                        imageView_play.setVisibility(View.GONE);
                                        if (status_type.equals("quote")) {

                                            textView_quotes.setMinHeight(columnWidth);

                                            imageView.setVisibility(View.GONE);
                                            linearLayout_download.setVisibility(View.GONE);

                                            textView_quotes.setVisibility(View.VISIBLE);
                                            textView_quotes.setText(statusLists.getStatus_title());

                                            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "text_font/" + statusLists.getQuote_font());
                                            textView_quotes.setTypeface(typeface);

                                            textView_quotes.setBackgroundColor(Color.parseColor(statusLists.getQuote_bg()));

                                        } else {
                                            imageView.setVisibility(View.VISIBLE);
                                            textView_quotes.setVisibility(View.GONE);
                                            if (statusLists.getStatus_layout().equals("Portrait")) {
                                                imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                                            } else {
                                                imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                                            }
                                        }
                                    } else {
                                        textView_quotes.setVisibility(View.GONE);
                                        if (statusLists.getStatus_layout().equals("Portrait")) {
                                            imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                                            playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnHeight / 2 + 140));
                                        } else {
                                            imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                                            playerView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2));
                                        }
                                    }

                                    //status view count
                                    if (status_type.equals("quote") || status_type.equals("image") || status_type.equals("gif")) {
                                        if (method.pref.getBoolean(method.pref_login, false)) {
                                            String get_user_id = method.pref.getString(method.profileId, null);
                                            status_view(get_user_id);
                                        }
                                    }

                                    if (status_type.equals("quote")) {
                                        imageViewFacebook.setImageDrawable(getResources().getDrawable(R.drawable.messanger_ic));
                                    } else {
                                        imageViewFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
                                    }

                                    //check user following or not
                                    checkLogin_following();

                                    //all data update
                                    updateData();

                                    if (statusLists.getIs_favourite().equals("true")) {
                                        imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.fav_white_ic));
                                        } else {
                                            imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav));
                                        }
                                    }

                                    if (statusLists.getAlready_like().equals("true")) {
                                        imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_white));
                                        } else {
                                            imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_ic));
                                        }
                                    }

                                    if (relatedLists.size() == 0) {
                                        rel_related.setVisibility(View.GONE);
                                    } else {
                                        rel_related.setVisibility(View.VISIBLE);
                                        if (statusLists.getStatus_layout().equals("Portrait")) {
                                            relatedPortraitAdapter = new RelatedScdPortraitAdapter(getActivity(), relatedLists, onClick, "related_single");
                                            recyclerView.setAdapter(relatedPortraitAdapter);
                                        } else {
                                            relatedAdapter = new RelatedScdAdapter(getActivity(), relatedLists, onClick, "related_single");
                                            recyclerView.setAdapter(relatedAdapter);
                                        }
                                    }

                                    if (statusLists.getCommentLists().size() == 0) {
                                        linearLayout_comment.setVisibility(View.GONE);
                                    } else {
                                        linearLayout_comment.setVisibility(View.VISIBLE);
                                        commentAdapter = new CommentAdapter(getActivity(), statusLists.getCommentLists());
                                        recyclerViewComment.setAdapter(commentAdapter);
                                    }

                                    textView_view.setText(method.format(Double.parseDouble(statusLists.getTotal_viewer())));
                                    textView_like.setText(method.format(Double.parseDouble(statusLists.getTotal_likes())));
                                    if (!status_type.equals("quote")) {
                                        textView_downLoad.setText(method.format(Double.parseDouble(statusLists.getTotal_download())));
                                    }

                                    Glide.with(getActivity().getApplicationContext()).load(statusLists.getStatus_thumbnail_b())
                                            .placeholder(R.drawable.placeholder_landscape).into(imageView);

                                    String buttonTotal = getResources().getString(R.string.view_all) + " " + "(" + statusLists.getTotal_comment() + ")";
                                    textView_all_comment.setText(buttonTotal);

                                    imageView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (status_type.equals("image") || status_type.equals("gif")) {
                                                startActivity(new Intent(getActivity(), ViewImage.class)
                                                        .putExtra("path", statusLists.getStatus_thumbnail_b()));
                                            }
                                        }
                                    });

                                    linearLayout_download.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageView_download.startAnimation(myAnim);
                                            if (status_type.equals("image")) {
                                                if (download_image_status_ad) {
                                                    method.VideoAdDialog("download", "");
                                                } else {
                                                    downloadStatus();
                                                }
                                            } else if (status_type.equals("gif")) {
                                                if (download_gif_status_ad) {
                                                    method.VideoAdDialog("download", "");
                                                } else {
                                                    downloadStatus();
                                                }
                                            } else if (status_type.equals("video")) {
                                                if (download_video_status_ad) {
                                                    if (Constant_Api.REWARD_VIDEO_AD_COUNT + 1 == Constant_Api.REWARD_VIDEO_AD_COUNT_SHOW) {
                                                        playerStop();
                                                        viewHide();
                                                    }
                                                    method.VideoAdDialog("download", "");
                                                } else {
                                                    downloadStatus();
                                                }
                                            }
                                        }
                                    });

                                    imageView_play.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageView_play.startAnimation(myAnim);
                                            if (video_views_status_ad) {
                                                method.VideoAdDialog("video_play", "");
                                            } else {
                                                playVideo();
                                            }
                                        }
                                    });

                                    imageView_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageView_like.startAnimation(myAnim);
                                            if (method.pref.getBoolean(method.pref_login, false)) {

                                                if (!statusLists.getUser_id().equals(method.pref.getString(method.profileId, null))) {
                                                    if (status_type.equals("image")) {
                                                        if (like_image_status_ad) {
                                                            method.VideoAdDialog("like", "");
                                                        } else {
                                                            like_status();
                                                        }
                                                    } else if (status_type.equals("gif")) {
                                                        if (like_gif_points_status_ad) {
                                                            method.VideoAdDialog("like", "");
                                                        } else {
                                                            like_status();
                                                        }
                                                    } else if (status_type.equals("quote")) {
                                                        if (like_quotes_status_ad) {
                                                            method.VideoAdDialog("like", "");
                                                        } else {
                                                            like_status();
                                                        }
                                                    } else {
                                                        if (like_video_status_ad) {
                                                            if (Constant_Api.REWARD_VIDEO_AD_COUNT + 1 == Constant_Api.REWARD_VIDEO_AD_COUNT_SHOW) {
                                                                playerStop();
                                                                viewHide();
                                                            }
                                                            method.VideoAdDialog("like", "");
                                                        } else {
                                                            like_status();
                                                        }
                                                    }
                                                } else {
                                                    method.alertBox(getResources().getString(R.string.you_have_not_like_video));
                                                }
                                            } else {
                                                viewHide();
                                                playerStop();
                                                Method.loginBack = true;
                                                startActivity(new Intent(getActivity(), Login.class));
                                            }
                                        }
                                    });

                                    imageView_fav.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageView_fav.startAnimation(myAnim);
                                            if (method.pref.getBoolean(method.pref_login, false)) {
                                                FavouriteIF favouriteIF = new FavouriteIF() {
                                                    @Override
                                                    public void isFavourite(String isFavourite, String message) {
                                                        if (!isFavourite.equals("")) {
                                                            if (isFavourite.equals("true")) {
                                                                imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav_hov));
                                                            } else {
                                                                if (method.isDarkMode()) {
                                                                    imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.fav_white_ic));
                                                                } else {
                                                                    imageView_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav));
                                                                }
                                                            }
                                                        }
                                                        Events.FavouriteNotify homeNotify = new Events.FavouriteNotify(statusLists.getId(), statusLists.getStatus_layout(), isFavourite, statusLists.getStatus_type());
                                                        GlobalBus.getBus().post(homeNotify);
                                                    }
                                                };
                                                method.addToFav(statusLists.getId(), method.pref.getString(method.profileId, ""), statusLists.getStatus_type(), favouriteIF);
                                            } else {
                                                Method.loginBack = true;
                                                startActivity(new Intent(getActivity(), Login.class));
                                            }
                                        }
                                    });

                                    imageViewWhatsApp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageViewWhatsApp.startAnimation(myAnim);
                                            shareType("whatsapp");
                                        }
                                    });

                                    imageViewFacebook.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageViewFacebook.startAnimation(myAnim);
                                            shareType("facebook");
                                        }
                                    });

                                    imageViewInstagram.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageViewInstagram.startAnimation(myAnim);
                                            shareType("instagram");
                                        }
                                    });

                                    imageViewTwitter.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            imageViewTwitter.startAnimation(myAnim);
                                            shareType("twitter");
                                        }
                                    });

                                    imageView_more.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            viewHide();
                                            playerStop();
                                            imageView_more.startAnimation(myAnim);

                                            String url = "";

                                            if (status_type.equals("image") || status_type.equals("gif")) {
                                                url = statusLists.getStatus_thumbnail_b();
                                            } else if (status_type.equals("video")) {
                                                url = statusLists.getVideo_url();
                                            } else {
                                                url = statusLists.getStatus_title();
                                            }

                                            BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetOption();
                                            Bundle args = new Bundle();
                                            args.putString("id", statusLists.getId());
                                            args.putString("url", url);//status type is quote. then send title in url.
                                            args.putString("status_type", status_type);
                                            bottomSheetDialogFragment.setArguments(args);
                                            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), "Bottom Sheet Dialog Fragment");

                                        }

                                    });

                                    cardView_user.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            viewHide();
                                            playerStop();
                                            ProfileFragment profileFragment = new ProfileFragment();
                                            Bundle bundle_profile = new Bundle();
                                            bundle_profile.putString("type", "other_user");
                                            bundle_profile.putString("id", statusLists.getUser_id());
                                            profileFragment.setArguments(bundle_profile);
                                            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, profileFragment, getResources().getString(R.string.profile)).addToBackStack(getResources().getString(R.string.profile)).commitAllowingStateLoss();
                                        }
                                    });

                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            viewHide();
                                            playerStop();

                                            String string = getResources().getString(R.string.related_status);

                                            if (statusLists.getStatus_layout().equals("Landscape")) {
                                                RelatedFragment relatedFragment = new RelatedFragment();
                                                Bundle bundle_rel = new Bundle();
                                                bundle_rel.putString("type", "related");
                                                bundle_rel.putString("post_id", statusLists.getId());
                                                bundle_rel.putString("cat_id", statusLists.getCid());
                                                bundle_rel.putString("typeLayout", "Landscape");
                                                relatedFragment.setArguments(bundle_rel);
                                                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, relatedFragment, string).addToBackStack(string).commitAllowingStateLoss();
                                            } else {
                                                RelatedPortraitFragment relatedPortraitFragment = new RelatedPortraitFragment();
                                                Bundle bundle_relPor = new Bundle();
                                                bundle_relPor.putString("type", "related");
                                                bundle_relPor.putString("post_id", statusLists.getId());
                                                bundle_relPor.putString("cat_id", statusLists.getCid());
                                                bundle_relPor.putString("typeLayout", "Portrait");
                                                relatedPortraitFragment.setArguments(bundle_relPor);
                                                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, relatedPortraitFragment, string).addToBackStack(string).commitAllowingStateLoss();
                                            }

                                        }
                                    });

                                    if (method.pref.getBoolean(method.pref_login, false)) {
                                        String image = method.pref.getString(method.userImage, null);
                                        if (image != null && !image.equals("")) {
                                            Glide.with(getActivity().getApplicationContext()).load(image)
                                                    .placeholder(R.drawable.user_profile)
                                                    .into(imageView_profile_comment);
                                        }
                                    }

                                    textView_all_comment.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            viewHide();
                                            playerStop();
                                            startActivity(new Intent(getActivity(), AllComment.class)
                                                    .putExtra("post_id", statusLists.getId())
                                                    .putExtra("type", statusLists.getStatus_type()));
                                        }
                                    });

                                    imageView_comment.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            viewHide();
                                            playerStop();

                                            if (method.pref.getBoolean(method.pref_login, false)) {

                                                editText_comment.setError(null);
                                                String comment = editText_comment.getText().toString();
                                                if (comment.equals("") || comment.isEmpty()) {
                                                    editText_comment.requestFocus();
                                                    editText_comment.setError(getResources().getString(R.string.please_enter_comment));
                                                } else {
                                                    if (method.isNetworkAvailable()) {
                                                        editText_comment.clearFocus();
                                                        inputMethodManager.hideSoftInputFromWindow(editText_comment.getWindowToken(), 0);
                                                        Comment(method.pref.getString(method.profileId, null), comment);
                                                    } else {
                                                        method.alertBox(getResources().getString(R.string.internet_connection));
                                                    }
                                                }

                                            } else {
                                                Method.loginBack = true;
                                                startActivity(new Intent(getActivity(), Login.class));
                                            }

                                        }
                                    });

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void checkLogin_following() {

        if (!statusLists.getUser_image().equals("")) {
            Glide.with(getActivity().getApplicationContext()).load(statusLists.getUser_image())
                    .placeholder(R.drawable.user_profile).into(circleImageView);
        }
        textView_userName.setText(statusLists.getUser_name());
        if (statusLists.getIs_verified().equals("true")) {
            textView_userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_verification, 0);
        }
        if (method.pref.getBoolean(method.pref_login, false)) {
            if (statusLists.getAlready_follow().equals("true")) {
                textView_follow.setText(getResources().getString(R.string.unfollow));
            } else {
                textView_follow.setText(getResources().getString(R.string.follow));
            }
        } else {
            textView_follow.setText(getResources().getString(R.string.follow));
        }

        textView_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    String user_id = method.pref.getString(method.profileId, null);
                    assert user_id != null;
                    if (user_id.equals(statusLists.getUser_id())) {
                        method.alertBox(getResources().getString(R.string.you_have_not_onFollow));
                    } else {
                        follow(user_id, statusLists.getUser_id());
                    }
                } else {
                    viewHide();
                    playerStop();
                    Method.loginBack = true;
                    startActivity(new Intent(getActivity(), Login.class));
                }
            }
        });
    }

    //------------------ play video ---------------------//

    private void playVideo() {

        playerView.setVisibility(View.VISIBLE);
        progressBar_player.setVisibility(View.VISIBLE);

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        playerView.setPlayer(player);

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getResources().getString(R.string.app_name)));
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(statusLists.getVideo_url()));
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady) {
                    progressBar_player.setVisibility(View.GONE);
                    //Increase count
                    isView = true;
                }
                if (playbackState == Player.STATE_ENDED) {
                    if (isView) {
                        isView = false;
                        if (method.pref.getBoolean(method.pref_login, false)) {
                            if (!method.pref.getString(method.profileId, null).equals(statusLists.getUser_id())) {
                                status_view(method.pref.getString(method.profileId, null));
                            }
                        }
                    }
                }
                super.onPlayerStateChanged(playWhenReady, playbackState);
            }
        });
    }

    //------------------ play video ---------------------//

    //------------------ share data to other application ---------------------//

    private void shareType(String share_type) {

        viewHide();
        playerStop();

        if (method.isNetworkAvailable()) {
            String status_type = statusLists.getStatus_type();
            switch (share_type) {
                case "whatsapp":
                    if (method.isAppInstalled_Whatsapp()) {
                        if (status_type.equals("quote")) {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.setPackage("com.whatsapp");
                            intent.putExtra(Intent.EXTRA_TEXT, statusLists.getStatus_title());
                            startActivity(intent);
                        } else if (status_type.equals("image") || status_type.equals("gif")) {
                            new ShareImage().execute(status_type, "whatsapp");
                        } else {
                            new ShareVideo().execute("whatsapp");
                        }
                    } else {
                        method.alertBox(getResources().getString(R.string.please_install_whatsapp));
                    }
                    break;
                case "facebook":
                    if (status_type.equals("quote")) {
                        if (method.isAppInstalled_fbMessenger()) {
                            Intent share_fb = new Intent(Intent.ACTION_SEND);// Create the new Intent using the 'Send' action.
                            share_fb.setType("text/plain"); // Set the MIME type
                            share_fb.setPackage("com.facebook.orca");
                            share_fb.putExtra(Intent.EXTRA_TEXT, statusLists.getStatus_title()); // Broadcast the Intent.
                            startActivity(share_fb);
                        } else {
                            method.alertBox(getResources().getString(R.string.please_install_fb_messenger));
                        }
                    } else {
                        if (method.isAppInstalled_facebook()) {
                            if (status_type.equals("image") || status_type.equals("gif")) {
                                new ShareImage().execute(status_type, "facebook");
                            } else {
                                new ShareVideo().execute("facebook");
                            }

                        } else {
                            method.alertBox(getResources().getString(R.string.please_install_facebook));
                        }
                    }
                    break;
                case "twitter":
                    if (method.isAppInstalled_twitter()) {
                        if (status_type.equals("quote")) {
                            Intent share_tw = new Intent(Intent.ACTION_SEND);
                            share_tw.setType("text/plain");
                            share_tw.setPackage("com.twitter.android");
                            share_tw.putExtra(Intent.EXTRA_TEXT, statusLists.getStatus_title()); // Broadcast the Intent.
                            startActivity(Intent.createChooser(share_tw, "Share to"));
                        } else if (status_type.equals("image") || status_type.equals("gif")) {
                            new ShareImage().execute(status_type, "twitter");
                        } else {
                            new ShareVideo().execute("twitter");
                        }
                    } else {
                        method.alertBox(getResources().getString(R.string.please_install_twitter));
                    }
                    break;
                case "instagram":
                    if (method.isAppInstalled_Instagram()) {
                        if (status_type.equals("quote")) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.setPackage("com.instagram.android");
                                intent.putExtra(Intent.EXTRA_TEXT, statusLists.getStatus_title());
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        } else if (status_type.equals("image") || status_type.equals("gif")) {
                            new ShareImage().execute(status_type, "instagram");
                        } else {
                            new ShareVideo().execute("instagram");
                        }
                    } else {
                        method.alertBox(getResources().getString(R.string.please_install_instagram));
                    }
                    break;
            }
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    @SuppressLint("StaticFieldLeak")
    public class ShareVideo extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String iconsStoragePath;
        private File sdIconStorageDir;
        private String type;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.setMax(100);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.cancel_dialog), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (sdIconStorageDir != null) {
                        sdIconStorageDir.delete();
                    }
                    dialog.dismiss();
                    cancel(true);
                }
            });
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {

            int count;
            try {
                URL url = new URL(statusLists.getVideo_url());
                String id = statusLists.getId();
                type = params[0];
                iconsStoragePath = getActivity().getExternalCacheDir().getAbsolutePath();
                String filePath = "file" + id + ".mp4";

                sdIconStorageDir = new File(iconsStoragePath, filePath);

                //create storage directories, if they don't exist
                if (sdIconStorageDir.exists()) {
                    Log.d("File_name", "File_name");
                } else {
                    URLConnection conection = url.openConnection();
                    conection.setRequestProperty("Accept-Encoding", "identity");
                    conection.connect();
                    // getting file length
                    int lenghtOfFile = conection.getContentLength();
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(sdIconStorageDir);
                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        progressDialog.setProgress((int) (total * 100 / lenghtOfFile));
                        output.write(data, 0, count);
                    }
                    output.flush(); // flushing output
                    output.close();// closing streams
                    input.close();
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            progressDialog.dismiss();

            switch (type) {
                case "whatsapp":
                    video_whatsapp(sdIconStorageDir.toString());
                    break;
                case "facebook":
                    video_fb(sdIconStorageDir.toString());
                    break;
                case "instagram":
                    video_instagram(sdIconStorageDir);
                    break;
                case "twitter":
                    video_twitter(sdIconStorageDir.toString());
                    break;
                default:
                    break;
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class ShareImage extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String iconsStoragePath;
        private File sdIconStorageDir;
        private String type, status_type;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.setMax(100);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.cancel_dialog), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (sdIconStorageDir != null) {
                        sdIconStorageDir.delete();
                    }
                    dialog.dismiss();
                    cancel(true);
                }
            });
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {

            int count;
            try {
                URL url = new URL(statusLists.getStatus_thumbnail_b());
                String id = statusLists.getId();
                status_type = params[0];
                type = params[1];
                iconsStoragePath = getActivity().getExternalCacheDir().getAbsolutePath();

                String filePath;

                if (status_type.equals("image")) {
                    filePath = "file" + id + ".jpg";
                } else {
                    filePath = "file" + id + ".gif";
                }

                sdIconStorageDir = new File(iconsStoragePath, filePath);

                //create storage directories, if they don't exist
                if (sdIconStorageDir.exists()) {
                    Log.d("File_name", "File_name");
                } else {
                    URLConnection conection = url.openConnection();
                    conection.setRequestProperty("Accept-Encoding", "identity");
                    conection.connect();
                    // getting file length
                    int lenghtOfFile = conection.getContentLength();
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(sdIconStorageDir);
                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        progressDialog.setProgress((int) (total * 100 / lenghtOfFile));
                        output.write(data, 0, count);
                    }
                    output.flush(); // flushing output
                    output.close();// closing streams
                    input.close();
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            progressDialog.dismiss();

            switch (type) {
                case "whatsapp":
                    image_whatsapp(sdIconStorageDir);
                    break;
                case "facebook":
                    image_fb(sdIconStorageDir);
                    break;
                case "instagram":
                    image_instagram(sdIconStorageDir);
                    break;
                case "twitter":
                    image_twitter(sdIconStorageDir.toString());
                    break;
                default:
                    break;
            }
        }

    }

    private void video_whatsapp(String path) {

        Uri uri = Uri.parse(path);
        Intent videoshare = new Intent(Intent.ACTION_SEND);
        videoshare.setType("*/*");
        videoshare.setPackage("com.whatsapp");
        videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        videoshare.putExtra(Intent.EXTRA_STREAM, uri);
        videoshare.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + getActivity().getApplication().getPackageName());
        startActivity(videoshare);

    }

    private void image_whatsapp(File path) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (status_type.equals("gif")) {
            intent.setType("image/gif");
        } else {
            intent.setType("image/*");
        }
        intent.setPackage("com.whatsapp");
        Uri uri = Uri.fromFile(path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + getActivity().getApplication().getPackageName());
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_to)));
    }

    private void video_fb(String path) {
        Intent share_fb = new Intent(Intent.ACTION_SEND);
        share_fb.setType("video/*");
        share_fb.setPackage("com.facebook.katana");
        File media_fb = new File(path);
        Uri uri_fb = Uri.fromFile(media_fb);
        share_fb.putExtra(Intent.EXTRA_STREAM, uri_fb);
        startActivity(Intent.createChooser(share_fb, getResources().getString(R.string.share_to)));
    }

    private void image_fb(File path) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (status_type.equals("gif")) {
            intent.setType("image/gif");
        } else {
            intent.setType("image/*");
        }
        intent.setPackage("com.facebook.katana");
        Uri uri = Uri.fromFile(path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_to)));
    }

    private void video_instagram(File file) {

        try {
            Intent intent = new Intent("com.instagram.share.ADD_TO_STORY");
            intent.setDataAndType(Uri.fromFile(file), "video/*");
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra("content_url", "");

            // Instantiate activity and verify it will resolve implicit intent
            Activity activity = getActivity();
            if (activity.getPackageManager().resolveActivity(intent, 0) != null) {
                activity.startActivityForResult(intent, 0);
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void image_instagram(File file) {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            if (status_type.equals("gif")) {
                intent.setType("image/gif");
            } else {
                intent.setType("image/*");
            }
            intent.setPackage("com.instagram.android");
            Uri uri = Uri.fromFile(file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_to)));
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void image_twitter(String path) {
        Intent intent = new Intent(Intent.ACTION_SEND);// Create the new Intent using the 'Send' action.
        intent.setType("image/*");   // Set the MIME type
        intent.setPackage("com.twitter.android");
        File media_tw = new File(path); // Create the URI from the media
        Uri uri_tw = Uri.fromFile(media_tw); // Add the URI to the Intent.
        intent.putExtra(Intent.EXTRA_STREAM, uri_tw); // Broadcast the Intent.
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_to)));
    }

    private void video_twitter(String path) {
        Intent share_tw = new Intent(Intent.ACTION_SEND);// Create the new Intent using the 'Send' action.
        share_tw.setType("video/*");   // Set the MIME type
        share_tw.setPackage("com.twitter.android");
        File media_tw = new File(path); // Create the URI from the media
        Uri uri_tw = Uri.fromFile(media_tw); // Add the URI to the Intent.
        share_tw.putExtra(Intent.EXTRA_STREAM, uri_tw); // Broadcast the Intent.
        startActivity(Intent.createChooser(share_tw, getResources().getString(R.string.share_to)));
    }

    //------------------ share data to other application ---------------------//

    private void updateData() {
        if (passId.equals(statusLists.getId())) {

            Events.InfoUpdate infoUpdate = new Events.InfoUpdate(statusLists.getId(),
                    "all", statusLists.getStatus_layout(), statusLists.getStatus_type(),
                    statusLists.getTotal_viewer(), statusLists.getTotal_likes(),
                    statusLists.getAlready_like(), position);
            GlobalBus.getBus().post(infoUpdate);

        }
    }

    private void downloadStatus() {

        if (method.isNetworkAvailable()) {
            Dexter.withActivity(getActivity())
                    .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            if (Method.isDownload) {
                                method.download(statusLists.getId(),
                                        statusLists.getStatus_title(),
                                        statusLists.getCategory_name(),
                                        statusLists.getStatus_thumbnail_s(),
                                        statusLists.getStatus_thumbnail_b(),
                                        statusLists.getVideo_url(),
                                        statusLists.getStatus_layout(),
                                        statusLists.getStatus_type(),
                                        statusLists.getWatermark_image(),
                                        statusLists.getWatermark_on_off());
                                if (method.pref.getBoolean(method.pref_login, false)) {
                                    if (!method.pref.getString(method.profileId, null).equals(statusLists.getUser_id())) {
                                        downloadCount(method.pref.getString(method.profileId, null));
                                    }
                                }
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.download_later), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            // check for permanent denial of permission
                            if (response.isPermanentlyDenied()) {
                                // navigate user to app settings
                            }
                            method.alertBox(getResources().getString(R.string.cannot_use_save_permission));
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }

    }

    private void like_status() {

        if (getActivity() != null) {

            String user_id = method.pref.getString(method.profileId, null);

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "user_status_like");
            jsObj.addProperty("user_id", user_id);
            jsObj.addProperty("post_id", statusLists.getId());
            jsObj.addProperty("type", statusLists.getStatus_type());
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String msg = object.getString("msg");
                                String success = object.getString("success");

                                if (success.equals("1")) {

                                    String activity_status = object.getString("activity_status");
                                    String total_likes = object.getString("total_likes");

                                    if (activity_status.equals("1")) {
                                        statusLists.setTotal_likes(total_likes);
                                        statusLists.setAlready_like("true");
                                        textView_like.setText(method.format(Double.parseDouble(statusLists.getTotal_likes())));
                                    } else {
                                        statusLists.setTotal_likes(total_likes);
                                        statusLists.setAlready_like("false");
                                        textView_like.setText(method.format(Double.parseDouble(statusLists.getTotal_likes())));
                                    }

                                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                                    if (passId.equals(statusLists.getId())) {
                                        Events.InfoUpdate infoUpdate = new Events.InfoUpdate(statusLists.getId(),
                                                "like", statusLists.getStatus_layout(), statusLists.getStatus_type(),
                                                "", statusLists.getTotal_likes(), statusLists.getAlready_like(), position);
                                        GlobalBus.getBus().post(infoUpdate);
                                    }

                                    if (activity_status.equals("1")) {
                                        imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_hov));
                                    } else {
                                        if (method.isDarkMode()) {
                                            imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_white));
                                        } else {
                                            imageView_like.setImageDrawable(getResources().getDrawable(R.drawable.like_ic));
                                        }
                                    }

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void status_view(String profileId) {

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "single_status_view_count");
            jsObj.addProperty("post_id", statusLists.getId());
            jsObj.addProperty("user_id", profileId);
            jsObj.addProperty("owner_id", statusLists.getUser_id());
            jsObj.addProperty("type", statusLists.getStatus_type());
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String msg = object.getString("msg");
                                String success = object.getString("success");

                                if (success.equals("1")) {
                                    int total_view = Integer.parseInt(statusLists.getTotal_viewer());
                                    total_view++;
                                    statusLists.setTotal_viewer(String.valueOf(total_view));
                                    textView_view.setText(method.format(Double.parseDouble(String.valueOf(total_view))));

                                    if (passId.equals(statusLists.getId())) {

                                        Events.InfoUpdate infoUpdate = new Events.InfoUpdate(statusLists.getId(),
                                                "view", statusLists.getStatus_layout(), statusLists.getStatus_type(),
                                                statusLists.getTotal_viewer(), "", "", position);
                                        GlobalBus.getBus().post(infoUpdate);
                                    }
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void downloadCount(String profileId) {

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "single_status_download");
            jsObj.addProperty("user_id", profileId);
            jsObj.addProperty("post_id", statusLists.getId());
            jsObj.addProperty("type", statusLists.getStatus_type());
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String msg = object.getString("msg");
                                String success = object.getString("success");

                                if (success.equals("1")) {
                                    String total_download = object.getString("total_download");

                                    Events.DownloadUpdate downloadUpdate = new Events.DownloadUpdate(statusLists.getId(),
                                            statusLists.getStatus_type(), total_download);
                                    GlobalBus.getBus().post(downloadUpdate);

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void Comment(final String userId, final String comment) {

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "add_status_comment");
            jsObj.addProperty("comment_text", comment);
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("post_id", statusLists.getId());
            jsObj.addProperty("type", statusLists.getStatus_type());
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                String msg = jsonObject.getString("msg");
                                String success = jsonObject.getString("success");

                                if (success.equals("1")) {

                                    editText_comment.setText("");

                                    String total_comment = jsonObject.getString("total_comment");

                                    String id = jsonObject.getString("comment_id");
                                    String user_id = jsonObject.getString("user_id");
                                    String user_name = jsonObject.getString("user_name");
                                    String user_image = jsonObject.getString("user_image");
                                    String post_id = jsonObject.getString("post_id");
                                    String status_type = jsonObject.getString("status_type");
                                    String comment_text = jsonObject.getString("comment_text");
                                    String comment_date = jsonObject.getString("comment_date");

                                    statusLists.getCommentLists().add(0, new CommentList(id, user_id, user_name, user_image, post_id, status_type, comment_text, comment_date));

                                    if (statusLists.getCommentLists().size() == 0) {
                                        linearLayout_comment.setVisibility(View.GONE);
                                    } else {
                                        linearLayout_comment.setVisibility(View.VISIBLE);
                                        if (commentAdapter == null) {
                                            commentAdapter = new CommentAdapter(getActivity(), statusLists.getCommentLists());
                                            recyclerViewComment.setAdapter(commentAdapter);
                                        } else {
                                            commentAdapter.notifyDataSetChanged();
                                        }
                                    }

                                    String buttonTotal = getResources().getString(R.string.view_all) + " " + "(" + total_comment + ")";
                                    textView_all_comment.setText(buttonTotal);

                                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                                } else {
                                    method.alertBox(msg);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void follow(String user_id, String other_user) {

        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "user_follow");
            jsObj.addProperty("user_id", other_user);
            jsObj.addProperty("follower_id", user_id);
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String msg = object.getString("msg");
                                String success = object.getString("success");

                                if (success.equals("1")) {
                                    String activity_status = object.getString("activity_status");
                                    if (activity_status.equals("1")) {
                                        textView_follow.setText(getResources().getString(R.string.unfollow));
                                    } else {
                                        textView_follow.setText(getResources().getString(R.string.follow));
                                    }
                                    method.alertBox(msg);
                                } else {
                                    method.alertBox(msg);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                        progressDialog.dismiss();

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.dismiss();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }

}
