package com.prayosha.shareyourstatus.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Adapter.PortraitAdapter;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EndlessRecyclerViewScrollListener;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class SubCatPortraitFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private String type, id, category_name, typeLayout;
    private ProgressBar progressBar;
    private MaterialTextView textView_noData;
    private RecyclerView recyclerView;
    private List<SubCategoryList> portraitLists;
    private PortraitAdapter portraitAdapter;
    private Boolean isOver = false;
    private int pagination_index = 1;
    private LayoutAnimationController animation;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.sub_cat_fragment, container, false);

        GlobalBus.getBus().register(this);

        portraitLists = new ArrayList<>();

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {
                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, title).addToBackStack(title).commitAllowingStateLoss();
            }
        };
        method = new Method(getActivity(), onClick);

        assert getArguments() != null;
        type = getArguments().getString("type");
        id = getArguments().getString("id");
        category_name = getArguments().getString("category_name");
        typeLayout = getArguments().getString("typeLayout");

        int resId = R.anim.layout_animation_fall_down;
        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        floatingActionButton = view.findViewById(R.id.fab_sub_category);
        progressBar = view.findViewById(R.id.progressbar_sub_category);
        textView_noData = view.findViewById(R.id.textView_sub_category);

        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.landscape_ic));
        if (type.equals("category")) {
            MainActivity.toolbar.setTitle(category_name);
        } else {
            if (HomeMainFragment.floating_home != null) {
                HomeMainFragment.floating_home.setVisibility(View.VISIBLE);
                HomeMainFragment.floating_home.setImageDrawable(getResources().getDrawable(R.drawable.landscape_ic));
            }
            floatingActionButton.setVisibility(View.GONE);
        }

        recyclerView = view.findViewById(R.id.recyclerView_sub_category);


        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (portraitAdapter.getItemViewType(position)) {
                    case 0:
                        return 2;
                    case 2:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pagination_index++;
                            callData(typeLayout);
                        }
                    }, 1000);
                } else {
                    portraitAdapter.hideHeader();
                }
            }
        });

        if (HomeMainFragment.floating_home != null) {
            HomeMainFragment.floating_home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubCategoryFragment subCategoryFragment = new SubCategoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    bundle.putString("category_name", category_name);
                    bundle.putString("type", type);
                    bundle.putString("typeLayout", "Landscape");
                    subCategoryFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    assert fragmentManager != null;
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout_home_main, subCategoryFragment, category_name).commitAllowingStateLoss();
                }
            });
        }

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().popBackStack();

                SubCategoryFragment subCategoryFragment = new SubCategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("category_name", category_name);
                bundle.putString("type", type);
                bundle.putString("typeLayout", "Landscape");
                subCategoryFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, subCategoryFragment, category_name).addToBackStack("sub").commitAllowingStateLoss();

            }
        });

        callData(typeLayout);

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.ic_searchView);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (method.isNetworkAvailable()) {
                    backStackRemove();
                    SearchFragment searchFragment = new SearchFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("search_menu", query);
                    bundle.putString("typeLayout", "Landscape");
                    searchFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().replace(R.id.frameLayout_main, searchFragment, query).commitAllowingStateLoss();
                    return false;
                } else {
                    method.alertBox(getResources().getString(R.string.internet_connection));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        }));

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void backStackRemove() {
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Subscribe
    public void getNotify(Events.FavouriteNotify favouriteNotify) {
        for (int i = 0; i < portraitLists.size(); i++) {
            if (portraitLists.get(i).getId().equals(favouriteNotify.getId())) {
                if (portraitLists.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                    portraitLists.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                    portraitAdapter.notifyItemChanged(i);
                }
            }
        }
    }

    private void callData(String typLayout) {
        if (getActivity() != null) {
            if (method.isNetworkAvailable()) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    data(method.pref.getString(method.profileId, null), typLayout);
                } else {
                    data("0", typLayout);
                }
            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }

    }

    private void data(String userId, String typeLayout) {

        if (portraitAdapter == null) {
            portraitLists.clear();
            progressBar.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "status_by_cat_id");
            jsObj.addProperty("cat_id", id);
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("page", pagination_index);
            jsObj.addProperty("filter_value", typeLayout);
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String id = object.getString("id");
                                    String status_type = object.getString("status_type");
                                    String status_title = object.getString("status_title");
                                    String status_layout = object.getString("status_layout");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String is_favourite = object.getString("is_favourite");

                                    portraitLists.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, "", "", "", "", "", "", is_favourite, "", ""));

                                }

                                if (jsonArray.length() == 0) {
                                    if (portraitAdapter != null) {
                                        isOver = true;
                                        portraitAdapter.hideHeader();
                                    }
                                }

                                if (portraitAdapter == null) {
                                    if (portraitLists.size() == 0) {
                                        textView_noData.setVisibility(View.VISIBLE);
                                    } else {
                                        textView_noData.setVisibility(View.GONE);
                                        portraitAdapter = new PortraitAdapter(getActivity(), portraitLists, onClick, "sub_category");
                                        recyclerView.setAdapter(portraitAdapter);
                                        recyclerView.setLayoutAnimation(animation);
                                    }
                                } else {
                                    portraitAdapter.notifyDataSetChanged();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }

}
