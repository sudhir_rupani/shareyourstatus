package com.prayosha.shareyourstatus.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Adapter.PortraitHomeAdapter;
import com.prayosha.shareyourstatus.Adapter.SliderAdapter;
import com.prayosha.shareyourstatus.Adapter.SubCategoryAdapter;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EnchantedViewPager;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

public class HomeFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private EnchantedViewPager slider;
    private NestedScrollView nestedScrollView;
    private List<SubCategoryList> sliderArray;
    private List<SubCategoryList> portraitArray;
    private List<SubCategoryList> landscapeArray;
    private SliderAdapter sliderAdapter;
    private PortraitHomeAdapter portraitHomeAdapter;
    private SubCategoryAdapter landscapeHomeAdapter;
    private RecyclerView recyclerView_portrait, recyclerView_landscape;
    private int oldPosition = 0;
    private Boolean isOver = false;
    private int pagination_index = 1;
    private LayoutAnimationController animation;
    private FloatingActionButton floatingActionButton;
    private ProgressBar progressbar_por, progressbar_landscape;
    private MaterialTextView textView_noData_por, textView_noData_lad;

    private Timer timer;
    private Runnable Update;
    private final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 3000;
    private final Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.home_fragment, container, false);

        GlobalBus.getBus().register(this);

        if (HomeMainFragment.floating_home != null) {
            HomeMainFragment.floating_home.setVisibility(View.GONE);
        }

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {
                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, title).addToBackStack(title).commitAllowingStateLoss();
            }
        };
        method = new Method(getActivity(), onClick, null, null);

        int columnWidth = method.getScreenWidth();
        portraitArray = new ArrayList<>();
        landscapeArray = new ArrayList<>();
        sliderArray = new ArrayList<>();

        int resId = R.anim.layout_animation_fall_down;
        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        slider = view.findViewById(R.id.slider_home);
        slider.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth / 2 + 100));

        slider.useScale();
        slider.removeAlpha();

        nestedScrollView = view.findViewById(R.id.nestedScrollView_home);
        floatingActionButton = view.findViewById(R.id.fab_home);
        textView_noData_por = view.findViewById(R.id.textView_por_home);
        textView_noData_lad = view.findViewById(R.id.textView_landscape_home);
        progressbar_por = view.findViewById(R.id.progressbar_por_home);
        progressbar_landscape = view.findViewById(R.id.progressbar_landscape_home);
        recyclerView_portrait = view.findViewById(R.id.recyclerView_portrait_home);
        recyclerView_landscape = view.findViewById(R.id.recyclerView_landscape_home);
        Button button_portrait = view.findViewById(R.id.button_portrait_home);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_portrait.setLayoutManager(layoutManager);
        recyclerView_portrait.setFocusable(false);
        recyclerView_portrait.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager_landscape = new LinearLayoutManager(getContext());
        recyclerView_landscape.setLayoutManager(layoutManager_landscape);
        recyclerView_landscape.setFocusable(false);
        recyclerView_landscape.setNestedScrollingEnabled(false);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        int visibleItemCount = layoutManager_landscape.getChildCount();
                        int totalItemCount = layoutManager_landscape.getItemCount();
                        int pastVisiblesItems = layoutManager_landscape.findFirstVisibleItemPosition();

                        if (totalItemCount > 5) {
                            floatingActionButton.show();
                        } else {
                            floatingActionButton.hide();
                        }

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (!isOver) {
                                oldPosition = landscapeArray.size();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        pagination_index++;
                                        callData_Landscape();
                                    }
                                }, 1000);
                            } else {
                                landscapeHomeAdapter.hideHeader();
                            }
                        }
                    }
                }
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nestedScrollView.scrollTo(0, 0);
            }
        });

        button_portrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = getResources().getString(R.string.portrait_status);
                MainActivity.toolbar.setTitle(tag);
                PortraitFragment portraitFragment = new PortraitFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", tag);
                portraitFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, portraitFragment, tag).addToBackStack(tag).commitAllowingStateLoss();
            }
        });

        callData_Other();
        callData_Landscape();

        return view;
    }

    private void callData_Other() {
        if (method.isNetworkAvailable()) {
            if (method.pref.getBoolean(method.pref_login, false)) {
                home(method.pref.getString(method.profileId, null));
            } else {
                home("0");
            }
        } else {
            method.alertBox(getResources().getString(R.string.internet_connection));
        }
    }

    private void callData_Landscape() {
        if (getActivity() != null) {
            if (method.isNetworkAvailable()) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    landscape(method.pref.getString(method.profileId, null));
                } else {
                    landscape("0");
                }
            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }
    }

    @Subscribe
    public void getNotify(Events.FavouriteNotify favouriteNotify) {
        if (portraitHomeAdapter != null && landscapeHomeAdapter != null) {
            if (favouriteNotify.getStatus_layout().equals("Portrait")) {
                for (int i = 0; i < portraitArray.size(); i++) {
                    if (portraitArray.get(i).getId().equals(favouriteNotify.getId())) {
                        if (portraitArray.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                            portraitArray.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                            portraitHomeAdapter.notifyItemChanged(i);
                        }
                    }
                }
            } else {
                for (int i = 0; i < landscapeArray.size(); i++) {
                    if (landscapeArray.get(i).getId().equals(favouriteNotify.getId())) {
                        if (landscapeArray.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                            landscapeArray.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                            landscapeHomeAdapter.notifyItemChanged(i);
                        }
                    }
                }
            }
        }
    }

    @Subscribe
    public void getMessage(Events.InfoUpdate infoUpdate) {
        if (landscapeHomeAdapter != null) {
            for (int i = 0; i < landscapeArray.size(); i++) {
                if (landscapeArray.get(i).getId().equals(infoUpdate.getId())) {
                    if (landscapeArray.get(i).getStatus_type().equals(infoUpdate.getStatus_type())) {
                        switch (infoUpdate.getType()) {
                            case "all":
                                landscapeArray.get(i).setTotal_viewer(infoUpdate.getView());
                                landscapeArray.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                landscapeArray.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                            case "view":
                                landscapeArray.get(i).setTotal_viewer(infoUpdate.getView());
                                break;
                            case "like":
                                landscapeArray.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                landscapeArray.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                        }
                        landscapeHomeAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }

    private void home(String userId) {

        sliderArray.clear();
        portraitArray.clear();
        progressbar_por.setVisibility(View.VISIBLE);

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            jsObj.addProperty("method_name", "home");
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);

                                JSONArray jsonArray_slider = object.getJSONArray("slider_status");

                                for (int i = 0; i < jsonArray_slider.length(); i++) {

                                    JSONObject object_slider = jsonArray_slider.getJSONObject(i);
                                    String id = object_slider.getString("id");
                                    String status_type = object_slider.getString("status_type");
                                    String status_title = object_slider.getString("status_title");
                                    String status_layout = object_slider.getString("status_layout");
                                    String status_thumbnail_b = object_slider.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object_slider.getString("status_thumbnail_s");
                                    String total_viewer = object_slider.getString("total_viewer");
                                    String quote_bg = object_slider.getString("quote_bg");
                                    String quote_font = object_slider.getString("quote_font");
                                    String external_link = object_slider.getString("external_link");

                                    sliderArray.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, "", "", "", quote_bg, quote_font, "", "", external_link));

                                }

                                JSONArray jsonArray_portrait = object.getJSONArray("portrait_status");

                                for (int i = 0; i < jsonArray_portrait.length(); i++) {

                                    JSONObject object_portrait = jsonArray_portrait.getJSONObject(i);
                                    String id = object_portrait.getString("id");
                                    String status_type = object_portrait.getString("status_type");
                                    String status_title = object_portrait.getString("status_title");
                                    String status_layout = object_portrait.getString("status_layout");
                                    String status_thumbnail_b = object_portrait.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object_portrait.getString("status_thumbnail_s");
                                    String is_favourite = object_portrait.getString("is_favourite");

                                    portraitArray.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, "", "", "", "", "", "", is_favourite, "", ""));

                                }

                                if (sliderArray.size() != 0) {

                                    sliderAdapter = new SliderAdapter(getActivity(), "slider", sliderArray, onClick);
                                    slider.setAdapter(sliderAdapter);

                                    Update = new Runnable() {
                                        public void run() {
                                            if (slider.getCurrentItem() == (sliderAdapter.getCount() - 1)) {
                                                slider.setCurrentItem(0, true);
                                            } else {
                                                slider.setCurrentItem(slider.getCurrentItem() + 1, true);
                                            }
                                        }
                                    };

                                    if (sliderAdapter.getCount() > 1) {
                                        timer = new Timer(); // This will create a new Thread
                                        timer.schedule(new TimerTask() { // task to be scheduled
                                            @Override
                                            public void run() {
                                                handler.post(Update);
                                            }
                                        }, DELAY_MS, PERIOD_MS);
                                    }

                                }

                                if (portraitArray.size() != 0) {
                                    textView_noData_por.setVisibility(View.GONE);
                                    portraitHomeAdapter = new PortraitHomeAdapter(getActivity(), portraitArray, onClick, getResources().getString(R.string.portrait_status));
                                    recyclerView_portrait.setAdapter(portraitHomeAdapter);
                                } else {
                                    textView_noData_por.setVisibility(View.VISIBLE);
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressbar_por.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressbar_por.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    private void landscape(String userId) {

        if (landscapeHomeAdapter == null) {
            landscapeArray.clear();
            progressbar_landscape.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("page", pagination_index);
            jsObj.addProperty("method_name", "landscape_status");
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            client.cancelAllRequests(true);
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String id = object.getString("id");
                                    String status_type = object.getString("status_type");
                                    String status_title = object.getString("status_title");
                                    String status_layout = object.getString("status_layout");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String total_likes = object.getString("total_likes");
                                    String total_viewer = object.getString("total_viewer");
                                    String category_name = object.getString("category_name");
                                    String already_like = object.getString("already_like");
                                    String quote_bg = object.getString("quote_bg");
                                    String quote_font = object.getString("quote_font");
                                    String is_favourite = object.getString("is_favourite");

                                    landscapeArray.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, already_like, category_name, quote_bg, quote_font, is_favourite, "", ""));

                                }

                                if (jsonArray.length() == 0) {
                                    if (landscapeHomeAdapter != null) {
                                        isOver = true;
                                        landscapeHomeAdapter.hideHeader();
                                    }
                                }

                                if (landscapeHomeAdapter == null) {
                                    if (landscapeArray.size() != 0) {
                                        landscapeHomeAdapter = new SubCategoryAdapter(getActivity(), landscapeArray, onClick, "home_sub");
                                        recyclerView_landscape.setAdapter(landscapeHomeAdapter);
                                        recyclerView_landscape.setLayoutAnimation(animation);
                                        textView_noData_lad.setVisibility(View.GONE);
                                    } else {
                                        textView_noData_lad.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    landscapeHomeAdapter.notifyItemMoved(oldPosition, landscapeArray.size());
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressbar_landscape.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressbar_landscape.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }

}
