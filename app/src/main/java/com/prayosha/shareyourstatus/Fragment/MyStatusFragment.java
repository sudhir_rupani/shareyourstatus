package com.prayosha.shareyourstatus.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Adapter.MyStatusAdapter;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EndlessRecyclerViewScrollListener;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MyStatusFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private ProgressBar progressBar;
    private String typeLayout, type, id;
    private MaterialTextView textView_noData;
    private RecyclerView recyclerView;
    private MyStatusAdapter myStatusAdapter;
    private List<SubCategoryList> myVideoLists;
    private LayoutAnimationController animation;
    private Boolean isOver = false;
    private int pagination_index = 1;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.my_upload_fragment, container, false);

        GlobalBus.getBus().register(this);

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {
                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, title).addToBackStack(title).commitAllowingStateLoss();
            }
        };
        method = new Method(getActivity(), onClick);

        myVideoLists = new ArrayList<>();

        assert getArguments() != null;
        typeLayout = getArguments().getString("typeLayout");
        type = getArguments().getString("type");
        id = getArguments().getString("id");

        int resId = R.anim.layout_animation_fall_down;
        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        progressBar = view.findViewById(R.id.progressbar_myUpload_fragment);
        textView_noData = view.findViewById(R.id.textView_myUpload_fragment);
        recyclerView = view.findViewById(R.id.recyclerView_myUpload_fragment);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pagination_index++;
                            callData();
                        }
                    }, 1000);
                } else {
                    myStatusAdapter.hideHeader();
                }
            }
        });

        callData();

        return view;

    }

    private void callData() {
        if (getActivity() != null) {
            if (method.isNetworkAvailable()) {
                if (getActivity() != null) {
                    MyVideo(id, typeLayout);
                }
            } else {
                recyclerView.setVisibility(View.GONE);
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }
    }

    @Subscribe
    public void getMessage(Events.InfoUpdate infoUpdate) {
        if (myStatusAdapter != null) {
            for (int i = 0; i < myVideoLists.size(); i++) {
                if (myVideoLists.get(i).getId().equals(infoUpdate.getId())) {
                    if (myVideoLists.get(i).getStatus_type().equals(infoUpdate.getStatus_type())) {
                        switch (infoUpdate.getType()) {
                            case "all":
                                myVideoLists.get(i).setTotal_viewer(infoUpdate.getView());
                                myVideoLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                myVideoLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                            case "view":
                                myVideoLists.get(i).setTotal_viewer(infoUpdate.getView());
                                break;
                            case "like":
                                myVideoLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                myVideoLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                        }
                        myStatusAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }

    private void MyVideo(final String id, String typeLayout) {

        if (myStatusAdapter == null) {
            myVideoLists.clear();
            progressBar.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "user_status_list");
            jsObj.addProperty("user_id", id);
            if (method.pref.getBoolean(method.pref_login, false)) {
                if (id.equals(method.pref.getString(method.profileId, null))) {
                    jsObj.addProperty("login_user", "true");
                } else {
                    jsObj.addProperty("login_user", "false");
                }
            } else {
                jsObj.addProperty("login_user", "false");
            }
            jsObj.addProperty("page", pagination_index);
            jsObj.addProperty("filter_value", typeLayout);
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String id = object.getString("id");
                                    String status_type = object.getString("status_type");
                                    String status_title = object.getString("status_title");
                                    String status_layout = object.getString("status_layout");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String total_likes = object.getString("total_likes");
                                    String total_viewer = object.getString("total_viewer");
                                    String category_name = object.getString("category_name");
                                    String already_like = object.getString("already_like");
                                    String quote_bg = object.getString("quote_bg");
                                    String quote_font = object.getString("quote_font");
                                    String is_reviewed = object.getString("is_reviewed");

                                    myVideoLists.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, already_like, category_name, quote_bg, quote_font, "",is_reviewed, ""));

                                }

                                if (jsonArray.length() == 0) {
                                    if (myStatusAdapter != null) {
                                        myStatusAdapter.hideHeader();
                                        isOver = true;
                                    }
                                }

                                if (myStatusAdapter == null) {
                                    if (myVideoLists.size() == 0) {
                                        textView_noData.setVisibility(View.VISIBLE);
                                    } else {
                                        textView_noData.setVisibility(View.GONE);
                                        myStatusAdapter = new MyStatusAdapter(getActivity(), myVideoLists, id, "my_video", onClick);
                                        recyclerView.setAdapter(myStatusAdapter);
                                        recyclerView.setLayoutAnimation(animation);
                                    }
                                } else {
                                    myStatusAdapter.notifyDataSetChanged();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }


}
