package com.prayosha.shareyourstatus.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Adapter.DownloadAdapter;
import com.prayosha.shareyourstatus.DataBase.DatabaseHandler;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textview.MaterialTextView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DownloadFragment extends Fragment {

    private Method method;
    private DatabaseHandler db;
    private String typeLayout;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private FloatingActionButton fab_button;
    private MaterialTextView textView_noData_found;
    private List<File> inFiles;
    private List<SubCategoryList> subCategoryLists;
    private List<SubCategoryList> downloadListsCompair;
    private DownloadAdapter downloadAdapter;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.sub_cat_fragment, container, false);

        MainActivity.toolbar.setTitle(getResources().getString(R.string.my_download));

        method = new Method(getActivity());

        assert getArguments() != null;
        typeLayout = getArguments().getString("typeLayout");

        db = new DatabaseHandler(getActivity());

        inFiles = new ArrayList<>();
        subCategoryLists = new ArrayList<>();
        downloadListsCompair = new ArrayList<>();

        progressBar = view.findViewById(R.id.progressbar_sub_category);
        textView_noData_found = view.findViewById(R.id.textView_sub_category);
        fab_button = view.findViewById(R.id.fab_sub_category);
        recyclerView = view.findViewById(R.id.recyclerView_sub_category);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        fab_button.setImageDrawable(getResources().getDrawable(R.drawable.portrait_ic));
        progressBar.setVisibility(View.GONE);

        fab_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadPortraitFragment portraitFragment = new DownloadPortraitFragment();
                Bundle bundle_fav = new Bundle();
                bundle_fav.putString("typeLayout", "Portrait");
                portraitFragment.setArguments(bundle_fav);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, portraitFragment, getResources().getString(R.string.favorites)).commit();
            }
        });

        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        new Execute().execute();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                        method.alertBox(getResources().getString(R.string.cannot_use_save_permission));
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();


        setHasOptionsMenu(true);
        return view;

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.ic_searchView);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (method.isNetworkAvailable()) {
                    backStackRemove();
                    SearchFragment searchFragment = new SearchFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("search_menu", query);
                    bundle.putString("typeLayout", "Landscape");
                    searchFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().replace(R.id.frameLayout_main, searchFragment, query).commitAllowingStateLoss();
                    return false;
                } else {
                    method.alertBox(getResources().getString(R.string.internet_connection));
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        }));

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void backStackRemove() {
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class Execute extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);
            subCategoryLists.clear();
            inFiles.clear();
            downloadListsCompair.clear();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            //get data in database
            subCategoryLists = db.getVideoDownload(typeLayout);

            //video file
            File file = new File(Constant_Api.video_path);
            getVideoFileList(file);

            //image and gif file
            File image_file = new File(Constant_Api.image_path);
            getImageFileList(image_file);

            getDownloadLists(inFiles);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (downloadListsCompair.size() == 0) {
                textView_noData_found.setVisibility(View.VISIBLE);
            } else {
                textView_noData_found.setVisibility(View.GONE);
                downloadAdapter = new DownloadAdapter(getActivity(), downloadListsCompair);
                recyclerView.setAdapter(downloadAdapter);
            }

            progressBar.setVisibility(View.GONE);

            super.onPostExecute(s);
        }
    }

    private void getVideoFileList(File parentDir) {
        try {
            Queue<File> files = new LinkedList<>();
            files.addAll(Arrays.asList(parentDir.listFiles()));
            while (!files.isEmpty()) {
                File file = files.remove();
                if (file.isDirectory()) {
                    files.addAll(Arrays.asList(file.listFiles()));
                } else if (file.getName().endsWith(".mp4")) {
                    inFiles.add(file);
                }
            }
        } catch (Exception e) {
            Log.d("error", e.toString());
        }
    }


    private void getImageFileList(File parentDir) {
        try {
            Queue<File> files = new LinkedList<>();
            files.addAll(Arrays.asList(parentDir.listFiles()));
            while (!files.isEmpty()) {
                File file = files.remove();
                if (file.isDirectory()) {
                    files.addAll(Arrays.asList(file.listFiles()));
                } else if (file.getName().endsWith(".gif") || file.getName().endsWith(".jpg")) {
                    inFiles.add(file);
                }
            }
        } catch (Exception e) {
            Log.d("error", e.toString());
        }
    }

    //check image, gif, video file available or not
    private List<SubCategoryList> getDownloadLists(List<File> list) {

        for (int i = 0; i < subCategoryLists.size(); i++) {

            String file_type = subCategoryLists.get(i).getStatus_type();

            if (file_type.equals("video")) {
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).toString().contains(subCategoryLists.get(i).getVideo_url())) {
                        downloadListsCompair.add(subCategoryLists.get(i));
                        break;
                    } else {
                        if (j == list.size() - 1) {
                            db.delete_status_download(subCategoryLists.get(i).getId(), subCategoryLists.get(i).getStatus_type());
                        }
                    }
                }
            } else {
                if (file_type.equals("gif")) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).toString().contains(subCategoryLists.get(i).getStatus_thumbnail_b())) {
                            downloadListsCompair.add(subCategoryLists.get(i));
                            break;
                        } else {
                            if (j == list.size() - 1) {
                                db.delete_status_download(subCategoryLists.get(i).getId(), subCategoryLists.get(i).getStatus_type());
                            }
                        }
                    }
                } else {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).toString().contains(subCategoryLists.get(i).getGif_url())) {
                            downloadListsCompair.add(subCategoryLists.get(i));
                            break;
                        } else {
                            if (j == list.size() - 1) {
                                db.delete_status_download(subCategoryLists.get(i).getId(), subCategoryLists.get(i).getStatus_type());
                            }
                        }
                    }
                }
            }
        }
        return downloadListsCompair;
    }

}
