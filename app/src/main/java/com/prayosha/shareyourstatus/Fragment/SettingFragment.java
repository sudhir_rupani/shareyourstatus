package com.prayosha.shareyourstatus.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.prayosha.shareyourstatus.Activity.AVStatus;
import com.prayosha.shareyourstatus.Activity.AboutUs;
import com.prayosha.shareyourstatus.Activity.AccountVerification;
import com.prayosha.shareyourstatus.Activity.ContactUs;
import com.prayosha.shareyourstatus.Activity.DeleteAccount;
import com.prayosha.shareyourstatus.Activity.EarnPoint;
import com.prayosha.shareyourstatus.Activity.Faq;
import com.prayosha.shareyourstatus.Activity.Language;
import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Activity.PrivacyPolice;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;

import cz.msebera.android.httpclient.Header;


public class SettingFragment extends Fragment {

    private Method method;
    private ProgressDialog progressDialog;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.setting_fragment, container, false);

        MainActivity.toolbar.setTitle(getResources().getString(R.string.setting));

        method = new Method(getActivity());

        progressDialog = new ProgressDialog(getActivity());

        SwitchMaterial switchMaterial = view.findViewById(R.id.switch_setting);
        MaterialTextView textView_shareApp = view.findViewById(R.id.textView_shareApp_setting);
        MaterialTextView textView_rateApp = view.findViewById(R.id.textView_rateApp_setting);
        MaterialTextView textView_moreApp = view.findViewById(R.id.textView_moreApp_setting);
        MaterialTextView textView_privacy_policy = view.findViewById(R.id.textView_privacy_policy_setting);
        MaterialTextView textView_aboutUs = view.findViewById(R.id.textView_aboutUs_setting);
        MaterialTextView textView_language = view.findViewById(R.id.textView_language_setting);
        MaterialTextView textView_contactUs = view.findViewById(R.id.textView_contactUs_setting);
        MaterialTextView textView_faq = view.findViewById(R.id.textView_faq_setting);
        MaterialTextView textView_point = view.findViewById(R.id.textView_point_setting);
        MaterialTextView textView_verification = view.findViewById(R.id.textView_verification_setting);
        MaterialTextView textView_deleteAccount = view.findViewById(R.id.textView_deleteAccount_setting);
        View view_deleteAccount = view.findViewById(R.id.view_deleteAccount_setting);
        View view_verification = view.findViewById(R.id.view_verification_setting);
        final MaterialTextView textViewSize = view.findViewById(R.id.textView_size_setting);
        ImageView imageView_clear = view.findViewById(R.id.imageView_clear_setting);

        if (method.pref.getBoolean(method.pref_login, false)) {
            textView_deleteAccount.setVisibility(View.VISIBLE);
            view_deleteAccount.setVisibility(View.VISIBLE);
            textView_verification.setVisibility(View.VISIBLE);
            view_verification.setVisibility(View.VISIBLE);
        } else {
            textView_deleteAccount.setVisibility(View.GONE);
            view_deleteAccount.setVisibility(View.GONE);
            textView_verification.setVisibility(View.GONE);
            view_verification.setVisibility(View.GONE);
        }

        double total = 0;
        String root = getActivity().getExternalCacheDir().getAbsolutePath();
        try {
            File file = new File(root);
            if (file.isDirectory()) {
                String[] children = file.list();
                for (String aChildren : children) {
                    File name = new File(root + "/" + aChildren);
                    total += getFileSizeMegaBytes(name);
                }
            }
        } catch (Exception e) {
            textViewSize.setText("Size " + "0.0" + " mb");
        }
        textViewSize.setText(getResources().getString(R.string.size) + " "
                + new DecimalFormat("##.##").format(total) + " "
                + getResources().getString(R.string.mb));

        imageView_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String root = getActivity().getExternalCacheDir().getAbsolutePath();
                File file = new File(root);
                if (file.isDirectory()) {
                    String[] children = file.list();
                    for (String aChildren : children) {
                        new File(file, aChildren).delete();
                    }
                    Toast.makeText(getActivity(), getResources().getString(R.string.locally_cached_data), Toast.LENGTH_SHORT).show();
                    textViewSize.setText(getResources().getString(R.string.size) + " "
                            + "0.0" + " "
                            + getResources().getString(R.string.mb));
                }
            }
        });

        if (method.pref.getBoolean(method.notification, true)) {
            switchMaterial.setChecked(true);
        } else {
            switchMaterial.setChecked(false);
        }

        switchMaterial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    OneSignal.setSubscription(true);
                } else {
                    OneSignal.setSubscription(false);
                }
                method.editor.putBoolean(method.notification, isChecked);
                method.editor.commit();
            }
        });

        textView_shareApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareApp();
            }
        });

        textView_rateApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateApp();
            }
        });

        textView_moreApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moreApp();
            }
        });

        textView_aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AboutUs.class));
            }
        });

        textView_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PrivacyPolice.class));
            }
        });

        textView_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Language.class)
                        .putExtra("type", "setting"));
            }
        });

        textView_contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ContactUs.class));
            }
        });

        textView_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Faq.class));
            }
        });

        textView_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), EarnPoint.class));
            }
        });

        textView_verification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (method.isNetworkAvailable()) {
                    if (method.pref.getBoolean(method.pref_login, false)) {
                        request(method.pref.getString(method.profileId, null));
                    } else {
                        method.alertBox(getResources().getString(R.string.you_have_not_login));
                    }
                } else {
                    method.alertBox(getResources().getString(R.string.internet_connection));
                }
            }
        });

        textView_deleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    startActivity(new Intent(getActivity(), DeleteAccount.class));
                } else {
                    method.alertBox(getResources().getString(R.string.you_have_not_login));
                }
            }
        });

        setHasOptionsMenu(false);
        return view;

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void request(String user_id) {

        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "profile_status");
            jsObj.addProperty("user_id", user_id);
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONObject object = jsonObject.getJSONObject(Constant_Api.tag);
                                String success = object.getString("success");

                                if (success.equals("1")) {

                                    String status = object.getString("status");
                                    String message = object.getString("message");
                                    switch (status) {
                                        case "0":
                                            startActivity(new Intent(getActivity(), AVStatus.class));
                                            break;
                                        case "1":
                                            startActivity(new Intent(getActivity(), AVStatus.class));
                                            break;
                                        case "2":
                                            startActivity(new Intent(getActivity(), AVStatus.class));
                                            break;
                                        case "3":
                                            startActivity(new Intent(getActivity(), AccountVerification.class));
                                            break;
                                    }

                                } else {
                                    method.alertBox(getResources().getString(R.string.wrong));
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }
                    }

                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.dismiss();
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });
        }

    }

    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getApplication().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getApplication().getPackageName())));
        }
    }

    private void moreApp() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.play_more_app))));
    }

    private void shareApp() {

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String sAux = "\n" + getResources().getString(R.string.Let_me_recommend_you_this_application) + "\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getActivity().getApplication().getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }

    }

    private static double getFileSizeMegaBytes(File file) {
        return (double) file.length() / (1024 * 1024);
    }

}
