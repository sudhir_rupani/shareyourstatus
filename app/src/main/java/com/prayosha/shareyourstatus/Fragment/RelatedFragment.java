package com.prayosha.shareyourstatus.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Adapter.SubCategoryAdapter;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EndlessRecyclerViewScrollListener;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class RelatedFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private String type, post_id, cat_id, typeLayout;
    private ProgressBar progressBar;
    private MaterialTextView textView_noData;
    private RecyclerView recyclerView;
    private List<SubCategoryList> relatedLists;
    private SubCategoryAdapter subCategoryAdapter;
    private LayoutAnimationController animation;
    private Boolean isOver = false;
    private int pagination_index = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.portrait_fragment, container, false);

        GlobalBus.getBus().register(this);

        MainActivity.toolbar.setTitle(getResources().getString(R.string.related_status));

        relatedLists = new ArrayList<>();

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {

                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, title).addToBackStack(title).commitAllowingStateLoss();

            }
        };
        method = new Method(getActivity(), onClick);

        assert getArguments() != null;
        type = getArguments().getString("type");
        post_id = getArguments().getString("post_id");
        cat_id = getArguments().getString("cat_id");
        typeLayout = getArguments().getString("typeLayout");

        int resId = R.anim.layout_animation_fall_down;
        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        progressBar = view.findViewById(R.id.progressbar_portrait_fragment);
        textView_noData = view.findViewById(R.id.textView_portrait_fragment);

        recyclerView = view.findViewById(R.id.recyclerView_portrait_fragment);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pagination_index++;
                            callData();
                        }
                    }, 1000);
                } else {
                    subCategoryAdapter.hideHeader();
                }
            }
        });

        callData();

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.ic_searchView);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (method.isNetworkAvailable()) {
                    backStackRemove();
                    SearchFragment searchFragment = new SearchFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("search_menu", query);
                    bundle.putString("typeLayout", "Landscape");
                    searchFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().replace(R.id.frameLayout_main, searchFragment, query).commitAllowingStateLoss();
                    return false;
                } else {
                    method.alertBox(getResources().getString(R.string.internet_connection));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        }));

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void backStackRemove() {
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Subscribe
    public void getNotify(Events.FavouriteNotify favouriteNotify) {
        for (int i = 0; i < relatedLists.size(); i++) {
            if (relatedLists.get(i).getId().equals(favouriteNotify.getId())) {
                if (relatedLists.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                    relatedLists.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                    subCategoryAdapter.notifyItemChanged(i);
                }
            }
        }
    }

    @Subscribe
    public void getMessage(Events.InfoUpdate infoUpdate) {
        if (subCategoryAdapter != null) {
            for (int i = 0; i < relatedLists.size(); i++) {
                if (relatedLists.get(i).getId().equals(infoUpdate.getId())) {
                    if (relatedLists.get(i).getStatus_type().equals(infoUpdate.getStatus_type())) {
                        switch (infoUpdate.getType()) {
                            case "all":
                                relatedLists.get(i).setTotal_viewer(infoUpdate.getView());
                                relatedLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                relatedLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                            case "view":
                                relatedLists.get(i).setTotal_viewer(infoUpdate.getView());
                                break;
                            case "like":
                                relatedLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                relatedLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                        }
                        subCategoryAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }

    private void callData() {
        if (getActivity() != null) {
            if (method.isNetworkAvailable()) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    related(method.pref.getString(method.profileId, null), post_id, cat_id, typeLayout);
                } else {
                    related("0", post_id, cat_id, typeLayout);
                }
            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }
    }

    private void related(String userId, String post_id, String cat_id, String typeLayout) {

        if (subCategoryAdapter == null) {
            relatedLists.clear();
            progressBar.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "related_status");
            jsObj.addProperty("post_id", post_id);
            jsObj.addProperty("cat_id", cat_id);
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("page", pagination_index);
            jsObj.addProperty("filter_value", typeLayout);
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("-2")) {
                                    method.suspend(message);
                                } else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String id = object.getString("id");
                                    String status_type = object.getString("status_type");
                                    String status_title = object.getString("status_title");
                                    String status_layout = object.getString("status_layout");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String total_likes = object.getString("total_likes");
                                    String total_viewer = object.getString("total_viewer");
                                    String category_name = object.getString("category_name");
                                    String already_like = object.getString("already_like");
                                    String quote_bg = object.getString("quote_bg");
                                    String quote_font = object.getString("quote_font");
                                    String is_favourite = object.getString("is_favourite");

                                    relatedLists.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, already_like, category_name, quote_bg, quote_font, is_favourite, "", ""));

                                }

                                if (jsonArray.length() == 0) {
                                    if (subCategoryAdapter != null) {
                                        isOver = true;
                                        subCategoryAdapter.hideHeader();
                                    }
                                }

                                if (subCategoryAdapter == null) {
                                    if (relatedLists.size() == 0) {
                                        textView_noData.setVisibility(View.VISIBLE);
                                    } else {
                                        textView_noData.setVisibility(View.GONE);
                                        subCategoryAdapter = new SubCategoryAdapter(getActivity(), relatedLists, onClick, type);
                                        recyclerView.setAdapter(subCategoryAdapter);
                                        recyclerView.setLayoutAnimation(animation);
                                    }
                                } else {
                                    subCategoryAdapter.notifyDataSetChanged();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }

}
