package com.prayosha.shareyourstatus.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import com.prayosha.shareyourstatus.Activity.MainActivity;
import com.prayosha.shareyourstatus.Adapter.SubCategoryAdapter;
import com.prayosha.shareyourstatus.InterFace.OnClick;
import com.prayosha.shareyourstatus.Item.SubCategoryList;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.API;
import com.prayosha.shareyourstatus.Util.Constant_Api;
import com.prayosha.shareyourstatus.Util.EndlessRecyclerViewScrollListener;
import com.prayosha.shareyourstatus.Util.Events;
import com.prayosha.shareyourstatus.Util.GlobalBus;
import com.prayosha.shareyourstatus.Util.Method;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cz.msebera.android.httpclient.Header;

public class SearchFragment extends Fragment {

    private Method method;
    private OnClick onClick;
    private String search, typeLayout;
    private ProgressBar progressBar;
    private MaterialTextView textView_noData;
    private RecyclerView recyclerView;
    private List<SubCategoryList> searchLists;
    private SubCategoryAdapter subCategoryAdapter;
    private LayoutAnimationController animation;
    private Boolean isOver = false;
    private int pagination_index = 1;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.sub_cat_fragment, container, false);

        GlobalBus.getBus().register(this);

        searchLists = new ArrayList<>();

        onClick = new OnClick() {
            @Override
            public void position(int position, String title, String type, String status_type, String id, String tag) {
                SCDetailFragment scDetailFragment = new SCDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", searchLists.get(position).getId());
                bundle.putString("type", type);
                bundle.putString("status_type", status_type);
                bundle.putInt("position", position);
                scDetailFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frameLayout_main, scDetailFragment, title).addToBackStack(title).commitAllowingStateLoss();
            }
        };
        method = new Method(getActivity(), onClick);

        search = getArguments().getString("search_menu");
        typeLayout = getArguments().getString("typeLayout");
        MainActivity.toolbar.setTitle(search);

        int resId = R.anim.layout_animation_fall_down;
        animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);

        floatingActionButton = view.findViewById(R.id.fab_sub_category);
        progressBar = view.findViewById(R.id.progressbar_sub_category);
        textView_noData = view.findViewById(R.id.textView_sub_category);

        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.portrait_ic));

        recyclerView = view.findViewById(R.id.recyclerView_sub_category);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pagination_index++;
                            callData();
                        }
                    }, 1000);
                } else {
                    subCategoryAdapter.hideHeader();
                }
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchPortraitFragment searchPortraitFragment = new SearchPortraitFragment();
                Bundle bundle = new Bundle();
                bundle.putString("search_menu", search);
                bundle.putString("typeLayout", "Portrait");
                searchPortraitFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_main, searchPortraitFragment, search).commitAllowingStateLoss();
            }
        });

        callData();

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Subscribe
    public void getNotify(Events.FavouriteNotify favouriteNotify) {
        for (int i = 0; i < searchLists.size(); i++) {
            if (searchLists.get(i).getId().equals(favouriteNotify.getId())) {
                if (searchLists.get(i).getStatus_type().equals(favouriteNotify.getStatus_type())) {
                    searchLists.get(i).setIs_favourite(favouriteNotify.getIs_favourite());
                    subCategoryAdapter.notifyItemChanged(i);
                }
            }
        }
    }

    @Subscribe
    public void getNotify(Events.InfoUpdate infoUpdate) {
        if (subCategoryAdapter != null) {
            for (int i = 0; i < searchLists.size(); i++) {
                if (searchLists.get(i).getId().equals(infoUpdate.getId())) {
                    if (searchLists.get(i).getStatus_type().equals(infoUpdate.getStatus_type())) {
                        switch (infoUpdate.getType()) {
                            case "all":
                                searchLists.get(i).setTotal_viewer(infoUpdate.getView());
                                searchLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                searchLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                            case "view":
                                searchLists.get(i).setTotal_viewer(infoUpdate.getView());
                                break;
                            case "like":
                                searchLists.get(i).setTotal_likes(infoUpdate.getTotal_like());
                                searchLists.get(i).setAlready_like(infoUpdate.getAlready_like());
                                break;
                        }
                        subCategoryAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }

    private void callData() {
        if (getActivity() != null) {
            if (method.isNetworkAvailable()) {
                if (method.pref.getBoolean(method.pref_login, false)) {
                    subCategory(method.pref.getString(method.profileId, null));
                } else {
                    subCategory("0");
                }
            } else {
                method.alertBox(getResources().getString(R.string.internet_connection));
            }
        }
    }

    private void subCategory(String userId) {

        if (subCategoryAdapter == null) {
            searchLists.clear();
            progressBar.setVisibility(View.VISIBLE);
        }

        if (getActivity() != null) {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API(getActivity()));
            jsObj.addProperty("method_name", "search_status");
            jsObj.addProperty("search_text", search);
            jsObj.addProperty("user_id", userId);
            jsObj.addProperty("page", pagination_index);
            jsObj.addProperty("filter_value", typeLayout);
            jsObj.addProperty("lang_ids", method.pref.getString(method.language_ids, ""));
            params.put("data", API.toBase64(jsObj.toString()));
            client.post(Constant_Api.url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (getActivity() != null) {

                        String res = new String(responseBody);

                        try {
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has(Constant_Api.STATUS)) {

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if(status.equals("-2")){
                                    method.suspend(message);
                                }else {
                                    method.alertBox(message);
                                }

                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.tag);

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String id = object.getString("id");
                                    String status_type = object.getString("status_type");
                                    String status_title = object.getString("status_title");
                                    String status_layout = object.getString("status_layout");
                                    String status_thumbnail_b = object.getString("status_thumbnail_b");
                                    String status_thumbnail_s = object.getString("status_thumbnail_s");
                                    String total_likes = object.getString("total_likes");
                                    String total_viewer = object.getString("total_viewer");
                                    String category_name = object.getString("category_name");
                                    String already_like = object.getString("already_like");
                                    String quote_bg = object.getString("quote_bg");
                                    String quote_font = object.getString("quote_font");
                                    String is_favourite = object.getString("is_favourite");

                                    searchLists.add(new SubCategoryList("", id, status_type, status_title, status_layout, status_thumbnail_b, status_thumbnail_s, total_viewer, total_likes, already_like,category_name, quote_bg, quote_font,is_favourite,"",""));

                                }

                                if (jsonArray.length() == 0) {
                                    if (subCategoryAdapter != null) {
                                        isOver = true;
                                        subCategoryAdapter.hideHeader();
                                    }
                                }

                                if (subCategoryAdapter == null) {
                                    if (searchLists.size() == 0) {
                                        textView_noData.setVisibility(View.VISIBLE);
                                    } else {
                                        textView_noData.setVisibility(View.GONE);
                                        subCategoryAdapter = new SubCategoryAdapter(getActivity(), searchLists, onClick, "search_menu");
                                        recyclerView.setAdapter(subCategoryAdapter);
                                        recyclerView.setLayoutAnimation(animation);
                                    }
                                } else {
                                    subCategoryAdapter.notifyDataSetChanged();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            method.alertBox(getResources().getString(R.string.failed_try_again));
                        }

                    }

                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressBar.setVisibility(View.GONE);
                    method.alertBox(getResources().getString(R.string.failed_try_again));
                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Unregister the registered event.
        GlobalBus.getBus().unregister(this);
    }

}
