package com.prayosha.shareyourstatus.Service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.daasuu.mp4compose.composer.Mp4Composer;
import com.daasuu.mp4compose.filter.GlWatermarkFilter;
import com.prayosha.shareyourstatus.DataBase.DatabaseHandler;
import com.prayosha.shareyourstatus.R;
import com.prayosha.shareyourstatus.Util.Method;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressUIListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

public class DownloadVideoService extends Service {

    private DatabaseHandler db;
    private RemoteViews rv;
    private OkHttpClient client;
    private Thread thread;
    private WaterMark waterMark;
    private Handler handler;
    private int NOTIFICATION_ID = 107;
    private boolean isResize = false;
    private boolean isWaterMark = false;
    private Mp4Composer mp4Composer;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private static final String CANCEL_TAG = "c_dv";
    private String NOTIFICATION_CHANNEL_ID = "download_video";
    public static final String ACTION_STOP = "com.dv.action.STOP";
    public static final String ACTION_START = "com.dv.action.START";
    private String video_id, downloadUrl, filePath_local, file_path, file_path_delete, file_name, layout_type, status_type, watermark_image, watermark_on_off;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        db = new DatabaseHandler(getApplicationContext());
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                .setTicker(getResources().getString(R.string.downloading))
                .setWhen(System.currentTimeMillis())
                .setOnlyAlertOnce(true);

        rv = new RemoteViews(getPackageName(), R.layout.my_custom_notification);
        rv.setTextViewText(R.id.nf_title, getString(R.string.app_name));
        rv.setProgressBar(R.id.progress, 100, 0, false);
        rv.setTextViewText(R.id.nf_percentage, getResources().getString(R.string.downloading) + " " + "(0%)");

        Intent closeIntent = new Intent(this, DownloadVideoService.class);
        closeIntent.setAction(ACTION_STOP);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0,
                closeIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        rv.setOnClickPendingIntent(R.id.relativeLayout_nf, pcloseIntent);

        builder.setCustomContentView(rv);
        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getResources().getString(R.string.app_name);// The user-visible name of the channel.
            mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }
        startForeground(NOTIFICATION_ID, builder.build());

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message message) {
                int progress = Integer.parseInt(message.obj.toString());
                switch (message.what) {
                    case 1:
                        rv.setProgressBar(R.id.progress, 100, progress, false);
                        if (isWaterMark) {
                            rv.setTextViewText(R.id.nf_percentage, getResources().getString(R.string.watermark) + " " + "(" + progress + " %)");
                        } else {
                            rv.setTextViewText(R.id.nf_percentage, getResources().getString(R.string.downloading) + " " + "(" + progress + " %)");
                        }
                        notificationManager.notify(NOTIFICATION_ID, builder.build());
                        break;
                    case 2:
                        stopForeground(false);
                        stopSelf();
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent.getAction() != null && intent.getAction().equals(ACTION_START)) {

            video_id = intent.getStringExtra("video_id");
            downloadUrl = intent.getStringExtra("downloadUrl");
            file_path = intent.getStringExtra("file_path");
            file_name = intent.getStringExtra("file_name");
            layout_type = intent.getStringExtra("layout_type");
            status_type = intent.getStringExtra("status_type");
            watermark_image = intent.getStringExtra("watermark_image");
            watermark_on_off = intent.getStringExtra("watermark_on_off");

            assert watermark_on_off != null;
            if (watermark_on_off.equals("true")) {
                filePath_local = getExternalCacheDir().getAbsolutePath();
            } else {
                filePath_local = file_path;
            }
            file_path_delete = filePath_local;

            init();
        }
        if (intent.getAction() != null && intent.getAction().equals(ACTION_STOP)) {
            try {
                if (client != null) {
                    for (Call call : client.dispatcher().queuedCalls()) {
                        if (call.request().tag().equals(CANCEL_TAG))
                            call.cancel();
                    }
                    for (Call call : client.dispatcher().runningCalls()) {
                        if (call.request().tag().equals(CANCEL_TAG))
                            call.cancel();
                    }
                }
                if (handler != null) {
                    handler.removeCallbacks(thread);
                }
                if (thread != null) {
                    thread.interrupt();
                    thread = null;
                }
                if (waterMark != null) {
                    waterMark.cancel(true);
                }
                if (mp4Composer != null) {
                    mp4Composer.cancel();
                }
                if (db != null) {
                    if (!db.checkId_status_download(video_id, status_type)) {
                        db.delete_status_download(video_id, status_type);
                    }
                }
                if (file_path_delete != null || file_name != null) {
                    File file = new File(file_path_delete, file_name);
                    if (file.exists()) {
                        file.delete();
                    }
                }
                stopForeground(false);
                stopSelf();
                Method.isDownload = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(false);
        stopSelf();
    }

    public void init() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {

                client = new OkHttpClient();
                Request.Builder builder = new Request.Builder()
                        .url(downloadUrl)
                        .addHeader("Accept-Encoding", "identity")
                        .get()
                        .tag(CANCEL_TAG);

                Call call = client.newCall(builder.build());

                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        Log.e("TAG", "=============onFailure===============");
                        e.printStackTrace();
                        Log.d("error_downloading", e.toString());
                        Method.isDownload = true;
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        Log.e("TAG", "=============onResponse===============");
                        assert response.body() != null;
                        final ResponseBody responseBody = ProgressHelper.withProgress(response.body(), new ProgressUIListener() {

                            //if you don't need this method, don't override this methd. It isn't an abstract method, just an empty method.
                            @Override
                            public void onUIProgressStart(long totalBytes) {
                                super.onUIProgressStart(totalBytes);
                                Log.e("TAG", "onUIProgressStart:" + totalBytes);
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onUIProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                                Log.e("TAG", "=============start===============");
                                Message msg = handler.obtainMessage();
                                msg.what = 1;
                                msg.obj = (int) (100 * percent) + "";
                                handler.sendMessage(msg);
                            }

                            //if you don't need this method, don't override this method. It isn't an abstract method, just an empty method.
                            @Override
                            public void onUIProgressFinish() {
                                super.onUIProgressFinish();
                                Log.e("TAG", "onUIProgressFinish:");

                                if (watermark_on_off.equals("true")) {
                                    //call data watermark class add to watermark
                                    MediaPlayer mp = new MediaPlayer();
                                    try {
                                        mp.setDataSource(filePath_local + "/" + file_name);
                                        mp.prepareAsync();
                                        mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                            @Override
                                            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                                                if (layout_type.equals("Portrait")) {
                                                    if (height <= 700) {
                                                        isResize = true;
                                                    }
                                                } else {
                                                    if (height <= 400 || width <= 400) {
                                                        isResize = true;
                                                    }
                                                }
                                                waterMark = new WaterMark();
                                                waterMark.execute();
                                            }
                                        });
                                    } catch (Exception e) {
                                        waterMark = new WaterMark();
                                        waterMark.execute();
                                        e.printStackTrace();
                                    }
                                } else {
                                    Message msg = handler.obtainMessage();
                                    msg.what = 2;
                                    msg.obj = 0 + "";
                                    handler.sendMessage(msg);
                                    Method.isDownload = true;
                                    showMedia(filePath_local, file_name);
                                }

                            }
                        });

                        try {

                            BufferedSource source = responseBody.source();
                            File outFile = new File(filePath_local + "/" + file_name);
                            BufferedSink sink = Okio.buffer(Okio.sink(outFile));
                            source.readAll(sink);
                            sink.flush();
                            source.close();

                        } catch (Exception e) {
                            Log.d("show_data", e.toString());
                        }

                    }
                });
            }
        });
        thread.start();
    }

    @SuppressLint("StaticFieldLeak")
    class WaterMark extends AsyncTask<String, String, String> {

        Bitmap image = null;

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(watermark_image);
                try {
                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    Log.d("error_data", e.toString());
                }
            } catch (IOException e) {
                image = BitmapFactory.decodeResource(getResources(), R.drawable.watermark);
            }
            if (isResize) {
                image = getResizedBitmap(image, 40, 40);
                isResize = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            file_path_delete = file_path;
            mp4Composer = null;
            mp4Composer = new Mp4Composer(filePath_local + "/" + file_name, file_path + "/" + file_name)
                    .filter(new GlWatermarkFilter(image, GlWatermarkFilter.Position.RIGHT_BOTTOM))
                    .listener(new Mp4Composer.Listener() {
                        @Override
                        public void onProgress(double progress) {
                            isWaterMark = true;
                            double value = progress * 100;
                            int i = (int) value;
                            Message msg = handler.obtainMessage();
                            msg.what = 1;
                            msg.obj = i + "";
                            handler.sendMessage(msg);
                        }

                        @Override
                        public void onCompleted() {
                            isWaterMark = false;
                            new File(getExternalCacheDir().getAbsolutePath() + "/" + file_name).delete();//delete file to save in cash folder
                            Message msg = handler.obtainMessage();
                            msg.what = 2;
                            msg.obj = 0 + "";
                            handler.sendMessage(msg);
                            Method.isDownload = true;
                            showMedia(file_path, file_name);
                        }

                        @Override
                        public void onCanceled() {
                            isWaterMark = false;
                            Message msg = handler.obtainMessage();
                            msg.what = 2;
                            msg.obj = 0 + "";
                            handler.sendMessage(msg);
                            Method.isDownload = true;
                        }

                        @Override
                        public void onFailed(Exception exception) {
                            isWaterMark = false;
                            Message msg = handler.obtainMessage();
                            msg.what = 2;
                            msg.obj = 0 + "";
                            handler.sendMessage(msg);
                            Method.isDownload = true;
                        }
                    })
                    .start();
        }
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public void showMedia(String file_path, String file_name) {
        try {
            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file_path + "/" + file_name},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
